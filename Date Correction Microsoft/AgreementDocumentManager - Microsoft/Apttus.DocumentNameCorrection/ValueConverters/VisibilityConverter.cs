﻿using Apttus.Model;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Apttus.DocumentNameCorrection.ValueConverters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "C:\\Users\\Tapankumar Patel\\Desktop\\tkppatel_WorkSpace\\BlobStorageBulkDocumentUpload\\BlobStorageApplication\\BlobStorageFileUploader\\ApttusLogo.ico";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "C:\\Users\\Tapankumar Patel\\Desktop\\tkppatel_WorkSpace\\BlobStorageBulkDocumentUpload\\BlobStorageApplication\\BlobStorageFileUploader\\ApttusLogo.ico";
        }
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class LoadingVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
                return Visibility.Visible;
            //return ((System.Collections.ObjectModel.Collection<Model.FileLogEntry>)value).Count > 0 ? Visibility.Visible : Visibility.Hidden;
            else
                return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    [ValueConversion(typeof(int), typeof(Visibility))]
    public class LoadingVisibilityConverterByCount : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToInt32(value) > 0 ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class ComponentVisibilityOnFlag : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)(value) == true)
            {
                return Visibility.Visible;
            }
            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    [ValueConversion(typeof(EnumApplicationState), typeof(Visibility))]
    public class ProgressImageVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                int applicationState = (int)(value);
                int ReferenceValue = (int)parameter;
                if (applicationState >= ReferenceValue)
                    return Visibility.Visible;
            }
            return Visibility.Hidden;
        }
        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class ComponentVisibilityForData : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return Visibility.Visible;
            }
            return Visibility.Hidden;
        }
        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
