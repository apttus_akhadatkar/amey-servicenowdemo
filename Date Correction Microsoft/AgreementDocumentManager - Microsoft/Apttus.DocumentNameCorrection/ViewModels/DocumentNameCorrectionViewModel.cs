﻿using Apttus.Model;
using Apttus.PluginExecutionService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Apttus.DocumentNameCorrection.ViewModels
{
    public class DocumentNameCorrectionViewModel : ViewModelBase
    {
        private string filePath = string.Empty;
        public object _syncLock = new object();
        public string FilePath
        {
            get { return filePath; }
            set
            {
                if (filePath == value) return;
                filePath = value;
                RaisePropertyChanged();
            }
        }
        private EnumEnvironment environment;
        public EnumEnvironment Environment
        {
            get { return environment; }
            set
            {
                if (environment == value) return;
                environment = value;
                RaisePropertyChanged();
            }
        }
        private ErrorDetail error = new ErrorDetail();
        public ErrorDetail Error
        {
            get { return error; }
            set
            {
                if (error == value) return;
                error = value;
                RaisePropertyChanged();
            }
        }
        private ApplicationState appState;
        public ApplicationState AppStatus
        {
            get { return appState; }
            set
            {
                if (appState == value) return;
                appState = value;
                RaisePropertyChanged();
            }
        }
        public Command BrowseFile
        {
            get; private set;
        }
        public Command ProcessAgreementCorrection
        {
            get; private set;
        }
        public Model.PluginExecutionContext PluginExecutionContext
        {
            get;
            set;
        }
        private ObservableCollection<FileLogEntry> gridData;
        public ObservableCollection<FileLogEntry> GridData
        {
            get { return gridData; }
            set
            {
                if (gridData == value) return;
                gridData = value;
                RaisePropertyChanged();
            }
        }
        public DocumentNameCorrectionViewModel()
        {
            BrowseFile = new Command(BrowseLocalFile);
            ProcessAgreementCorrection = new Command(ProcessAgreementDocuments);
        }
        private async void BrowseLocalFile()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Comma Separated Values (.csv)|*.csv";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true && !string.IsNullOrEmpty(dlg.FileName))
            {
                // Open document
                FilePath = dlg.FileName;
            }

        }
        private async void ProcessAgreementDocuments()
        {
            //Process.IsEnabled = false;
            //btnbrowse.IsEnabled = false;
            //Validate.IsEnabled = false;
            //string fullPath = FileNameTextBox.Text;
            if (File.Exists(FilePath))
            {
                if (Helper.HasWritePermissionOnDir(System.IO.Path.GetDirectoryName(FilePath)))
                {
                    PluginExecutionContext = new Model.PluginExecutionContext();
                    PluginExecutionContext.Error = Error;

                    PluginExecutionContext.PluginExecutionSequence = ConfigurationManager.AppSettings.Get("PluginExecutionSequenceDocVersionNameCorrection");
                    PluginExecutionContext.executionContext = new Model.ExecutionContext();
                    PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier = DateTime.Now.ToString("yyMMddHHmmssff"); ;
                    PluginExecutionContext.executionContext.AllowLogs = true;
                    PluginExecutionContext.SplitDelimeter =
                    PluginExecutionContext.executionContext.SplitDelimeter = Convert.ToChar(ConfigurationManager.AppSettings.Get("SplitDelimeter"));
                    PluginExecutionContext.executionContext.AzureBlobContainer = ConfigurationManager.AppSettings.Get("Container");
                    PluginExecutionContext.executionContext.CRM_MaxInsertChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxInsertChunk"));
                    PluginExecutionContext.executionContext.CRM_MaxQueryConditionChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxQueryConditionChunk"));
                    
                    PluginExecutionContext.RecoveryPluginExecutionSequence = ConfigurationManager.AppSettings.Get("RecoveryFlow");
                    PluginExecutionContext.executionContext.FilePath = FilePath;
                    ExecutePlugins executePlugins = new ExecutePlugins();
                    PluginExecutionContext.executionContext.FileDataLog = new AsyncObservableCollection<FileLogEntry>();
                    PluginExecutionContext.executionContext.SyncLock = _syncLock;
                    BindingOperations.EnableCollectionSynchronization(PluginExecutionContext.executionContext.FileDataLog, _syncLock);
                    GridData = PluginExecutionContext.executionContext.FileDataLog;
                    PluginExecutionContext.executionContext.SkippedFileDataLog = new List<FileLogEntry>();
                    AppStatus = PluginExecutionContext.executionContext.ApplicationRunningState = new ApplicationState();
                    PluginExecutionContext.executionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.IdleState;
                    //HandleApplicationStatus();
                    Task.Run(() => executePlugins.Execute(PluginExecutionContext));
                }
                else
                {
                    throw new Exception("No Write permission to save log file..");
                }
            }

        }
    }
}
