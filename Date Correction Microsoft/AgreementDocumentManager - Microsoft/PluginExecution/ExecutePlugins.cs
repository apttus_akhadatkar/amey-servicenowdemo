﻿using Apttus.Model;
using Apttus.Plugins;
using System;
using System.Linq;

namespace Apttus.PluginExecutionService
{
    public class ExecutePlugins
    {
        public async void Execute(PluginExecutionContext pluginExecutionContext)
        {
            string[] pluginsToExecute = pluginExecutionContext.PluginExecutionSequence.Split(pluginExecutionContext.SplitDelimeter);
            pluginExecutionContext.executionContext.ApplicationRunningState.MaxStateInteger = pluginsToExecute.Length;
            try
            {
                pluginExecutionContext.executionContext.ApplicationRunningState.IsAppRunning = true;
                foreach (string objType in pluginsToExecute)
                {
                    try
                    {
                        Type type = Type.GetType("Apttus.Plugins." + objType + ", Apttus.Plugins");
                        PluginExecution instance = null;
                        if (type != null)
                        {

                            pluginExecutionContext.executionContext.ApplicationRunningState.LiveStatus = objType + " In Progress.";
                            instance = (PluginExecution)Activator.CreateInstance(type);
                            ((PluginExecution)instance).Process(pluginExecutionContext.executionContext);
                        }
                        else
                        {
                            pluginExecutionContext.Error.ErrorMessage = pluginExecutionContext.Error.ErrorMessage + Environment.NewLine + string.Format("Plugin not found : {0}", objType);
                            pluginExecutionContext.Error.ErrorOccured = true;
                            if (!pluginExecutionContext.executionContext.ContinueOnError)
                            {
                                pluginExecutionContext.executionContext.ApplicationRunningState.LiveStatus = " Error in executing : " + objType + Environment.NewLine + "Process aborted due to error.";
                                return;
                            }
                        }
                        pluginExecutionContext.executionContext.ApplicationRunningState.ApplicationStateInteger++;
                        pluginExecutionContext.executionContext.ApplicationRunningState.LiveStatus = objType + " Execution Completed.";
                    }
                    catch (Exception ex)
                    {
                        pluginExecutionContext.Error.ErrorMessage = pluginExecutionContext.Error.ErrorMessage + Environment.NewLine  + Environment.NewLine + ex.Message;
                        pluginExecutionContext.executionContext.ApplicationRunningState.LiveStatus = " Error in executing : " + objType;
                        pluginExecutionContext.Error.ErrorOccured = true;
                        if (!pluginExecutionContext.executionContext.ContinueOnError)
                        {
                            pluginExecutionContext.executionContext.ApplicationRunningState.LiveStatus = " Error in executing : " + objType + Environment.NewLine + "Process aborted due to error."; ;
                            return;
                        }
                    }
                }
                //if (pluginExecutionContext.executionContext.FileDataLog.Count(x => x.HasError == true && (x.DocumentVersion != "" || x.DocumentVersionDetail != "")) > 0)
                //    RunRecovery(pluginExecutionContext);
                pluginExecutionContext.executionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.ProcessCompleted;
                pluginExecutionContext.executionContext.ApplicationRunningState.LiveStatus = "Application process completed.";
                pluginExecutionContext.executionContext.ApplicationRunningState.IsAppRunning = false;
            }
            catch (Exception ex)
            {
                pluginExecutionContext.Error.ErrorMessage = pluginExecutionContext.Error.ErrorMessage + Environment.NewLine  + Environment.NewLine + ex.Message;
                pluginExecutionContext.Error.ErrorOccured = true;
            }
            pluginExecutionContext.executionContext.ApplicationRunningState.IsAppRunning = false;
        }
        public void RunRecovery(PluginExecutionContext pluginExecutionContext)
        {
            string[] pluginsToExecute = pluginExecutionContext.RecoveryPluginExecutionSequence.Split(pluginExecutionContext.SplitDelimeter);
            pluginExecutionContext.executionContext.ApplicationRunningState.MaxStateInteger = pluginsToExecute.Length;
            pluginExecutionContext.executionContext.ApplicationRunningState.IsAppRunning = true;
            try
            {
                pluginExecutionContext.executionContext.ApplicationRunningState.IsAppRunning = true;
                pluginExecutionContext.executionContext.ApplicationRunningState.ApplicationStateInteger = 0;
                foreach (string objType in pluginsToExecute)
                {
                    Type type = Type.GetType("Apttus.Plugins." + objType + ", Apttus.Plugins");
                    PluginExecution instance = null;
                    if (type != null)
                    {

                        pluginExecutionContext.executionContext.ApplicationRunningState.LiveStatus = objType + " In Progress.";
                        instance = (PluginExecution)Activator.CreateInstance(type);
                        ((PluginExecution)instance).Process(pluginExecutionContext.executionContext);
                    }
                    pluginExecutionContext.executionContext.ApplicationRunningState.ApplicationStateInteger++;
                }
                pluginExecutionContext.executionContext.ApplicationRunningState.IsAppRunning = false;
            }
            catch (Exception ex)
            { }
        }
    }
}

