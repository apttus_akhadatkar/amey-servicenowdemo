﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BlobStorageApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Student> students = new List<Student>();
        public MainWindow()
        {
            
        }

        protected void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            dataGrid.Items.Refresh();
        }
        protected void dispatcherTimer1_Tick(object sender, EventArgs e)
        {
            students.Add(new Student() { Name = "Tapan", SchoolName = "BVM", RollNumber = 10 });
        }
        private void SetTimer()
        {
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
        }
        private void SetTimer1()
        {
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer1_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 3);
            dispatcherTimer.Start();
        }

        private void LoadGrid_Click(object sender, RoutedEventArgs e)
        {
            SetTimer();
            SetTimer1();
            dataGrid.ItemsSource = students;
        }
    }
    public class Student
    {
        public string Name { get; set; }
        public string SchoolName { get; set; }
        public int RollNumber { get; set; }
    }
}
