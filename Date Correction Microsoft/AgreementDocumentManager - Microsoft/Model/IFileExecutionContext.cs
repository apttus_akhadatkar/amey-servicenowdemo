﻿namespace Apttus.Model
{
    public interface IFileExecutionContext
    {
        string FilePath { get; set; }

        bool AllowLogs { get; set; }

        char SplitDelimeter { get; set; }

        string CurrentUniqueRunIdentifier { get; set; }
    }
}
