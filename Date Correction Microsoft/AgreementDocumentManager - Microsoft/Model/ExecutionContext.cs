﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Apttus.Model
{
    public partial class ExecutionContext : IExecutionContext
    {
		public DateTime BeforeDate { get; set; }

		public AzureAuthenticationDetail AzureLogin
        {
            get;
            set;
        }
        public bool ContinueOnError
        {
            get;
            set;
        }
        public bool IsAnnotationsEnabled
        {
            get;
            set;
        }
        private ObservableCollection<FileLogEntry> fileLogs;
        public string FilePath { get; set; }

        public bool AllowLogs { get; set; }

        public char SplitDelimeter { get; set; }

        public string CurrentUniqueRunIdentifier { get; set; }
        public string InputDataLength { get; set; }

        public string LogFilePath { get; set; }

        public CRMUserDetail CRMUser
        {
            get;
            set;
        }
        public string AgreementIdentifier { get; set; }
        public int CRM_MaxUploadFileSize { get; set; }
        public int CRM_MaxInsertChunk { get; set; }

        public int CRM_MaxQueryConditionChunk { get; set; }

        public string AzureBlobContainer { get; set; }

        public List<string> AgreementsToProcess { get; set; }

        public List<string> AgreementAndDocumentType { get; set; }

        public List<DataLine> InputFileDataLines { get; set; }

        public List<FileLogEntry> SkippedFileDataLog { get; set; }

        public ObservableCollection<FileLogEntry> FileDataLog
        {
            get
            {
                return fileLogs;
            }
            set
            {
                fileLogs = value;
            }
        }

        public Dictionary<string, Guid> AgreeNumToDocVersionIDMap { get; set; }
        public ObservableCollection<DataLine> InvalidDataLines { get; set; }
        public ObservableCollection<DataLine> IncorrectDataLines { get; set; }
        public string InvalidDataFilePath { get; set; }
        public ApplicationState ApplicationRunningState { get; set; }

        public object SyncLock { get; set; }
        public bool ValidationOnly { get; set; }
        public string fileDownLoadPath { get; set; }
    }
}
