﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.Model
{
    public class CRMUserDetail: INotifyPropertyChanged
    {
        string loggedInUser;
        public string LoggedInUser
        {
            get { return loggedInUser; }
            set
            {
                if (loggedInUser == value) return;
                loggedInUser = value;
                RaisePropertyChanged();
            }
        }

        //Password for CRM Connection
        string crmDetail;
        public string CRMDetail
        {
            get { return crmDetail; }
            set
            {
                if (crmDetail == value) return;
                crmDetail = value;
                RaisePropertyChanged();
            }
        }

        //Service URL for CRM Connection
        string crmSvcUrl;
        public string CRM_ServiceUrl
        {
            get { return crmSvcUrl; }
            set
            {
                if (crmSvcUrl == value) return;
                crmSvcUrl = value;
                RaisePropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
