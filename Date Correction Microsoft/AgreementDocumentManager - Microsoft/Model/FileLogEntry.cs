﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Apttus.Model
{
    public class FileLogEntry : INotifyPropertyChanged
    {
        private string agreementDocumentType;
        public string AgreementDocumentType
        {

            get { return agreementDocumentType; }
            set
            {
                if (agreementDocumentType == value) return;
                agreementDocumentType = value;
                RaisePropertyChanged();
            }
        }
        private string documentSecurity;
        public string DocumentSecurity
        {

            get { return documentSecurity; }
            set
            {
                if (documentSecurity == value) return;
                documentSecurity = value;
                RaisePropertyChanged();
            }
        }
        private string agreementGUID;
        public string AgreementGUID
        {
            get { return agreementGUID; }
            set
            {
                if (agreementGUID == value) return;
                agreementGUID = value;
                RaisePropertyChanged();
            }
        }
        public string UniqueIdentifier { get; set; }
        private string blobSourcePath;
        public string BlobSourcePath
        {
            get { return blobSourcePath; }
            set
            {
                if (blobSourcePath == value) return;
                blobSourcePath = value;
                RaisePropertyChanged();
            }
        }
        public string FileName { get; set; }
        private string blobDetail;
        public string BlobDetail
        {
            get { return blobDetail; }
            set
            {
                if (blobDetail == value) return;
                blobDetail = value;
                RaisePropertyChanged();
            }
        }
        private string documentVersion;
        public string DocumentVersion
        {
            get { return documentVersion; }
            set
            {
                if (documentVersion == value) return;
                documentVersion = value;
                RaisePropertyChanged();
            }
        }
        private string documentVersionDetail;
        public string DocumentVersionDetail
        {
            get { return documentVersionDetail; }
            set
            {
                if (documentVersionDetail == value) return;
                documentVersionDetail = value;
                RaisePropertyChanged();
            }
        }
        private string annotations;
        public string Annotations
        {
            get { return annotations; }
            set
            {
                if (annotations == value) return;
                annotations = value;
                RaisePropertyChanged();
            }
        }
        private string documentVersionDetailTitle;
        public string DocumentVersionDetailTitle
        {
            get { return documentVersionDetailTitle; }
            set
            {
                if (documentVersionDetailTitle == value) return;
                documentVersionDetailTitle = value;
                RaisePropertyChanged();
            }
        }
        private string agreementNumber;
        public string AgreementNumber
        {
            get { return agreementNumber; }
            set
            {
                if (agreementNumber == value) return;
                agreementNumber = value;
                RaisePropertyChanged();
            }
        }
		private DateTime? startdateModified;
		public DateTime? StartDateModified
		{
			get { return startdateModified; }
			set
			{
				if (startdateModified == value) return;
				startdateModified = value;
				RaisePropertyChanged();
			}
		}
		private DateTime? enddateModified;
		public DateTime? EndDateModified
		{
			get { return enddateModified; }
			set
			{
				if (enddateModified == value) return;
				enddateModified = value;
				RaisePropertyChanged();
			}
		}
		private UploadStatus status;
        public UploadStatus Status
        {
            get { return status; }
            set
            {
                if (status == value) return;
                status = value;
                RaisePropertyChanged();
            }
        }
        public string CurrentRunIdentifier { get; set; }
        private bool isBlobUploaded;
        public bool IsProcessed
        {
            get { return isBlobUploaded; }
            set
            {
                if (isBlobUploaded == value) return;
                isBlobUploaded = value;
                RaisePropertyChanged();
            }
        }
        public string FilePathToUpload { get; set; }
        public string DocumentType { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        private bool hasError = false;
        public bool HasError
        {
            get { return hasError; }
            set
            {
                if (hasError == value) return;
                hasError = value;
                RaisePropertyChanged();
            }
        }
        private bool isCorrected = false;
        public bool IsCorrected
        {
            get { return isCorrected; }
            set
            {
                if (isCorrected == value) return;
                isCorrected = value;
                RaisePropertyChanged();
            }
        }
		private string agreementName;
		public string AgreementName
		{
			get { return agreementName; }
			set
			{
				if (agreementName == value) return;
				agreementName = value;
				RaisePropertyChanged();
			}
		}
		private DateTime? agreementStartDate;
		public DateTime? AgreementStartDate
		{
			get { return agreementStartDate; }
			set
			{
				if (agreementStartDate == value) return;
				agreementStartDate = value;
				RaisePropertyChanged();
			}
		}
		private DateTime? agreementEnddate;
		public DateTime? AgreementEnddate
		{
			get { return agreementEnddate; }
			set
			{
				if (agreementEnddate == value) return;
				agreementEnddate = value;
				RaisePropertyChanged();
			}
		}
		private string agreementStatus;
		public string AgreementStatus
		{
			get { return agreementStatus; }
			set
			{
				if (agreementStatus == value) return;
				agreementStatus = value;
				RaisePropertyChanged();
			}
		}
		private string agreementStatusCategory;
		public string AgreementStatusCategory
		{
			get { return agreementStatusCategory; }
			set
			{
				if (agreementStatusCategory == value) return;
				agreementStatusCategory = value;
				RaisePropertyChanged();
			}
		}
		private string agreementRecordType;
		public string AgreementRecordType
		{
			get { return agreementRecordType; }
			set
			{
				if (agreementRecordType == value) return;
				agreementRecordType = value;
				RaisePropertyChanged();
			}
		}
		private string agreementOwner;
		public string AgreementOwner
		{
			get { return agreementOwner; }
			set
			{
				if (agreementOwner == value) return;
				agreementOwner = value;
				RaisePropertyChanged();
			}
		}
		private string agreementOwnerID;
		public string AgreementOwnerID
		{
			get { return agreementOwnerID; }
			set
			{
				if (agreementOwnerID == value) return;
				agreementOwnerID = value;
				RaisePropertyChanged();
			}
		}
		private string agreementAccount;
		public string AgreementAccount
		{
			get { return agreementAccount; }
			set
			{
				if (agreementAccount == value) return;
				agreementAccount = value;
				RaisePropertyChanged();
			}
		}
		protected void RaisePropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
