﻿namespace Apttus.Model
{
    public class AzureAuthenticationDetail
    {
        public string ClientID
        {
            get;
            set;
        }
        public string ClientSecretKey
        {
            get;
            set;
        }
        public string DataMarketAccessURI
        {
            get;
            set;
        }
        public string FileUploadURI
        {
            get;
            set;
        }
    }
}
