﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Apttus.Model
{
    public class ApplicationLog
    {
        public ApplicationState ApplicationRunState { get; set; }
    }
    public class ApplicationState : INotifyPropertyChanged
    {
        private EnumApplicationState applicationRunStatus;
        public EnumApplicationState ApplicationRunStatus
        {
            get { return applicationRunStatus; }
            set
            {
                if (applicationRunStatus == value) return;
                applicationRunStatus = value;
                RaisePropertyChanged();
            }
        }
        private EnumApplicationMode applicationMode;
        public EnumApplicationMode ApplicationMode
        {
            get { return applicationMode; }
            set
            {
                if (applicationMode == value) return;
                applicationMode = value;
                RaisePropertyChanged();
            }
        }

        private bool? isAppRunning = null;
        public bool? IsAppRunning
        {
            get { return isAppRunning; }
            set
            {
                if (isAppRunning == value) return;
                isAppRunning = value;
                RaisePropertyChanged();
            }
        }
        private string liveStatus;
        public string LiveStatus
        {
            get { return liveStatus; }
            set
            {
                if (liveStatus == value) return;
                liveStatus = value;
                RaisePropertyChanged();
            }
        }
        private int applicationStateInt;

        public int ApplicationStateInteger
        {
            get { return applicationStateInt; }
            set
            {
                if (applicationStateInt == value) return;
                applicationStateInt = value;
                RaisePropertyChanged();
            }
        }
        private int maxStateInt;

        public int MaxStateInteger
        {
            get { return maxStateInt; }
            set
            {
                if (maxStateInt == value) return;
                maxStateInt = value;
                RaisePropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
    public enum EnumApplicationState
    {
        IdleState = 0,
        ApplicationStarted = 1,
        InputFileValidationInProgress = 2,
        InputFileValidationCompleted = 3,
        FileDataCapturingInProgress = 4,
        FileDataCapturingDone = 5,
        DataValidationInProgress = 6,
        DataValidationDone = 7,
        CRMConnectionInProgress = 8,
        CRMConnectionDone = 9,
        DocumentVersionSyncInProgress = 10,
        DocumentVersionSyncDone = 11,
        DocumentVersionDetailSyncInProgress = 12,
        DocumentVersionDetailSyncDone = 13,
        BlobFileUploadInProgress = 14,
        BlobFileUploadDone = 15,
        ProcessCompleted = 16
    }

    public enum EnumApplicationMode
    {
        ProcessBusiness,
        Recovery,
        Validation
    }
    public enum EnumFilterType
    {
        AgreementNumber,
        BlobSource,
        DocumentVersion,
        DocumentVersionDetail,
        Status,
        IsBlobUploaded,
        BlobPath,
        DocumentType
    }
    public class DataLine
    {
        //public List<string> LineArray { get { return Line.Split('|').ToList(); } }
        public string Line { get; set; }
        public string AgreementNumber { get; set; }
        public string FileLocation { get; set; }
        public string BlobFileName { get; set; }
        public string DocumentType { get; set; }
        public string Error { get; set; }
    }
    public class ErrorDetail : INotifyPropertyChanged
    {
        private bool errorOccured;
        public bool ErrorOccured
        {
            get { return errorOccured; }
            set
            {
                if (errorOccured == value) return;
                errorOccured = value;
                RaisePropertyChanged();
            }
        }
        private string errorMessage;
        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                RaisePropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
