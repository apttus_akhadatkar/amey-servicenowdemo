﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Apttus.Model
{
    public interface IExecutionContext
    {
		DateTime BeforeDate { get; set; }
		//Credentials for AZURE Login
		AzureAuthenticationDetail AzureLogin
        {
            get;
            set;
        }

        //To Check whether to continue on error
        bool ContinueOnError
        {
            get;
            set;
        }
        bool IsAnnotationsEnabled
        {
            get;
            set;
        }

        //File Path for processing data
        string FilePath { get; set; }
        string AgreementIdentifier { get; set; }

        //Allow file logs
        bool AllowLogs { get; set; }

        //Split delimeter
        char SplitDelimeter { get; set; }

        //Current unique run identifier for the application
        string CurrentUniqueRunIdentifier { get; set; }

        //File log path
        string LogFilePath { get; set; }
        string InputDataLength { get; set; }

        //User Name for CRM Connection
        CRMUserDetail CRMUser { get; set; }

        //Max query condition at a time in CRM
        int CRM_MaxQueryConditionChunk { get; set; }

        //MAX insertion at a time
        int CRM_MaxInsertChunk { get; set; }
        int CRM_MaxUploadFileSize { get; set; }

        //AZURE blob container
        string AzureBlobContainer { get; set; }

        //AgreementList to be processed
        List<string> AgreementsToProcess { get; set; }

        //AgreementANDDocumentType key value pair
        List<string> AgreementAndDocumentType { get; set; }

        //FileDataLog which is the data to process, referenced to the data grid
        ObservableCollection<FileLogEntry> FileDataLog { get; set; }

        //Input data Lines for dataGrid, error correction
        List<DataLine> InputFileDataLines { get; set; }

        List<FileLogEntry> SkippedFileDataLog { get; set; }

        Dictionary<string, Guid> AgreeNumToDocVersionIDMap { get; set; }

        //Invalid data lines in the application that are shown in the Data Grid 
        ObservableCollection<DataLine> InvalidDataLines { get; set; }
        string InvalidDataFilePath { get; set; }
        ApplicationState ApplicationRunningState { get; set; }
        object SyncLock { get; set; }
        bool ValidationOnly { get; set; }

        string fileDownLoadPath { get; set; }
    }
}
