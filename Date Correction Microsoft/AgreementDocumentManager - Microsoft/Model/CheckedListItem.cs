﻿namespace Apttus.Model
{
    public class CheckedListItem<T>
    {
        public T Item { get; set; }
        public bool IsChecked { get; set; }
    }
}
