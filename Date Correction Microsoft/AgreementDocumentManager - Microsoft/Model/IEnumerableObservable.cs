﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Apttus.Model
{
    public class IEnumerableObservable<T> : IObservable<T>, IEnumerable<T>
    {
        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
