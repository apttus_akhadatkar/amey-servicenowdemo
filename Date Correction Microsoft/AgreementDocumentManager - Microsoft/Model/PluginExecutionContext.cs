﻿namespace Apttus.Model
{
    public class PluginExecutionContext
    {
        public string PluginExecutionSequence { get; set; }
        public string RecoveryPluginExecutionSequence { get; set; }
        public char SplitDelimeter { get; set; }
        public IExecutionContext executionContext { get; set; }
        public ErrorDetail Error { get; set; }
    }

}
