﻿namespace Apttus.Model
{
    public enum UploadStatus
    {
        FileValidated,
        AgreementInfoRetrieved,
        AgreementInfoDoesntExist,
        DocumentVersionCreated,
        DocumentVersionCreationFailed,
        DocumentVersionDetailCreated,
        FileUploaded,
        FileValidationFailed,
        DocumentVersionDetailCreationFailed,
        BlobUploadFailed,
        InvalidAgreementNumber,
        DocumentVersionRecovered,
        FileDownloaded,
        FileDownloadFailed
    }
}
