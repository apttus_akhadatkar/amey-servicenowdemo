﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Apttus.AgreementDocumentManager.Views
{
    /// <summary>
    /// Interaction logic for MainWindowBulkDocumentStorage.xaml
    /// </summary>
    public partial class MainWindowBulkDocumentStorage : Window
    {
        public string IconImageName = "ApttusLogo.ico";
        public MainWindowBulkDocumentStorage()
        {
            InitializeComponent();
        }

        private void CheckBox_SourceUpdated(object sender, DataTransferEventArgs e)
        {

        }

        private void dataGrid_CopyingRowClipboardContent(object sender, System.Windows.Controls.DataGridRowClipboardEventArgs e)
        {
            //e.ClipboardRowContent.Add(new DataGridClipboardCellContent(e.Item, (sender as DataGrid).Columns[0], "Abc-hello"));
        }
    }
}
