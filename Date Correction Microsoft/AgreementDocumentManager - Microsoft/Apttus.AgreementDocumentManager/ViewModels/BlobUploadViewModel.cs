﻿using Apttus.LoginControl.LoginWindow;
using Apttus.Model;
using Apttus.PluginExecutionService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Apttus.AgreementDocumentManager.ViewModels
{
    public enum EnumApplicationState
    {
        IdleState = 0,
        ApplicationStarted = 1,
        InputFileValidationInProgress = 2,
        InputFileValidationCompleted = 3,
        FileDataCapturingInProgress = 4,
        FileDataCapturingDone = 5,
        DataValidationInProgress = 6,
        DataValidationDone = 7,
        CRMConnectionInProgress = 8,
        CRMConnectionDone = 9,
        DocumentVersionSyncInProgress = 10,
        DocumentVersionSyncDone = 11,
        DocumentVersionDetailSyncInProgress = 12,
        DocumentVersionDetailSyncDone = 13,
        BlobFileUploadInProgress = 14,
        BlobFileUploadDone = 15
    }
    internal class BlobUploadViewModel : ViewModelBase
    {
        public object _syncLock = new object();
        public object _syncLockError = new object();
        public object _syncIncorrectLockError = new object();

        public BlobUploadViewModel()
        {
            DocumentUploadData = new DocumentUploadDataViewModel();
            OpenFilterPopup = false;
            //BrowseFile = new Command(BrowseLocalFile);
            //ProcessAgreements = new Command(ProcessAgreementDocuments);
            StartFilter = false;
            FilterData = new Command(StartFiltering);
            OpenFilter = new Command(OpenFilterPopUp);
            OpenErrorFile = new Command(OpenCorrectedFile);
            OpenAppLogFile = new Command(OpenLogFile);
            SaveAndValidate = new Command(SaveAndValidateCorrectedAgreements);
            FileUploadImageFileName = ConfigurationManager.AppSettings.Get("FileUploadImageName");
            ProgressArrowImageFileName = ConfigurationManager.AppSettings.Get("ProgressArrowImageName");
            DataValidationImageFileName = ConfigurationManager.AppSettings.Get("DataValidation");
            CRMSynchronizationImageFileName = ConfigurationManager.AppSettings.Get("CRMSynchronization");
            UploadToBlobImageFileName = ConfigurationManager.AppSettings.Get("BlobUpload");
            ApttusLogoImageFileName = ConfigurationManager.AppSettings.Get("ApttusLogo");
            FilterImageFileName = ConfigurationManager.AppSettings.Get("FilterImage");
            SelectAllFilterColumnData = new Command(SelectAllFilter);
            UnSelectAllFilterColumnData = new Command(UnSelectAllFilter);
            ApplyFilterOnData = new Command(ApplyFilter);
            CancelSelectedFilter = new Command(CancelFilter);
            CheckedData = new Command(Checked);
            UnCheckedData = new Command(Unchecked);
            OpenRecoveryFile = new Command(BrowseRecoveryFile);
            ProcessFileForRecovery = new Command(ProcessRecovery);
            ConnectToCRM = new Command(LoginToCRM);
            BrowseAgreementsForBulkDownload = new Command(BrowseAgreements);
            ProcessAgreementsForBulkDownload = new Command(ProcessAgreementsDownload); 
            IsDailyMode = true;
            ExecutionMode = ConfigurationManager.AppSettings.Get("Mode");
            IsMigrationMode = !IsDailyMode;
        }
        public Model.PluginExecutionContext PluginExecutionContext
        {
            get;
            set;
        }
        private bool openErrorPopup;

        public string ExecutionMode { get; set; }
        public bool IsMigrationMode { get; set; }
        public bool IsDailyMode { get; set; }
        private ErrorDetail error = new ErrorDetail();
        public ErrorDetail Error
        {
            get { return error; }
            set
            {
                if (error == value) return;
                error = value;
                RaisePropertyChanged();
            }
        }
        private CRMUserDetail crmDetail;
        public CRMUserDetail CRMDetail
        {
            get { return crmDetail; }
            set
            {
                if (crmDetail == value) return;
                crmDetail = value;
                RaisePropertyChanged();
            }
        }
        private string filePath = string.Empty;
        private bool authenticated = false;
        public bool Authenticated
        {
            get { return authenticated; }
            set
            {
                if (authenticated == value) return;
                authenticated = value;
                RaisePropertyChanged();
            }
        }
        private bool startFilter;
        public bool StartFilter
        {
            get { return startFilter; }
            set
            {
                if (startFilter == value) return;
                startFilter = value;
                RaisePropertyChanged();
            }
        }
        public bool isErrorDataRefreshed
        {
            get;
            set;
        }
        public string FilePath
        {
            get { return filePath; }
            set
            {
                if (filePath == value) return;
                filePath = value;
                RaisePropertyChanged();
            }
        }
        private string recoveryFileName;
        public string RecoveryFilePath
        {
            get { return recoveryFileName; }
            set
            {
                if (recoveryFileName == value) return;
                recoveryFileName = value;
                RaisePropertyChanged();
            }
        }

        public DocumentUploadDataViewModel DocumentUploadData
        {
            get;
            set;
        }

        private ObservableCollection<FileLogEntry> gridData;
        public ObservableCollection<FileLogEntry> GridData
        {
            get { return gridData; }
            set
            {
                if (gridData == value) return;
                gridData = value;
                RaisePropertyChanged();
            }
        }
        private ObservableCollection<FileLogEntry> recoveryGridData;
        public ObservableCollection<FileLogEntry> RecoveryGridData
        {
            get { return recoveryGridData; }
            set
            {
                if (recoveryGridData == value) return;
                recoveryGridData = value;
                RaisePropertyChanged();
            }
        }
        private ObservableCollection<FileLogEntry> fileDownloadData;
        public ObservableCollection<FileLogEntry> FileDownloadData
        {
            get { return fileDownloadData; }
            set
            {
                if (fileDownloadData == value) return;
                fileDownloadData = value;
                RaisePropertyChanged();
            }
        }

        public IEnumerable<FileLogEntry> FilteredData
        {
            get;
            set;
        }
        private ObservableCollection<DataLine> errorGridData;
        public ObservableCollection<DataLine> ErrorGridData
        {
            get { return errorGridData; }
            set
            {
                if (errorGridData == value) return;
                errorGridData = value;
                RaisePropertyChanged();
            }
        }
        private ApplicationState applicationState;
        public ApplicationState ApplicationState
        {
            get { return applicationState; }
            set
            {
                if (applicationState == value) return;
                applicationState = value;
                RaisePropertyChanged();
            }
        }

        public EnumFilterType CurrentFilterType
        {
            get;
            set;
        }
        public ObservableCollection<CheckedListItem<object>> CurrentFilterObject
        {
            get;
            set;
        }
        public Dictionary<EnumFilterType, ObservableCollection<CheckedListItem<object>>> objFilter
        {
            get;
            set;
        }
        public string InvalidDataFilePath
        {
            get;
            set;
        }
        private string logDataFilePath;
        public string LogDataFilePath
        {
            get { return logDataFilePath; }
            set
            {
                if (logDataFilePath == value) return;
                logDataFilePath = value;
                RaisePropertyChanged();
            }
        }
        public string FileUploadImageFileName
        {
            get;
            set;
        }
        public string ProgressArrowImageFileName
        {
            get;
            set;
        }
        public string DataValidationImageFileName
        {
            get;
            set;
        }
        public string CRMSynchronizationImageFileName
        {
            get;
            set;
        }
        public string UploadToBlobImageFileName
        {
            get;
            set;
        }
        public string ApttusLogoImageFileName
        {
            get;
            set;
        }
        public string FilterImageFileName
        {
            get;
            set;
        }
        public Command ConnectToCRM
        {
            get; private set;
        }
        public Command OpenRecoveryFile
        {
            get;
            set;
        }
        public Command ProcessFileForRecovery
        {
            get;
            set;
        }
        public Command ProcessAgreements
        {
            get;
            private set;
        }
        public Command BrowseFile
        {
            get; private set;
        }
        public Command FilterData
        {
            get; private set;
        }
        public Command OpenFilter
        {
            get; private set;
        }
        public Command SelectAllFilterColumnData
        {
            get; private set;
        }
        public Command UnSelectAllFilterColumnData
        {
            get; private set;
        }
        public Command ApplyFilterOnData
        {
            get; private set;
        }
        public Command CancelSelectedFilter
        {
            get; private set;
        }
        public Command CheckedData
        {
            get; private set;
        }
        public Command UnCheckedData
        {
            get; private set;
        }
        public Command SaveAndValidate
        {
            get; private set;
        }
        public Command OpenErrorFile
        {
            get; private set;
        }
        public Command OpenAppLogFile
        {
            get; private set;
        }
        public Command BrowseAgreementsForBulkDownload
        {
            get; private set;
        }
        public Command ProcessAgreementsForBulkDownload
        {
            get; private set;
        }
        public string FilterPopupPlacementTarget
        {
            get;
            set;
        }
        private bool openFilterPopup;
        public bool OpenFilterPopup
        {
            get { return openFilterPopup; }
            set
            {
                if (openFilterPopup == value) return;
                openFilterPopup = value;
                RaisePropertyChanged();
            }
        }
        public string CurrentRunIdentifier
        {
            get;
            set;
        }
        private ObservableCollection<CheckedListItem<object>> filterItem;
        public ObservableCollection<CheckedListItem<object>> ItemFilters
        {
            get { return filterItem; }
            set
            {
                if (filterItem == value) return;
                filterItem = value;
                RaisePropertyChanged();
            }
        }
        private EnumEnvironment environment;
        public EnumEnvironment Environment
        {
            get { return environment; }
            set
            {
                if (environment == value) return;
                environment = value;
                RaisePropertyChanged();
            }
        }
        private async void LoginToCRM()
        {
            #region Login Control
            // Establish the Login control
            CrmLogin ctrl = new CrmLogin();
            // Wire Event to login response. 
            ctrl.ConnectionToCrmCompleted += ctrl_ConnectionToCrmCompleted;
            // Show the dialog. 
            ctrl.ShowDialog();

            // Handel return. 
            if (ctrl.CrmConnectionMgr != null && ctrl.CrmConnectionMgr.CrmSvc != null && ctrl.CrmConnectionMgr.CrmSvc.IsReady)
            {
                Authenticated = true;
            }
            else
            {

            }

            #endregion
        }
        private void ctrl_ConnectionToCrmCompleted(object sender, EventArgs e)
        {
            if (sender is CrmLogin)
            {
                ((CrmLogin)sender).Close();
            }
        }
        private async void BrowseLocalFile()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Comma Separated Values (.csv)|*.csv";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true && !string.IsNullOrEmpty(dlg.FileName))
            {
                // Open document
                FilePath = dlg.FileName;
            }

        }
        private async void BrowseRecoveryFile()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            //dlg.Filter = "Comma Separated Values (.csv)|*.csv";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true && !string.IsNullOrEmpty(dlg.FileName))
            {
                // Open document
                RecoveryFilePath = dlg.FileName;
            }

        }
        private async void BrowseAgreements()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            //dlg.Filter = "Comma Separated Values (.csv)|*.csv";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true && !string.IsNullOrEmpty(dlg.FileName))
            {
                // Open document
                FilePath = dlg.FileName;
            }

        }
        private async void ProcessAgreementsDownload()
        {
            if (!string.IsNullOrEmpty(FilePath) && File.Exists(FilePath))
            {
                if (Helper.HasWritePermissionOnDir(System.IO.Path.GetDirectoryName(FilePath)))
                {
                    PluginExecutionContext = new Model.PluginExecutionContext();
                    PluginExecutionContext.Error = Error;
                    PluginExecutionContext.PluginExecutionSequence = ConfigurationManager.AppSettings.Get("DocumentDownloadSequence");
                    PluginExecutionContext.executionContext = new Model.ExecutionContext();
                    PluginExecutionContext.executionContext.fileDownLoadPath = ConfigurationManager.AppSettings.Get("DocumentDownloadPath");
                    PluginExecutionContext.executionContext.InputDataLength = ConfigurationManager.AppSettings.Get("InputDataLength");
                    CRMDetail = PluginExecutionContext.executionContext.CRMUser = new CRMUserDetail();
                    CurrentRunIdentifier = PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier = DateTime.Now.ToString("yyMMddHHmmssff"); ;
                    PluginExecutionContext.executionContext.AllowLogs = true;
                    PluginExecutionContext.executionContext.CRM_MaxInsertChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxInsertChunk"));
                    PluginExecutionContext.executionContext.CRM_MaxUploadFileSize = ConfigurationManager.AppSettings.Get("MaxUploadFileSize") != null ? int.Parse(ConfigurationManager.AppSettings.Get("MaxUploadFileSize")) : 0;
                    PluginExecutionContext.executionContext.CRM_MaxQueryConditionChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxQueryConditionChunk"));
                    PluginExecutionContext.executionContext.FilePath = FilePath;
                    PluginExecutionContext.SplitDelimeter =
                    PluginExecutionContext.executionContext.SplitDelimeter = Convert.ToChar(ConfigurationManager.AppSettings.Get("SplitDelimeter"));
                    PluginExecutionContext.executionContext.AzureBlobContainer = ConfigurationManager.AppSettings.Get("Container");
                    ExecutePlugins executePlugins = new ExecutePlugins();
                    PluginExecutionContext.executionContext.LogFilePath = Path.GetDirectoryName(FilePath) + Path.DirectorySeparatorChar + "/DocDownloadFileLog" + "_" + PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier + ".csv"; ;
                    LogDataFilePath = PluginExecutionContext.executionContext.LogFilePath;
                    PluginExecutionContext.executionContext.FileDataLog = new AsyncObservableCollection<FileLogEntry>();
                    PluginExecutionContext.executionContext.SyncLock = _syncLock;
                    BindingOperations.EnableCollectionSynchronization(PluginExecutionContext.executionContext.FileDataLog, _syncLock);
                    FileDownloadData = PluginExecutionContext.executionContext.FileDataLog;
                    PluginExecutionContext.executionContext.SkippedFileDataLog = new List<FileLogEntry>();
                    PluginExecutionContext.executionContext.ApplicationRunningState = new ApplicationState();
                    ApplicationState = PluginExecutionContext.executionContext.ApplicationRunningState;

                    Thread newWindowThread = new Thread(new ThreadStart(() =>
                    {
                        executePlugins.Execute(PluginExecutionContext);
                        System.Windows.Threading.Dispatcher.Run();
                    }));

                    // set the apartment state  
                    newWindowThread.SetApartmentState(ApartmentState.STA);

                    // make the thread a background thread  
                    newWindowThread.IsBackground = true;

                    // start the thread  
                    newWindowThread.Start();
                }
                else
                {
                    throw new Exception("No Write permission to save log file..");
                }
            }
        }
        private async void ProcessRecovery()
        {
            if (string.IsNullOrEmpty(RecoveryFilePath))
                RecoveryFilePath = LogDataFilePath;
            if (!string.IsNullOrEmpty(RecoveryFilePath) && File.Exists(RecoveryFilePath))
            {
                if (Helper.HasWritePermissionOnDir(System.IO.Path.GetDirectoryName(RecoveryFilePath)))
                {
                    PluginExecutionContext = new Model.PluginExecutionContext();
                    PluginExecutionContext.Error = Error;
                    PluginExecutionContext.PluginExecutionSequence = ConfigurationManager.AppSettings.Get("ManualRecoveryFlow");
                    PluginExecutionContext.executionContext = new Model.ExecutionContext();
                    CRMDetail = PluginExecutionContext.executionContext.CRMUser = new CRMUserDetail();
                    PluginExecutionContext.executionContext.AllowLogs = true;
                    PluginExecutionContext.executionContext.CRM_MaxInsertChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxInsertChunk"));
                    PluginExecutionContext.executionContext.CRM_MaxQueryConditionChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxQueryConditionChunk"));
                    PluginExecutionContext.executionContext.FilePath = RecoveryFilePath;
                    PluginExecutionContext.SplitDelimeter =
                    PluginExecutionContext.executionContext.SplitDelimeter = Convert.ToChar(ConfigurationManager.AppSettings.Get("SplitDelimeter"));
                    PluginExecutionContext.executionContext.AzureBlobContainer = ConfigurationManager.AppSettings.Get("Container");
                    ExecutePlugins executePlugins = new ExecutePlugins();
                    PluginExecutionContext.executionContext.FileDataLog = new ObservableCollection<FileLogEntry>();
                    BindingOperations.EnableCollectionSynchronization(PluginExecutionContext.executionContext.FileDataLog, _syncLock);

                    RecoveryGridData = PluginExecutionContext.executionContext.FileDataLog;

                    PluginExecutionContext.executionContext.LogFilePath = Path.GetDirectoryName(RecoveryFilePath) + Path.DirectorySeparatorChar + "/FileLog" + "_" + PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier;
                    LogDataFilePath = PluginExecutionContext.executionContext.LogFilePath;
                    PluginExecutionContext.executionContext.ApplicationRunningState = new ApplicationState();
                    ApplicationState = PluginExecutionContext.executionContext.ApplicationRunningState;

                    Thread newWindowThread = new Thread(new ThreadStart(() =>
                    {
                        executePlugins.Execute(PluginExecutionContext);
                        System.Windows.Threading.Dispatcher.Run();
                    }));

                    // set the apartment state  
                    newWindowThread.SetApartmentState(ApartmentState.STA);

                    // make the thread a background thread  
                    newWindowThread.IsBackground = true;

                    // start the thread  
                    newWindowThread.Start();
                }
            }
        }
        private async void ProcessAgreementDocuments()
        {
            //Process.IsEnabled = false;
            //btnbrowse.IsEnabled = false;
            //Validate.IsEnabled = false;
            //string fullPath = FileNameTextBox.Text;
            if (!string.IsNullOrEmpty(FilePath) && File.Exists(FilePath))
            {
                if (Helper.HasWritePermissionOnDir(System.IO.Path.GetDirectoryName(FilePath)))
                {
                    PluginExecutionContext = new Model.PluginExecutionContext();
                    PluginExecutionContext.Error = Error;
                    if (IsDailyMode)
                    {
                        PluginExecutionContext.PluginExecutionSequence = ConfigurationManager.AppSettings.Get("PluginExecutionSequenceDailyMode");
                    }
                    else
                    {
                        PluginExecutionContext.PluginExecutionSequence = ConfigurationManager.AppSettings.Get("PluginExecutionSequence");
                    }
                    PluginExecutionContext.RecoveryPluginExecutionSequence = ConfigurationManager.AppSettings.Get("RecoveryFlow");
                    PluginExecutionContext.executionContext = new Model.ExecutionContext();
                    PluginExecutionContext.executionContext.InputDataLength = ConfigurationManager.AppSettings.Get("InputDataLength");
                    PluginExecutionContext.executionContext.AgreementIdentifier = ConfigurationManager.AppSettings.Get("AgreementIdentifier");
                    CRMDetail = PluginExecutionContext.executionContext.CRMUser = new CRMUserDetail();
                    CurrentRunIdentifier = PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier = DateTime.Now.ToString("yyMMddHHmmssff"); ;
                    PluginExecutionContext.executionContext.AllowLogs = true;
                    PluginExecutionContext.executionContext.CRM_MaxInsertChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxInsertChunk"));
                    PluginExecutionContext.executionContext.CRM_MaxUploadFileSize = ConfigurationManager.AppSettings.Get("MaxUploadFileSize") != null ? int.Parse(ConfigurationManager.AppSettings.Get("MaxUploadFileSize")) : 0;
                    PluginExecutionContext.executionContext.CRM_MaxQueryConditionChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxQueryConditionChunk"));
                    PluginExecutionContext.executionContext.FilePath = FilePath;
                    PluginExecutionContext.SplitDelimeter =
                    PluginExecutionContext.executionContext.SplitDelimeter = Convert.ToChar(ConfigurationManager.AppSettings.Get("SplitDelimeter"));
                    PluginExecutionContext.executionContext.AzureBlobContainer = ConfigurationManager.AppSettings.Get("Container");
                    ExecutePlugins executePlugins = new ExecutePlugins();
                    PluginExecutionContext.executionContext.InvalidDataLines = new ObservableCollection<DataLine>();
                    BindingOperations.EnableCollectionSynchronization(PluginExecutionContext.executionContext.InvalidDataLines, _syncLockError);
                    ErrorGridData = PluginExecutionContext.executionContext.InvalidDataLines;
                    PluginExecutionContext.executionContext.ValidationOnly = false;
                    PluginExecutionContext.executionContext.InvalidDataFilePath = System.IO.Path.GetDirectoryName(FilePath) + "/InvalidData" + "_" + PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier + ".csv";
                    InvalidDataFilePath = PluginExecutionContext.executionContext.InvalidDataFilePath;
                    PluginExecutionContext.executionContext.LogFilePath = Path.GetDirectoryName(FilePath) + Path.DirectorySeparatorChar + "/FileLog" + "_" + PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier + ".csv"; ;
                    LogDataFilePath = PluginExecutionContext.executionContext.LogFilePath;
                    PluginExecutionContext.executionContext.FileDataLog = new AsyncObservableCollection<FileLogEntry>();
                    PluginExecutionContext.executionContext.SyncLock = _syncLock;
                    BindingOperations.EnableCollectionSynchronization(PluginExecutionContext.executionContext.FileDataLog, _syncLock);
                    GridData = PluginExecutionContext.executionContext.FileDataLog;
                    PluginExecutionContext.executionContext.SkippedFileDataLog = new List<FileLogEntry>();
                    PluginExecutionContext.executionContext.ApplicationRunningState = new ApplicationState();
                    ApplicationState = PluginExecutionContext.executionContext.ApplicationRunningState;

                    if (bool.Parse(ConfigurationManager.AppSettings.Get("IsAnnotationsEnabled")))
                    {
                        PluginExecutionContext.executionContext.IsAnnotationsEnabled = true;
                    }

                    if (IsDailyMode == true)
                    {
                        PluginExecutionContext.executionContext.AzureLogin = new AzureAuthenticationDetail();
                        PluginExecutionContext.executionContext.AzureLogin.ClientID = ConfigurationManager.AppSettings.Get("ClientID");
                        PluginExecutionContext.executionContext.AzureLogin.ClientSecretKey = ConfigurationManager.AppSettings.Get("ClientSecretKey");
                        PluginExecutionContext.executionContext.AzureLogin.DataMarketAccessURI = ConfigurationManager.AppSettings.Get("DataMarketAccessURI");
                        PluginExecutionContext.executionContext.AzureLogin.FileUploadURI = ConfigurationManager.AppSettings.Get("DocUploadURI");
                    }

                    Thread newWindowThread = new Thread(new ThreadStart(() =>
                    {
                        executePlugins.Execute(PluginExecutionContext);
                        System.Windows.Threading.Dispatcher.Run();
                    }));

                    // set the apartment state  
                    newWindowThread.SetApartmentState(ApartmentState.STA);

                    // make the thread a background thread  
                    newWindowThread.IsBackground = true;

                    // start the thread  
                    newWindowThread.Start();
                }
                else
                {
                    throw new Exception("No Write permission to save log file..");
                }
            }

        }
        private async void SaveAndValidateCorrectedAgreements()
        {
            if (!string.IsNullOrEmpty(InvalidDataFilePath) && Helper.HasWritePermissionOnDir(InvalidDataFilePath))
            {
                Model.PluginExecutionContext pluginExecutionContext = new Model.PluginExecutionContext();
                pluginExecutionContext.PluginExecutionSequence = "DataValidation";
                pluginExecutionContext.executionContext = new Model.ExecutionContext();
                pluginExecutionContext.executionContext.InvalidDataLines = new ObservableCollection<DataLine>();
                pluginExecutionContext.executionContext.CurrentUniqueRunIdentifier = CurrentRunIdentifier;
                pluginExecutionContext.executionContext.InputFileDataLines = new List<DataLine>();
                pluginExecutionContext.SplitDelimeter =
                pluginExecutionContext.executionContext.SplitDelimeter = Convert.ToChar(ConfigurationManager.AppSettings.Get("SplitDelimeter"));
                foreach (object obj in ErrorGridData)
                {
                    DataLine objLine = (DataLine)obj;
                    objLine.Line = string.Format("{1}{0}{2}{0}{3}{0}{4}", pluginExecutionContext.executionContext.SplitDelimeter,
                        objLine.AgreementNumber, objLine.FileLocation, objLine.BlobFileName, objLine.DocumentType);
                    objLine.Error = "";
                    pluginExecutionContext.executionContext.InputFileDataLines.Add(objLine);
                }
                pluginExecutionContext.executionContext.AllowLogs = true;
                pluginExecutionContext.executionContext.InvalidDataFilePath = InvalidDataFilePath;
                pluginExecutionContext.executionContext.FilePath = InvalidDataFilePath;
                pluginExecutionContext.executionContext.ApplicationRunningState = new ApplicationState();

                ExecutePlugins executePlugins = new ExecutePlugins();
                pluginExecutionContext.executionContext.InvalidDataLines = new ObservableCollection<DataLine>();
                BindingOperations.EnableCollectionSynchronization(pluginExecutionContext.executionContext.InvalidDataLines, _syncLockError);
                ErrorGridData = pluginExecutionContext.executionContext.InvalidDataLines;
                isErrorDataRefreshed = false;
                pluginExecutionContext.executionContext.ValidationOnly = true;
                pluginExecutionContext.executionContext.FileDataLog = new AsyncObservableCollection<FileLogEntry>();
                Task.Run(() => executePlugins.Execute(pluginExecutionContext));
            }
        }
        private async void OpenLogFile()
        {
            if (File.Exists(LogDataFilePath))
            {
                System.Diagnostics.Process.Start("notepad.exe", LogDataFilePath);
            }
        }
        private async void OpenCorrectedFile()
        {
            if (File.Exists(InvalidDataFilePath))
            {
                System.Diagnostics.Process.Start("notepad.exe", "'" + InvalidDataFilePath + "'");
            }
        }
        private async void StartFiltering()
        {
            StartFilter = !StartFilter;
            if (StartFilter)
            {
                objFilter = new Dictionary<EnumFilterType, ObservableCollection<CheckedListItem<object>>>();
                ItemFilters = new ObservableCollection<CheckedListItem<object>>();
                FilteredData = GridData;
            }
            else
            {
                CurrentFilterObject.Clear();
                objFilter.Clear();
            }
        }
        private async void OpenFilterPopUp()
        {
            CurrentFilterObject = new ObservableCollection<CheckedListItem<object>>();
            ItemFilters = new ObservableCollection<CheckedListItem<object>>();
            switch (CurrentFilterType)
            {
                case EnumFilterType.AgreementNumber:
                    {
                        if (objFilter.ContainsKey(EnumFilterType.AgreementNumber))
                        {
                            ItemFilters = objFilter[EnumFilterType.AgreementNumber];
                        }
                        else
                        {
                            foreach (string log in FilteredData.Select(x => x.AgreementNumber).Distinct())
                            {
                                ItemFilters.Add(new CheckedListItem<object>() { Item = log, IsChecked = false });
                            }
                            FilterPopupPlacementTarget = "btnAgreementFilter";
                        }
                        break;
                    }
                case EnumFilterType.BlobPath:
                    {
                        if (objFilter.ContainsKey(EnumFilterType.BlobPath))
                        {
                            ItemFilters = objFilter[EnumFilterType.BlobPath];
                        }
                        else
                        {
                            foreach (string log in FilteredData.Select(x => x.BlobSourcePath).Distinct())
                            {
                                ItemFilters.Add(new CheckedListItem<object>() { Item = log, IsChecked = false });
                            }
                            FilterPopupPlacementTarget = "btnBlobPath";
                        }
                        break;
                    }
                case EnumFilterType.DocumentVersion:
                    {
                        if (objFilter.ContainsKey(EnumFilterType.DocumentVersion))
                        {
                            ItemFilters = objFilter[EnumFilterType.DocumentVersion];
                        }
                        else
                        {
                            foreach (string log in FilteredData.Select(x => x.DocumentVersion).Distinct())
                            {
                                ItemFilters.Add(new CheckedListItem<object>() { Item = log, IsChecked = false });
                            }
                            FilterPopupPlacementTarget = "btnDocumentVersion";
                        }
                        break;
                    }
                case EnumFilterType.DocumentVersionDetail:
                    {
                        if (objFilter.ContainsKey(EnumFilterType.DocumentVersionDetail))
                        {
                            ItemFilters = objFilter[EnumFilterType.DocumentVersionDetail];
                        }
                        else
                        {
                            foreach (string log in FilteredData.Select(x => x.DocumentVersionDetail).Distinct())
                            {
                                ItemFilters.Add(new CheckedListItem<object>() { Item = log, IsChecked = false });
                            }
                            FilterPopupPlacementTarget = "btnDocumentVersionDetail";
                        }
                        break;
                    }
                case EnumFilterType.Status:
                    {
                        if (objFilter.ContainsKey(EnumFilterType.Status))
                        {
                            ItemFilters = objFilter[EnumFilterType.Status];
                        }
                        else
                        {
                            foreach (Model.UploadStatus log in FilteredData.Select(x => x.Status).Distinct())
                            {
                                ItemFilters.Add(new CheckedListItem<object>() { Item = log, IsChecked = false });
                            }
                            FilterPopupPlacementTarget = "btnStatus";
                        }
                        break;
                    }
                case EnumFilterType.IsBlobUploaded:
                    {
                        if (objFilter.ContainsKey(EnumFilterType.IsBlobUploaded))
                        {
                            ItemFilters = objFilter[EnumFilterType.IsBlobUploaded];
                        }
                        else
                        {
                            foreach (bool log in FilteredData.Select(x => x.IsProcessed).Distinct())
                            {
                                ItemFilters.Add(new CheckedListItem<object>() { Item = log, IsChecked = false });
                            }
                            FilterPopupPlacementTarget = "btnBlobUpload";
                        }
                        break;
                    }
                case EnumFilterType.DocumentType:
                    {
                        if (objFilter.ContainsKey(EnumFilterType.DocumentType))
                        {
                            ItemFilters = objFilter[EnumFilterType.DocumentType];
                        }
                        else
                        {
                            foreach (string log in FilteredData.Select(x => x.DocumentType).Distinct())
                            {
                                ItemFilters.Add(new CheckedListItem<object>() { Item = log, IsChecked = false });
                            }
                            FilterPopupPlacementTarget = "btnDocumentType";
                        }
                        break;
                    }
            }
            OpenFilterPopup = true;
            CurrentFilterObject.Clear();
        }
        private async void SelectAllFilter()
        {
            CurrentFilterObject.Clear();
            foreach (object obj in ItemFilters)
            {
                ((CheckedListItem<object>)obj).IsChecked = true;
                CurrentFilterObject.Add((CheckedListItem<object>)obj);
            }
        }
        private async void UnSelectAllFilter()
        {
            foreach (object obj in ItemFilters)
            {
                ((CheckedListItem<object>)obj).IsChecked = false;
                CurrentFilterObject.Remove((CheckedListItem<object>)obj);
            }
        }
        private async void ApplyFilter()
        {
            objFilter[CurrentFilterType] = CurrentFilterObject;
            FilteredData = GridData;
            foreach (EnumFilterType filterType in objFilter.Keys)
            {
                switch (filterType)
                {
                    case EnumFilterType.AgreementNumber:
                        {
                            FilteredData =
                            (from gridData in FilteredData
                             join filterData in objFilter[EnumFilterType.AgreementNumber]
                             on gridData.AgreementNumber equals filterData.Item
                             select gridData).ToList();
                            break;
                        }
                    case EnumFilterType.BlobPath:
                        {
                            FilteredData =
                            (from gridData in FilteredData
                             join filterData in objFilter[EnumFilterType.BlobPath]
                             on gridData.BlobSourcePath equals filterData.Item
                             select gridData).ToList();
                            break;
                        }
                    case EnumFilterType.DocumentType:
                        {
                            FilteredData =
                            (from gridData in FilteredData
                             join filterData in objFilter[EnumFilterType.DocumentType]
                             on gridData.DocumentType equals filterData.Item
                             select gridData).ToList();
                            break;
                        }
                    case EnumFilterType.DocumentVersion:
                        {
                            FilteredData =
                            (from gridData in FilteredData
                             join filterData in objFilter[EnumFilterType.DocumentVersion]
                             on gridData.DocumentVersion equals filterData.Item
                             select gridData).ToList();
                            break;
                        }
                    case EnumFilterType.DocumentVersionDetail:
                        {
                            FilteredData =
                            (from gridData in FilteredData
                             join filterData in objFilter[EnumFilterType.DocumentVersionDetail]
                             on gridData.DocumentVersionDetail equals filterData.Item
                             select gridData).ToList();
                            break;
                        }
                    case EnumFilterType.IsBlobUploaded:
                        {
                            FilteredData =
                            (from gridData in FilteredData
                             join filterData in objFilter[EnumFilterType.IsBlobUploaded]
                             on gridData.IsProcessed equals filterData.Item
                             select gridData).ToList();
                            break;
                        }
                    case EnumFilterType.Status:
                        {
                            FilteredData =
                            (from gridData in FilteredData
                             join filterData in objFilter[EnumFilterType.Status]
                             on gridData.Status equals filterData.Item
                             select gridData).ToList();
                            break;
                        }
                }
            }
            openFilterPopup = false;
            //popupFilter.IsOpen = false;
            //dataGrid.ItemsSource = FilteredData;
            //dataGrid.Items.Refresh();
        }
        private async void CancelFilter()
        {
            ItemFilters.Clear();
            OpenFilterPopup = false;
        }
        private async void Checked()
        {
            foreach (object obj in ItemFilters)
            {
                if (((CheckedListItem<object>)obj).IsChecked == true && !CurrentFilterObject.Contains((CheckedListItem<object>)obj))
                {
                    CurrentFilterObject.Add((CheckedListItem<object>)obj);
                }
            }
        }
        private async void Unchecked()
        {
            foreach (object obj in ItemFilters)
            {
                if (((CheckedListItem<object>)obj).IsChecked == false)
                {
                    CurrentFilterObject.Remove((CheckedListItem<object>)obj);
                }
            }
        }
    }
}
