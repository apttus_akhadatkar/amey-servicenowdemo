﻿using Apttus.Model;
using Apttus.PluginExecutionService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Apttus.AgreementDateCorrection.ViewModels
{
	class DateCorrectionViewModel : ViewModelBase
	{
		public object _syncLock = new object();
		public object _syncLockError = new object();
		public object _syncIncorrectLockError = new object();
		public enum EnumApplicationState
		{
			IdleState = 0,
			ApplicationStarted = 1,
			InputFileValidationInProgress = 2,
			InputFileValidationCompleted = 3,
			FileDataCapturingInProgress = 4,
			FileDataCapturingDone = 5,
			DataValidationInProgress = 6,
			DataValidationDone = 7,
			CRMConnectionInProgress = 8,
			CRMConnectionDone = 9,
			DocumentVersionSyncInProgress = 10,
			DocumentVersionSyncDone = 11,
			DocumentVersionDetailSyncInProgress = 12,
			DocumentVersionDetailSyncDone = 13,
			BlobFileUploadInProgress = 14,
			BlobFileUploadDone = 15
		}

		public DateCorrectionViewModel()
		{
			OpenFilterPopup = false;
			BrowseFile = new Command(BrowseLocalFile);
			ProcessAgreementsCommand = new Command(ProcessAgreements);
			StartFilter = false;
			//FilterData = new Command(StartFiltering);
			//OpenFilter = new Command(OpenFilterPopUp);
			//OpenErrorFile = new Command(OpenCorrectedFile);
			//OpenAppLogFile = new Command(OpenLogFile);
			//SaveAndValidate = new Command(SaveAndValidateCorrectedAgreements);
			FileUploadImageFileName = ConfigurationManager.AppSettings.Get("FileUploadImageName");
			ProgressArrowImageFileName = ConfigurationManager.AppSettings.Get("ProgressArrowImageName");
			DataValidationImageFileName = ConfigurationManager.AppSettings.Get("DataValidation");
			CRMSynchronizationImageFileName = ConfigurationManager.AppSettings.Get("CRMSynchronization");
			UploadToBlobImageFileName = ConfigurationManager.AppSettings.Get("BlobUpload");
			ApttusLogoImageFileName = ConfigurationManager.AppSettings.Get("ApttusLogo");
			FilterImageFileName = ConfigurationManager.AppSettings.Get("FilterImage");
			//SelectAllFilterColumnData = new Command(SelectAllFilter);
			//UnSelectAllFilterColumnData = new Command(UnSelectAllFilter);
			//ApplyFilterOnData = new Command(ApplyFilter);
			//CancelSelectedFilter = new Command(CancelFilter);
			//CheckedData = new Command(Checked);
			//UnCheckedData = new Command(Unchecked);
			//OpenRecoveryFile = new Command(BrowseRecoveryFile);
			//ProcessFileForRecovery = new Command(ProcessRecovery);
			//ConnectToCRM = new Command(LoginToCRM);
			//BrowseAgreementsForBulkDownload = new Command(BrowseAgreements);
			//ProcessAgreementsForBulkDownload = new Command(ProcessAgreementsDownload);
			IsDailyMode = true;
			ExecutionMode = ConfigurationManager.AppSettings.Get("Mode");
			IsMigrationMode = !IsDailyMode;
		}
		public Model.PluginExecutionContext PluginExecutionContext
		{
			get;
			set;
		}
		private bool openErrorPopup;
		public DateTime DateBefore { get; set; }
		public string ExecutionMode { get; set; }
		public bool IsMigrationMode { get; set; }
		public bool IsDailyMode { get; set; }
		private ErrorDetail error = new ErrorDetail();
		public ErrorDetail Error
		{
			get { return error; }
			set
			{
				if (error == value) return;
				error = value;
				RaisePropertyChanged();
			}
		}
		private CRMUserDetail crmDetail;
		public CRMUserDetail CRMDetail
		{
			get { return crmDetail; }
			set
			{
				if (crmDetail == value) return;
				crmDetail = value;
				RaisePropertyChanged();
			}
		}
		private string filePath = string.Empty;
		private bool authenticated = false;
		public bool Authenticated
		{
			get { return authenticated; }
			set
			{
				if (authenticated == value) return;
				authenticated = value;
				RaisePropertyChanged();
			}
		}
		private bool startFilter;
		public bool StartFilter
		{
			get { return startFilter; }
			set
			{
				if (startFilter == value) return;
				startFilter = value;
				RaisePropertyChanged();
			}
		}
		public bool isErrorDataRefreshed
		{
			get;
			set;
		}
		public string FilePath
		{
			get { return filePath; }
			set
			{
				if (filePath == value) return;
				filePath = value;
				RaisePropertyChanged();
			}
		}
		private string recoveryFileName;
		public string RecoveryFilePath
		{
			get { return recoveryFileName; }
			set
			{
				if (recoveryFileName == value) return;
				recoveryFileName = value;
				RaisePropertyChanged();
			}
		}

		public DateCorrectionViewModel CorrectionViewModel
		{
			get;
			set;
		}

		private ObservableCollection<FileLogEntry> gridData;
		public ObservableCollection<FileLogEntry> GridData
		{
			get { return gridData; }
			set
			{
				if (gridData == value) return;
				gridData = value;
				RaisePropertyChanged();
			}
		}
		private ObservableCollection<FileLogEntry> recoveryGridData;
		public ObservableCollection<FileLogEntry> RecoveryGridData
		{
			get { return recoveryGridData; }
			set
			{
				if (recoveryGridData == value) return;
				recoveryGridData = value;
				RaisePropertyChanged();
			}
		}
		private ObservableCollection<FileLogEntry> fileDownloadData;
		public ObservableCollection<FileLogEntry> FileDownloadData
		{
			get { return fileDownloadData; }
			set
			{
				if (fileDownloadData == value) return;
				fileDownloadData = value;
				RaisePropertyChanged();
			}
		}

		public IEnumerable<FileLogEntry> FilteredData
		{
			get;
			set;
		}
		private ObservableCollection<DataLine> errorGridData;
		public ObservableCollection<DataLine> ErrorGridData
		{
			get { return errorGridData; }
			set
			{
				if (errorGridData == value) return;
				errorGridData = value;
				RaisePropertyChanged();
			}
		}
		private ApplicationState applicationState;
		public ApplicationState ApplicationState
		{
			get { return applicationState; }
			set
			{
				if (applicationState == value) return;
				applicationState = value;
				RaisePropertyChanged();
			}
		}

		public EnumFilterType CurrentFilterType
		{
			get;
			set;
		}
		public ObservableCollection<CheckedListItem<object>> CurrentFilterObject
		{
			get;
			set;
		}
		public Dictionary<EnumFilterType, ObservableCollection<CheckedListItem<object>>> objFilter
		{
			get;
			set;
		}
		public string InvalidDataFilePath
		{
			get;
			set;
		}
		private string logDataFilePath;
		public string LogDataFilePath
		{
			get { return logDataFilePath; }
			set
			{
				if (logDataFilePath == value) return;
				logDataFilePath = value;
				RaisePropertyChanged();
			}
		}
		public string FileUploadImageFileName
		{
			get;
			set;
		}
		public string ProgressArrowImageFileName
		{
			get;
			set;
		}
		public string DataValidationImageFileName
		{
			get;
			set;
		}
		public string CRMSynchronizationImageFileName
		{
			get;
			set;
		}
		public string UploadToBlobImageFileName
		{
			get;
			set;
		}
		public string ApttusLogoImageFileName
		{
			get;
			set;
		}
		public string FilterImageFileName
		{
			get;
			set;
		}
		//public Command ConnectToCRM
		//{
		//	get; private set;
		//}
		//public Command OpenRecoveryFile
		//{
		//	get;
		//	set;
		//}
		//public Command ProcessFileForRecovery
		//{
		//	get;
		//	set;
		//}
		//public Command ProcessAgreementsCommand
		//{
		//	get;
		//	private set;
		//}
		public Command BrowseFile
		{
			get; private set;
		}
		public Command ProcessAgreementsCommand
		{
			get; private set;
		}
		//public Command OpenFilter
		//{
		//	get; private set;
		//}
		//public Command SelectAllFilterColumnData
		//{
		//	get; private set;
		//}
		//public Command UnSelectAllFilterColumnData
		//{
		//	get; private set;
		//}
		//public Command ApplyFilterOnData
		//{
		//	get; private set;
		//}
		//public Command CancelSelectedFilter
		//{
		//	get; private set;
		//}
		//public Command CheckedData
		//{
		//	get; private set;
		//}
		//public Command UnCheckedData
		//{
		//	get; private set;
		//}
		//public Command SaveAndValidate
		//{
		//	get; private set;
		//}
		//public Command OpenErrorFile
		//{
		//	get; private set;
		//}
		//public Command OpenAppLogFile
		//{
		//	get; private set;
		//}
		//public Command BrowseAgreementsForBulkDownload
		//{
		//	get; private set;
		//}
		//public Command ProcessAgreementsForBulkDownload
		//{
		//	get; private set;
		//}
		public string FilterPopupPlacementTarget
		{
			get;
			set;
		}
		private bool openFilterPopup;
		public bool OpenFilterPopup
		{
			get { return openFilterPopup; }
			set
			{
				if (openFilterPopup == value) return;
				openFilterPopup = value;
				RaisePropertyChanged();
			}
		}
		public string CurrentRunIdentifier
		{
			get;
			set;
		}
		private ObservableCollection<CheckedListItem<object>> filterItem;
		public ObservableCollection<CheckedListItem<object>> ItemFilters
		{
			get { return filterItem; }
			set
			{
				if (filterItem == value) return;
				filterItem = value;
				RaisePropertyChanged();
			}
		}
		private EnumEnvironment environment;
		public EnumEnvironment Environment
		{
			get { return environment; }
			set
			{
				if (environment == value) return;
				environment = value;
				RaisePropertyChanged();
			}
		}
		private async void BrowseLocalFile()
		{
			Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

			// Set filter for file extension and default file extension
			dlg.DefaultExt = ".txt";
			dlg.Filter = "Comma Separated Values (.csv)|*.csv";

			// Display OpenFileDialog by calling ShowDialog method
			Nullable<bool> result = dlg.ShowDialog();

			// Get the selected file name and display in a TextBox
			if (result == true && !string.IsNullOrEmpty(dlg.FileName))
			{
				// Open document
				FilePath = dlg.FileName;
			}

		}
		private async void ProcessAgreements()
		{
			//Process.IsEnabled = false;
			//btnbrowse.IsEnabled = false;
			//Validate.IsEnabled = false;
			//string fullPath = FileNameTextBox.Text;
			PluginExecutionContext = new Model.PluginExecutionContext();
			PluginExecutionContext.Error = Error;

			PluginExecutionContext.PluginExecutionSequence = ConfigurationManager.AppSettings.Get("PluginExecutionSequence");

			//PluginExecutionContext.RecoveryPluginExecutionSequence = ConfigurationManager.AppSettings.Get("RecoveryFlow");
			PluginExecutionContext.executionContext = new Model.ExecutionContext();
			PluginExecutionContext.executionContext.BeforeDate = DateBefore;
			PluginExecutionContext.executionContext.InputDataLength = ConfigurationManager.AppSettings.Get("InputDataLength");
			PluginExecutionContext.executionContext.AgreementIdentifier = ConfigurationManager.AppSettings.Get("AgreementIdentifier");
			CRMDetail = PluginExecutionContext.executionContext.CRMUser = new CRMUserDetail();
			CurrentRunIdentifier = PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier = DateTime.Now.ToString("yyMMddHHmmssff"); ;
			PluginExecutionContext.executionContext.AllowLogs = true;
			PluginExecutionContext.executionContext.CRM_MaxInsertChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxInsertChunk"));
			PluginExecutionContext.executionContext.CRM_MaxUploadFileSize = ConfigurationManager.AppSettings.Get("MaxUploadFileSize") != null ? int.Parse(ConfigurationManager.AppSettings.Get("MaxUploadFileSize")) : 0;
			PluginExecutionContext.executionContext.CRM_MaxQueryConditionChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxQueryConditionChunk"));
			//PluginExecutionContext.executionContext.FilePath = FilePath;
			PluginExecutionContext.SplitDelimeter =
			PluginExecutionContext.executionContext.SplitDelimeter = Convert.ToChar(ConfigurationManager.AppSettings.Get("SplitDelimeter"));
			//PluginExecutionContext.executionContext.AzureBlobContainer = ConfigurationManager.AppSettings.Get("Container");
			ExecutePlugins executePlugins = new ExecutePlugins();
			PluginExecutionContext.executionContext.InvalidDataLines = new ObservableCollection<DataLine>();
			BindingOperations.EnableCollectionSynchronization(PluginExecutionContext.executionContext.InvalidDataLines, _syncLockError);
			ErrorGridData = PluginExecutionContext.executionContext.InvalidDataLines;
			//PluginExecutionContext.executionContext.ValidationOnly = false;
			//PluginExecutionContext.executionContext.InvalidDataFilePath = System.IO.Path.GetDirectoryName(FilePath) + "/InvalidData" + "_" + PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier + ".csv";
			//InvalidDataFilePath = PluginExecutionContext.executionContext.InvalidDataFilePath;
			//PluginExecutionContext.executionContext.LogFilePath = Path.GetDirectoryName(FilePath) + Path.DirectorySeparatorChar + "/FileLog" + "_" + PluginExecutionContext.executionContext.CurrentUniqueRunIdentifier + ".csv"; ;
			//LogDataFilePath = PluginExecutionContext.executionContext.LogFilePath;
			PluginExecutionContext.executionContext.FileDataLog = new AsyncObservableCollection<FileLogEntry>();
			PluginExecutionContext.executionContext.SyncLock = _syncLock;
			BindingOperations.EnableCollectionSynchronization(PluginExecutionContext.executionContext.FileDataLog, _syncLock);
			GridData = PluginExecutionContext.executionContext.FileDataLog;
			//PluginExecutionContext.executionContext.SkippedFileDataLog = new List<FileLogEntry>();
			PluginExecutionContext.executionContext.ApplicationRunningState = new ApplicationState();
			ApplicationState = PluginExecutionContext.executionContext.ApplicationRunningState;

			//if (bool.Parse(ConfigurationManager.AppSettings.Get("IsAnnotationsEnabled")))
			//{
			//	PluginExecutionContext.executionContext.IsAnnotationsEnabled = true;
			//}

			if (IsDailyMode == true)
			{
				PluginExecutionContext.executionContext.AzureLogin = new AzureAuthenticationDetail();
				PluginExecutionContext.executionContext.AzureLogin.ClientID = ConfigurationManager.AppSettings.Get("ClientID");
				PluginExecutionContext.executionContext.AzureLogin.ClientSecretKey = ConfigurationManager.AppSettings.Get("ClientSecretKey");
				PluginExecutionContext.executionContext.AzureLogin.DataMarketAccessURI = ConfigurationManager.AppSettings.Get("DataMarketAccessURI");
				PluginExecutionContext.executionContext.AzureLogin.FileUploadURI = ConfigurationManager.AppSettings.Get("DocUploadURI");
			}

			Thread newWindowThread = new Thread(new ThreadStart(() =>
			{
				executePlugins.Execute(PluginExecutionContext);
				System.Windows.Threading.Dispatcher.Run();
			}));

			// set the apartment state  
			newWindowThread.SetApartmentState(ApartmentState.STA);

			// make the thread a background thread  
			newWindowThread.IsBackground = true;

			// start the thread  
			newWindowThread.Start();

		}
	}
}
