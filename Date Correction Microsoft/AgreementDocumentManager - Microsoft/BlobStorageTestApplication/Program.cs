﻿using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace BlobStorageTestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //string fullPath = Console.ReadLine();
            //if (File.Exists(fullPath))
            //{
            //    if (Helper.HasWritePermissionOnDir(Path.GetDirectoryName(fullPath)))
            //    {
            //        Model.PluginExecutionContext pluginExecutionContext = new Model.PluginExecutionContext();
            //        pluginExecutionContext.executionContext = new Model.ExecutionContext();
            //        pluginExecutionContext.executionContext.AllowLogs = true;
            //        pluginExecutionContext.executionContext.CRM_MaxInsertChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxInsertChunk"));
            //        pluginExecutionContext.executionContext.CRM_MaxQueryConditionChunk = int.Parse(ConfigurationManager.AppSettings.Get("MaxQueryConditionChunk"));
            //        pluginExecutionContext.executionContext.CRM_Password = ConfigurationManager.AppSettings.Get("Password");
            //        pluginExecutionContext.executionContext.CRM_ServiceUrl = ConfigurationManager.AppSettings.Get("ServiceUrl");
            //        pluginExecutionContext.executionContext.CRM_Username = ConfigurationManager.AppSettings.Get("Username");
            //        pluginExecutionContext.executionContext.FilePath = fullPath;
            //        pluginExecutionContext.executionContext.SplitDelimeter = char.Parse(ConfigurationManager.AppSettings.Get("SplitDelimeter"));
            //    }
            //    else
            //    {
            //        throw new Exception("No Write permission to save log file..");
            //    }
            //}

            AuthenticationHelper helper = new AuthenticationHelper();
            helper.UploadAgreementDocumentApi(new Guid(), "", "", null);
            //helper.UploadAgreementDocumentApi();
        }
    }
    public class AuthenticationHelper
    {
        static AdmAccessTokenForHelper admToken;
        static string headerValue;

        private string ClientId = null;
        private string ClientSecret = null;
        private string DatamarketAccessUri = "https://login.windows.net/microsoft.com/oauth2/token";
        private string APIUri = "https://msft-dev1.apttuscloud.io/";
        private string ResourceId = null;
        private string GetAuthorizationToken(string clientId, string clientSecret, string DatamarketAccessUri, string resourceId)
        {
            AdmAuthenticationForHelper admAuth = new AdmAuthenticationForHelper("83c03f17-333a-48a1-aade-ddff15638884", "25L1VaQSeJmN2P8sC4LIr3f2YK418J7yjLmswEAM62E=", DatamarketAccessUri, "83c03f17-333a-48a1-aade-ddff15638884");
            try
            {
                admToken = admAuth.GetAccessToken();

                // Create a header with the access_token property of the returned token
                headerValue = "Bearer " + admToken.access_token;
                return headerValue;

            }
            catch (WebException e)
            {
                ProcessWebException(e);
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private static void ProcessWebException(WebException e)
        {

            // Obtain detailed error information
            string strResponse = string.Empty;
            using (HttpWebResponse response = (HttpWebResponse)e.Response)
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(responseStream, System.Text.Encoding.ASCII))
                    {
                        strResponse = sr.ReadToEnd();
                    }
                }
            }
        }
        public string DetectMethod(string authToken, string json, string methodType, string uri, ITracingService tracer)
        {
            //tracer.Trace("Entered in DetectMethod ! ");
            #region sample Code
            //string output = String.Empty;
            //HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            //httpWebRequest.Method = methodType;
            //httpWebRequest.Headers.Add("Authorization", authToken);

            ////byte[] data = Encoding.UTF8.GetBytes(json); ;

            //httpWebRequest.ContentType = "multipart/form-data;";
            //httpWebRequest.Accept = "application/json";
            ////httpWebRequest.ContentLength = data.Length;

            //FileStream fileStream = new FileStream("C:\\Users\\Tapankumar Patel\\Desktop\\Sample MSA ACME_Original_MSA_2017-01-19.docx", FileMode.Open, FileAccess.Read);
            //byte[] buffer = new byte[4096];
            //int bytesRead = 0;
            //Stream rs = httpWebRequest.GetRequestStream();
            //while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            //{
            //    rs.Write(buffer, 0, bytesRead);
            //}
            //fileStream.Close();

            //HttpWebResponse myHttpWebResponse = null;
            ////tracer.Trace("0");
            //try
            //{
            //    //tracer.Trace("1");
            //    myHttpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //    //tracer.Trace("2");
            //    Stream responseStream = myHttpWebResponse.GetResponseStream();
            //    //tracer.Trace("3");
            //    StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
            //    //tracer.Trace("4");
            //    string pageContent = myStreamReader.ReadToEnd();
            //    //tracer.Trace("5");
            //    myStreamReader.Close();
            //    responseStream.Close();

            //    myHttpWebResponse.Close();



            //    #region MultiPart Data

            //    #endregion


            //    return pageContent;


            //}
            //    catch (WebException wex)
            //    {
            //        string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
            //        return wex.Message.ToString();
            //    }
            //    catch (Exception ex)
            //    {
            //        return ex.Message.ToString();
            //    }

            //    finally
            //    {
            //        if (myHttpWebResponse != null)
            //        {
            //            myHttpWebResponse.Close();
            //            myHttpWebResponse = null;
            //        }
            //    }
            #endregion

            try
            {

                HttpClient httpClient = new HttpClient();
                // This is the API URL that we need to invoke
                string uploadServiceBaseAddress = "https://msft-dev1.apttuscloud.io" + "/api/clm/v1/agreements/" + "420e31de-f58a-e611-80e3-c4346bac219c" + "/documents?isVersioned=true"; //"http://localhost/SampleAPI/api/AgreementDocument/420e31de-f58a-e611-80e3-c4346bac219c?isVersioned=true&isImportOffline=false";// //" // //////////
                // Read the files from disk 
                string file = "C:\\Users\\Tapankumar Patel\\Desktop\\Sample MSA ACME_Original_MSA_2017-01-19.docx";
                var fileStream = File.Open(file, FileMode.Open);

                string file1 = "C:\\Users\\Tapankumar Patel\\Desktop\\Quote Template (Project).docx";
                var fileStream1 = File.Open(file1, FileMode.Open);

                Dictionary<string, object> metadata = new Dictionary<string, object>();
                metadata.Add("Notes", "Testing");
                metadata.Add("DocumentType", new { key = 100000000 });

                Dictionary<string, object> metadata1 = new Dictionary<string, object>();
                metadata1.Add("Notes", "Testing");
                metadata1.Add("DocumentType", new { key = 100000001 });
                var fileInfo = new FileInfo(file);
                var fileInfo1 = new FileInfo(file1);
                var content = new MultipartFormDataContent("---------------------------7e132f116048a");
                //content.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
                //content.Headers.ContentType.MediaType = "multipart/form-data";
                StreamContent sc = new StreamContent(fileStream);
                sc.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                content.Add(sc, "\"2\"", string.Format("\"{0}\"", fileInfo.Name));
                content.Add(new StringContent(JsonConvert.SerializeObject(metadata)), "\"2\"");//"{ 'Notes':'Testing','DocumentType':{ 'key':100000000} }"//JsonConvert.SerializeObject(metadata)
                StreamContent sc1 = new StreamContent(fileStream1);
                sc1.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                //StreamContent sc1 = new StreamContent(fileStream);
                sc1.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                content.Add(sc1, "\"1\"", string.Format("\"{0}\"", fileInfo1.Name));
                content.Add(new StringContent(JsonConvert.SerializeObject(metadata1)), "\"1\"");//"{ 'Notes':'Testing','DocumentType':{ 'key':100000000} }"//JsonConvert.SerializeObject(metadata)

                httpClient.DefaultRequestHeaders.Add("Authorization", authToken);
                
                Task taskUpload = httpClient.PostAsync(uploadServiceBaseAddress, content).ContinueWith(task =>
                {
                    if (task.Status == TaskStatus.RanToCompletion)
                    {
                        var response = task.Result;

                        if (response.IsSuccessStatusCode)
                        {
                            // Read other header values if you want..
                            foreach (var header in response.Content.Headers)
                            {
                                Console.WriteLine("{0}: {1}", header.Key, string.Join(",", header.Value));
                            }
                        }
                        else
                        {
                            Console.WriteLine("Status Code: {0} - {1}", response.StatusCode, response.ReasonPhrase);
                            Console.WriteLine("Response Body: {0}", response.Content.ReadAsStringAsync().Result);
                        }
                    }

                    fileStream.Dispose();
                });

                taskUpload.Wait();

                httpClient.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return "";
        }

        public string UploadAgreementDocumentApi(Guid agreementId, string body, string secureConfig, ITracingService tracer)
        {
            XmlDocument doc = new XmlDocument();
            //doc.LoadXml(secureConfig);

            string uri = APIUri + "/api/clm/v1/agreements/" + "0FE01BF8-D02F-E711-80ED-C4346BACFBBC" + "/Documents?isVersioned=true";
            //tracer.Trace(uri);
            //tracer.Trace(ClientId);
            //tracer.Trace(DatamarketAccessUri);

            string token = String.Empty;
            string response = String.Empty;

            // Get Authorization Token
            token = GetAuthorizationToken(ClientId, ClientSecret, DatamarketAccessUri, ResourceId);
            //tracer.Trace(token);


            // To Fetch API Response
            response = DetectMethod(token, body, "POST", uri, tracer);
            //tracer.Trace(response);

            return response;
        }
    }


    [DataContract]
    public class AdmAccessTokenForHelper
    {
        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public string token_type { get; set; }
        [DataMember]
        public string expires_in { get; set; }
        [DataMember]
        public string scope { get; set; }
    }
    public class AdmAuthenticationForHelper
    {
        //    public static readonly string DatamarketAccessUri = "https://login.windows.net/api.apttuscloud.io/oauth2/token";
        public static readonly string DatamarketAccessUri;
        private string clientId;
        private string clientSecret;
        private string request;
        private AdmAccessTokenForHelper token;
        private Timer accessTokenRenewer;
        //Access token expires every 10 minutes. Renew it every 9 minutes only.
        private const int RefreshTokenDuration = 9;
        public AdmAuthenticationForHelper(string clientId, string clientSecret, string DatamarketAccessUri, string resourceId)
        {
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            //If clientid or client secret has special characters, encode before sending request
            this.request = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&resource=" + resourceId, System.Uri.EscapeDataString(clientId), System.Uri.EscapeDataString(clientSecret));
            this.token = HttpPost(DatamarketAccessUri, this.request);
            //renew the token every specfied minutes
            accessTokenRenewer = new Timer(new TimerCallback(OnTokenExpiredCallback), this, TimeSpan.FromMinutes(RefreshTokenDuration), TimeSpan.FromMilliseconds(-1));
        }
        public AdmAccessTokenForHelper GetAccessToken()
        {
            return this.token;
        }
        private void RenewAccessToken()
        {
            AdmAccessTokenForHelper newAccessToken = HttpPost(DatamarketAccessUri, this.request);
            //swap the new token with old one
            //Note: the swap is thread unsafe
            this.token = newAccessToken;
            Console.WriteLine(string.Format("Renewed token for user: {0} is: {1}", this.clientId, this.token.access_token));
        }
        private void OnTokenExpiredCallback(object stateInfo)
        {
            try
            {
                RenewAccessToken();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                try
                {
                    accessTokenRenewer.Change(TimeSpan.FromMinutes(RefreshTokenDuration), TimeSpan.FromMilliseconds(-1));
                }
                catch (Exception ex)
                {
                }
            }
        }
        private AdmAccessTokenForHelper HttpPost(string DatamarketAccessUri, string requestDetails)
        {
            //Prepare OAuth request 
            WebRequest webRequest = WebRequest.Create(DatamarketAccessUri);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = Encoding.ASCII.GetBytes(requestDetails);
            webRequest.ContentLength = bytes.Length;
            using (Stream outputStream = webRequest.GetRequestStream())
            {
                outputStream.Write(bytes, 0, bytes.Length);
            }
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AdmAccessTokenForHelper));
                //Get deserialized object from JSON stream
                AdmAccessTokenForHelper token = (AdmAccessTokenForHelper)serializer.ReadObject(webResponse.GetResponseStream());
                return token;
            }
        }
    }
}
