﻿using Apttus.LoginControl.LoginWindow;
using Apttus.Model;
using Apttus.Plugins.Helpers;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.ServiceModel.Description;

namespace Apttus.Plugins
{
	internal class Helper
	{
		static IOrganizationService ServiceProxy;
		static List<int> fileSizeList;
		public void ConnectToMSCRM(CRMUserDetail CRMDetail)
		{
			try
			{
				// Establish the Login control
				CrmLogin ctrl = new CrmLogin();
				// Wire Event to login response. 
				ctrl.ConnectionToCrmCompleted += ctrl_ConnectionToCrmCompleted;
				// Show the dialog. 
				ctrl.ShowDialog();

				// Handel return. 
				if (ctrl.CrmConnectionMgr != null && ctrl.CrmConnectionMgr.CrmSvc != null && ctrl.CrmConnectionMgr.CrmSvc.IsReady)
				{
					ctrl.CrmConnectionMgr.CrmSvc.OrganizationServiceProxy.Timeout = new TimeSpan(3, 20, 0);
					ServiceProxy = (IOrganizationService)(ctrl.CrmConnectionMgr.CrmSvc.OrganizationServiceProxy);

					CRMDetail.LoggedInUser = ctrl.CrmConnectionMgr.CrmSvc.OrganizationServiceProxy.ClientCredentials.UserName.UserName.ToString();
					CRMDetail.CRMDetail = ctrl.CrmConnectionMgr.ConnectedOrgFriendlyName;
					CRMDetail.CRM_ServiceUrl = ctrl.CrmConnectionMgr.ConnectedOrgUniqueName;
				}
				else
				{
					throw new Exception("Can't connect to CRM.");
				}
				//ClientCredentials credentials = new ClientCredentials();
				//credentials.UserName.UserName = UserName;
				//credentials.UserName.Password = Password;
				//Uri serviceUri = new Uri(SoapOrgServiceUri);
				//OrganizationServiceProxy proxy = new OrganizationServiceProxy(serviceUri, null, credentials, null);
				//proxy.EnableProxyTypes();
				//proxy.Timeout = new TimeSpan(0, 20, 0); //20 minutes
				//ServiceProxy = (IOrganizationService)proxy;

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		public void GetRelationships()
		{
			RetrieveEntityRequest retrieveBankAccountEntityRequest = new RetrieveEntityRequest
			{
				EntityFilters = EntityFilters.All,
				LogicalName = "apttus_apts_agreement"
			};
			RetrieveEntityResponse retrieveBankAccountEntityResponse = (RetrieveEntityResponse)ServiceProxy.Execute(retrieveBankAccountEntityRequest);

			var oneToNRelationships = retrieveBankAccountEntityResponse.EntityMetadata.OneToManyRelationships;
			var relatedEntities = retrieveBankAccountEntityResponse.EntityMetadata.CanBeRelatedEntityInRelationship;

			Dictionary<string, string> attributesData = new Dictionary<string, string>();

			RetrieveEntityRequest metaDataRequest = new RetrieveEntityRequest();
			RetrieveEntityResponse metaDataResponse = new RetrieveEntityResponse();
			metaDataRequest.EntityFilters = EntityFilters.Attributes;
			metaDataRequest.LogicalName = "apttus_apts_agreement";
			metaDataResponse = (RetrieveEntityResponse)ServiceProxy.Execute(metaDataRequest);

			foreach (AttributeMetadata a in metaDataResponse.EntityMetadata.Attributes)
			{
				//if more than one label for said attribute, get the one matching the languade code we want...
				if (a.DisplayName.LocalizedLabels.Count() > 1)
					attributesData.Add(a.LogicalName, a.DisplayName.LocalizedLabels.SingleOrDefault().Label);

				//else, get the only one available regardless of languade code...
				if (a.DisplayName.LocalizedLabels.Count() == 1)
					attributesData.Add(a.LogicalName, a.DisplayName.LocalizedLabels[0].Label);
			}

			//return attributesData;
		}
		public void ExecuteMultiple(List<Entity> entityList, EnumOperationType enumOperationType)
		{
			List<OrganizationRequest> requestWithResults = new List<OrganizationRequest>();
			foreach (var entity in entityList)
			{
				if (enumOperationType == EnumOperationType.Insert)
				{
					CreateRequest createRequest = new CreateRequest { Target = entity };
					requestWithResults.Add(createRequest);
				}
				else if (enumOperationType == EnumOperationType.Update)
				{
					UpdateRequest createRequest = new UpdateRequest { Target = entity };
					requestWithResults.Add(createRequest);
				}
				else if (enumOperationType == EnumOperationType.Delete)
				{
					DeleteRequest deleteRequest = new DeleteRequest { Target = entity.ToEntityReference() };
					requestWithResults.Add(deleteRequest);
				}
			}
			HandleRequests.Execute(ServiceProxy, requestWithResults, false, true);
		}
		private void ctrl_ConnectionToCrmCompleted(object sender, EventArgs e)
		{
			if (sender is CrmLogin)
			{
				((CrmLogin)sender).Close();
			}
		}
		internal ExecuteMultipleResponse ChunkAndExecute(List<Entity> entityList, int MaxInsertChunk, List<int> fileSizeList = null)
		{
			return ChunkAndExecute(entityList, EnumOperationType.Insert, MaxInsertChunk, fileSizeList);
		}
		internal ExecuteMultipleResponse ChunkAndExecute(List<Entity> entityList, EnumOperationType enumOperationType, int MaxInsertChunk, List<int> fileSizeList = null)
		{

			ExecuteMultipleResponse allResults = null;
			ExecuteMultipleResponse chunkResult = new ExecuteMultipleResponse();
			int MaxTransactionRecords = MaxInsertChunk; //300
			int totalFileSize;
			int fileSizeToUpload = 0;
			if (fileSizeList != null && fileSizeList.Count > 0 && fileSizeList.Count == entityList.Count)
			{
				totalFileSize = fileSizeList.Sum();
				if (Convert.ToInt32(totalFileSize) <= Convert.ToInt32(MaxInsertChunk * 1024))
				{
					allResults = ExecuteChunk(entityList, enumOperationType);
				}
				else
				{
					int fileCount = 0;
					foreach (int fileSize in fileSizeList)
					{
						if (fileSizeToUpload < Convert.ToInt32(MaxInsertChunk * 1024))
						{
							fileSizeToUpload = fileSizeToUpload + fileSize;
							if (fileSizeToUpload > (Convert.ToInt32(MaxInsertChunk * 1024) + (Convert.ToInt32((MaxInsertChunk * 1024) * (25 / 100)))) && fileCount > 0)
							{
								break;
							}
							fileCount++;
						}
						else
						{
							break;
						}
					}
					List<Entity> MaxRecordsChunk = entityList.GetRange(0, fileCount);

					// Part 1 - Chunk of Max Records
					//Mark as in progress
					chunkResult = ExecuteChunk(MaxRecordsChunk, enumOperationType);
					//Mark as completed / Error with error message
					allResults = appendExecuteResponse(allResults, chunkResult);

					// Part 2 - Chunk of Remaining Records - Recursion
					List<Entity> RemainingRecords = entityList.GetRange(fileCount, entityList.Count - fileCount);
					List<int> RemainingFileSize = fileSizeList.GetRange(fileCount, fileSizeList.Count - fileCount);
					chunkResult = ChunkAndExecute(RemainingRecords, enumOperationType, MaxTransactionRecords, RemainingFileSize);
					allResults = appendExecuteResponse(allResults, chunkResult);
				}
			}


			else
			{
				if (entityList.Count <= MaxTransactionRecords)
				{
					allResults = ExecuteChunk(entityList, enumOperationType);
				}
				else if (entityList.Count > MaxTransactionRecords)
				{
					List<Entity> MaxRecordsChunk = entityList.GetRange(0, MaxTransactionRecords);

					// Part 1 - Chunk of Max Records
					//Mark as in progress
					chunkResult = ExecuteChunk(MaxRecordsChunk, enumOperationType);
					//Mark as completed / Error with error message
					allResults = appendExecuteResponse(allResults, chunkResult);

					// Part 2 - Chunk of Remaining Records - Recursion
					List<Entity> RemainingRecords = entityList.GetRange(MaxTransactionRecords, entityList.Count - MaxTransactionRecords);
					chunkResult = ChunkAndExecute(RemainingRecords, enumOperationType, MaxTransactionRecords);
					allResults = appendExecuteResponse(allResults, chunkResult);
				}
			}
			return allResults;
		}
		private ExecuteMultipleResponse appendExecuteResponse(ExecuteMultipleResponse allResults, ExecuteMultipleResponse chunkResult)
		{
			if (allResults == null)
			{
				allResults = new ExecuteMultipleResponse();
				allResults = chunkResult;
			}
			else
				allResults.Responses.AddRange(chunkResult.Responses);
			return allResults;
		}
		internal EntityCollection ChunkAndFetchOndateEntity(String entityName, int MaxQueryConditionChunk)
		{
			EntityCollection EntityList = new EntityCollection();

			EntityCollection ChunkEntityList;
			int conditionChunk = MaxQueryConditionChunk;//2000
			DateTime afterDate = new DateTime(2017, 8, 1, 0, 0, 0);
            DateTime beforeDate = new DateTime(2017, 9, 1, 0, 0, 0);
            QueryExpression query = null;
				if (entityName.Equals("apttus_apts_agreement"))
					query = BuildAgreemnetQuery(null, afterDate,beforeDate);
				ChunkEntityList = RetrieveMultiple(query);
				EntityList.Entities.AddRange(ChunkEntityList.Entities);

			return EntityList;
		}
		internal EntityCollection ChunkAndFetchEntity(List<string> agreeNumList, String entityName, int MaxQueryConditionChunk)
		{
			EntityCollection EntityList = new EntityCollection();
			if (agreeNumList.Count > 0)
			{
				EntityCollection ChunkEntityList;
				int conditionChunk = MaxQueryConditionChunk;//2000
				int totalRecords = agreeNumList.Count;
				int chunkCount = totalRecords > conditionChunk ? conditionChunk : totalRecords;
				int offset = 0;
				do
				{
					List<string> agreeNumChunkList = agreeNumList.GetRange(offset, chunkCount);
					DateTime afterDate = new DateTime(2017, 8, 1, 0, 0, 0);
                    DateTime beforeDate = new DateTime(2017, 9, 1, 0, 0, 0);
                    QueryExpression query = null;
					if (entityName.Equals("apttus_apts_agreement"))
						query = BuildAgreemnetQuery(agreeNumChunkList, afterDate,beforeDate);
					else if (entityName.Equals("apttus_contractnumber"))
						query = BuildAgreemnetQueryOnContract(agreeNumChunkList);
					else if (entityName.Equals("apttus_documentversion"))
						query = BuildDocumentVersionQuery(agreeNumChunkList);
					else if (entityName.Equals("apttus_documentversiondetail"))
						query = BuildDocumentVersionDetailQuery(agreeNumChunkList);
					else if (entityName.Equals("annotation"))
						query = BuildAnnotationsQuery(agreeNumChunkList);
					else if (entityName.Equals("DocumentOnAgreements"))
						query = BuildDocumentDetailQuery(agreeNumChunkList);
					ChunkEntityList = RetrieveMultiple(query);
					EntityList.Entities.AddRange(ChunkEntityList.Entities);

					offset += chunkCount;
					totalRecords = totalRecords - chunkCount;
					chunkCount = totalRecords > conditionChunk ? conditionChunk : totalRecords;

				} while (totalRecords > 0);
			}
			return EntityList;
		}
		internal ExecuteMultipleResponse ExecuteChunk(List<Entity> chunkList, EnumOperationType enumOperationType)
		{
			ExecuteMultipleResponse saveResult = new ExecuteMultipleResponse();
			EntityCollection ChunkCollection = new EntityCollection();

			if (chunkList.Count > 0)
			{
				foreach (Entity item in chunkList)
					ChunkCollection.Entities.Add(item);
			}

			try
			{
				// Create an ExecuteMultipleRequest object.
				ExecuteMultipleRequest requestWithResults = new ExecuteMultipleRequest()
				{
					// Assign settings that define execution behavior: continue on error, return responses. 
					Settings = new ExecuteMultipleSettings()
					{
						ContinueOnError = true,
						ReturnResponses = true
					},
					// Create an empty organization request collection.
					Requests = new OrganizationRequestCollection()
				};

				// Add a CreateRequest for each entity to the request collection.
				foreach (var entity in ChunkCollection.Entities)
				{
					if (enumOperationType == EnumOperationType.Insert)
					{
						CreateRequest createRequest = new CreateRequest { Target = entity };
						requestWithResults.Requests.Add(createRequest);
					}
					else if (enumOperationType == EnumOperationType.Update)
					{
						UpdateRequest createRequest = new UpdateRequest { Target = entity };
						requestWithResults.Requests.Add(createRequest);
					}
					else if (enumOperationType == EnumOperationType.Delete)
					{
						DeleteRequest deleteRequest = new DeleteRequest { Target = entity.ToEntityReference() };
						requestWithResults.Requests.Add(deleteRequest);
					}
				}
				requestWithResults.Settings.ContinueOnError = true;
				requestWithResults.Settings.ReturnResponses = true;
				// Execute all the requests in the request collection using a single web method call.
				saveResult = (ExecuteMultipleResponse)ServiceProxy.Execute(requestWithResults);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error in Inserting Records:" + Environment.NewLine + ex.Message);
			}
			return saveResult;
		}
		internal OrganizationResponse ExecuteEntitySave(Entity entity, bool createNewRecord)
		{
			OrganizationRequest createRequest = null;
			if (createNewRecord)
			{
				createRequest = new CreateRequest { Target = entity };
			}
			else
			{
				createRequest = new UpdateRequest { Target = entity };
			}
			return (OrganizationResponse)ServiceProxy.Execute(createRequest);
		}
		internal QueryExpression BuildAgreemnetQuery(List<string> agreeNumChunkList, DateTime afterdate, DateTime beforeDate)
		{
			//QueryExpression query = new QueryExpression()
			//{
			//    Distinct = false,
			//    EntityName = "apttus_apts_agreement",
			//    ColumnSet = new ColumnSet("apttus_apts_agreementid", "apttus_agreement_number"),
			//    Criteria =
			//    {
			//        Conditions =
			//        {
			//            new ConditionExpression("apttus_agreement_number", ConditionOperator.In,agreeNumChunkList)
			//        }
			//    }
			//};
			//return query;
			QueryExpression query = new QueryExpression()
			{
				Distinct = false,
				EntityName = "apttus_apts_agreement",
				ColumnSet = new ColumnSet("apttus_apts_agreementid",
										  "apttus_agreement_number",
										  "apttus_name",
										  "apttus_contract_start_date",
										  "apttus_contract_end_date",
										  "apttus_status",
										  "apttus_status_category",
										  "apttus_recordtypeid",
										  "ownerid",
										  "apttus_accountid",
                                          "createdon",
                                          "modifiedon"
										  //,"cpro_testdate",
										  //"cpro_testdate2"
										  ),
				Criteria =
				{
					Conditions =
					{
						new ConditionExpression("createdon", ConditionOperator.OnOrAfter,afterdate),
                     //   new ConditionExpression("modifiedon", ConditionOperator.OnOrBefore,beforeDate),
                        new ConditionExpression("apttus_agreement_number", ConditionOperator.BeginsWith,"3")
                    }
				}
			};
			return query;
		}
		internal QueryExpression BuildDocumentDetailQuery(List<string> agreeNumChunkList)
		{
			LinkEntity objLinkEntityDocVersionToDocVersionDetail = new LinkEntity()
			{
				EntityAlias = "documentversion",
				Columns = new ColumnSet(true),//new ColumnSet("documentversion.apttus_documentversionidid", "documentversion.apttus_documentversiondetailid", "documentversion.apttus_title"),
				JoinOperator = JoinOperator.Inner,
				LinkFromAttributeName = "apttus_documentversionid",
				LinkFromEntityName = "apttus_documentversion",
				LinkToAttributeName = "apttus_documentversionidid",
				LinkToEntityName = "apttus_documentversiondetail"
			};
			LinkEntity objLinkEntityDocVersionToAgreement = new LinkEntity()
			{
				EntityAlias = "agreement",
				Columns = //new ColumnSet(true),
				new ColumnSet("apttus_documenttype"),
				JoinOperator = JoinOperator.Inner,
				LinkFromAttributeName = "apttus_apts_agreementid",
				LinkFromEntityName = "apttus_apts_agreement",
				LinkToAttributeName = "apttus_agreementidid",
				LinkToEntityName = "apttus_documentversion"
			};
			objLinkEntityDocVersionToAgreement.LinkEntities.Add(objLinkEntityDocVersionToDocVersionDetail);
			QueryExpression query = new QueryExpression()
			{
				Distinct = false,
				EntityName = "apttus_apts_agreement",
				Criteria = new FilterExpression()
				{
					Conditions =
							{
								new ConditionExpression("apttus_agreement_number", ConditionOperator.In,agreeNumChunkList)
							},
				},
				ColumnSet = new ColumnSet("apttus_agreement_number", "apttus_apts_agreementid")//new ColumnSet("apttus_documentversionidid", "apttus_documentversiondetailid", "apttus_title"),
			};

			query.LinkEntities.Add(objLinkEntityDocVersionToAgreement);
			return query;
		}
		internal QueryExpression BuildAgreemnetQueryOnContract(List<string> contractNumChunkList)
		{
			QueryExpression query = new QueryExpression()
			{
				Distinct = false,
				EntityName = "apttus_apts_agreement",
				ColumnSet = new ColumnSet("apts_contractnumber", "apttus_apts_agreementid", "apttus_agreement_number"),
				Criteria =
				{
					Conditions =
					{
						new ConditionExpression("apts_contractnumber", ConditionOperator.In,contractNumChunkList)
					}
				}
			};
			return query;
		}
		internal QueryExpression BuildDocumentVersionQuery(List<string> agreeNumChunkList)
		{
			QueryExpression query = new QueryExpression()
			{
				Distinct = false,
				EntityName = "apttus_documentversion",
				ColumnSet = new ColumnSet("apttus_documentversionid", "apttus_documenttype", "apttus_agreementidid"),
				LinkEntities =
				{
					new LinkEntity
					{
						JoinOperator = JoinOperator.Inner,
						EntityAlias = "agreement",
						Columns = new ColumnSet(new[] { "apttus_agreement_number" }),
						LinkFromAttributeName = "apttus_agreementidid",
						LinkFromEntityName = "apttus_documentversion",
						LinkToAttributeName = "apttus_apts_agreementid",
						LinkToEntityName = "apttus_apts_agreement",
						LinkCriteria =
						{
							Conditions =
							{
								new ConditionExpression("apttus_agreement_number", ConditionOperator.In,agreeNumChunkList)
							}
						}
					}
				}
			};

			return query;
		}
		internal QueryExpression BuildDocumentVersionDetailQuery(List<string> agreeNumChunkList)
		{
			QueryExpression query = new QueryExpression()
			{
				Distinct = false,
				EntityName = "apttus_documentversiondetail",
				ColumnSet = new ColumnSet(true),
				LinkEntities =
				{
					new LinkEntity
					{
						JoinOperator = JoinOperator.Inner,
						EntityAlias = "documentversion",
						Columns = new ColumnSet(new[] { "apttus_documentversionid" }),
						LinkFromAttributeName = "apttus_documentversionidid",
						LinkFromEntityName = "apttus_documentversiondetail",
						LinkToAttributeName = "apttus_documentversionid",
						LinkToEntityName = "apttus_documentversion",
						LinkCriteria =
						{
							Conditions =
							{
								new ConditionExpression("apttus_documentversionid", ConditionOperator.In,agreeNumChunkList)
							}
						}
					}
				}
			};

			return query;
		}
		internal QueryExpression BuildAnnotationsQuery(List<string> DocVersionDetailChunkList)
		{
			QueryExpression query = new QueryExpression()
			{
				Distinct = false,
				EntityName = "annotation",
				ColumnSet = new ColumnSet("objectid", "annotationid", "isdocument"),
				Criteria =
				{
					Conditions =
					{
						new ConditionExpression("objectid", ConditionOperator.In,DocVersionDetailChunkList)
					}
				}
			};
			return query;
		}
		internal List<string> BuildDocumentVersionDetailWithAnnotationsQuery(List<FileLogEntry> documentVersionDetail)
		{
			List<string> attachmentIDs = new List<string>();
			foreach (FileLogEntry fileLog in documentVersionDetail)
			{
				string AttachmentIds = string.Empty;
				var fetchXml = string.Format("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
										"<entity name='annotation'>" +
										"<attribute name='annotationid' />" +
										 "<attribute name='isdocument' />" +
										"<filter type='and'>" +
										"<condition attribute='objectid' operator='eq' value='{0}'/>" +
										"</filter>" +
										"</entity>" +
										"</fetch>", fileLog.DocumentVersionDetail);

				var response = ServiceProxy.RetrieveMultiple(new FetchExpression(fetchXml));
				var notes = response.Entities;
				if (notes != null && notes.Count > 0)
				{
					for (int i = 0; i <= notes.Count - 1; i++)
					{
						if (notes[i].GetAttributeValue<bool>("isdocument"))
						{
							AttachmentIds += notes[i].Id.ToString() + ",";
						}
					}
					AttachmentIds = AttachmentIds.Substring(0, AttachmentIds.Length - 1);
					attachmentIDs.Add(AttachmentIds);
				}
			}
			return attachmentIDs;
		}

		internal EntityCollection RetrieveMultiple(QueryExpression query)
		{
			EntityCollection EntityList = new EntityCollection();

			try
			{
				int pageNumber = 1;
				RetrieveMultipleRequest multiRequest;
				RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();
				do
				{
					query.PageInfo.Count = 5000;
					query.PageInfo.PagingCookie = (pageNumber == 1) ? null : multiResponse.EntityCollection.PagingCookie;
					query.PageInfo.PageNumber = pageNumber++;
					multiRequest = new RetrieveMultipleRequest();
					multiRequest.Query = query;
					multiResponse = (RetrieveMultipleResponse)ServiceProxy.Execute(multiRequest);
					EntityList.Entities.AddRange(multiResponse.EntityCollection.Entities);
				}
				while (multiResponse.EntityCollection.MoreRecords);

			}
			catch (Exception ex)
			{
				throw ex;

			}
			return EntityList;
		}
		internal static bool HasReadPermissionOnDirectory(string path)
		{
			var accessControlList = Directory.GetAccessControl(path);
			if (accessControlList == null)
				return false;
			var accessRules = accessControlList.GetAccessRules(true, true,
										typeof(System.Security.Principal.SecurityIdentifier));
			if (accessRules == null)
				return false;

			foreach (FileSystemAccessRule rule in accessRules)
			{
				if (rule.FileSystemRights == FileSystemRights.Read || rule.FileSystemRights == FileSystemRights.FullControl)
					return true;
			}
			return false;
		}
	}

	public enum EnumOperationType
	{
		Insert,
		Update,
		Delete,
	}
}
