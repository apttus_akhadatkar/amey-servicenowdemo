﻿using Apttus.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Apttus.Plugins
{
    public class DocumentUploadContent
    {
        public string DocumentType { get; set; }
        public string DocumentFilePath { get; set; }
        public string DocumentName { get; set; }
    }
    public class AuthenticationHelper
    {
        static AdmAccessTokenForHelper admToken;
        static string headerValue;

        private string ClientId = null;
        private string ClientSecret = null;
        private string DatamarketAccessUri = null;
        private string APIUri = null;
        private string ResourceId = null;
        public AuthenticationHelper()
        {

        }
        private string GetAuthorizationToken(string clientId, string clientSecret, string DatamarketAccessUri, string resourceId)
        {
            AdmAuthenticationForHelper admAuth = new AdmAuthenticationForHelper(clientId, clientSecret, DatamarketAccessUri, resourceId);
            try
            {
                admToken = admAuth.GetAccessToken();

                // Create a header with the access_token property of the returned token
                headerValue = "Bearer " + admToken.access_token;
                return headerValue;

            }
            catch (WebException e)
            {
                ProcessWebException(e);
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private static void ProcessWebException(WebException e)
        {

            // Obtain detailed error information
            string strResponse = string.Empty;
            using (HttpWebResponse response = (HttpWebResponse)e.Response)
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(responseStream, System.Text.Encoding.ASCII))
                    {
                        strResponse = sr.ReadToEnd();
                    }
                }
            }

        }
        public string DetectMethod(string authToken, List<DocumentUploadContent> lstDocumentContent, string methodType, string uri)
        {
            string result = string.Empty;
            try
            {
                HttpClient httpClient = new HttpClient();
                // This is the API URL that we need to invoke
                string uploadServiceBaseAddress = uri;
                // Read the files from disk 
                var content = new MultipartFormDataContent("---------------------------7e132f116048a");
                int i = 0;
                List<FileStream> lst = new List<FileStream>();
                foreach (DocumentUploadContent document in lstDocumentContent)
                {
                    string file = document.DocumentFilePath;
                    FileStream fileStream = File.Open(file, FileMode.Open);
                    Dictionary<string, object> metadata = new Dictionary<string, object>();
                    metadata.Add("Notes", "");
                    metadata.Add("DocumentType", new { key = document.DocumentType });

                    var fileInfo = new FileInfo(file);
                    StreamContent sc = new StreamContent(fileStream);
                    sc.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    content.Add(sc, string.Format("\"{0}\"", i), string.Format("\"{0}\"", document.DocumentName));
                    content.Add(new StringContent(JsonConvert.SerializeObject(metadata)), string.Format("\"{0}\"", i));
                    lst.Add(fileStream);
                    i++;
                }

                httpClient.DefaultRequestHeaders.Add("Authorization", authToken);

                Task taskUpload = httpClient.PostAsync(uploadServiceBaseAddress, content).ContinueWith(task =>
                {
                    if (task.Status == TaskStatus.RanToCompletion)
                    {
                        var response = task.Result;
                        result = response.StatusCode.ToString();
                        if (response.IsSuccessStatusCode)
                        {
                            result = response.Content.ReadAsStringAsync().Result;
                        }
                        else
                        {
                            throw new Exception("File could not be uploaded : " + result);
                        }
                    }
                    else
                    {
                        throw new Exception("File could not be uploaded : " + result);
                    }
                    lst.ForEach(x => x.Dispose());
                });

                taskUpload.Wait();

                httpClient.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public string UploadAgreementDocumentApi(Guid agreementId, List<DocumentUploadContent> lstDocumentContents, AzureAuthenticationDetail azureLoginDetail)
        {
            XmlDocument doc = new XmlDocument();

            ClientId = azureLoginDetail.ClientID;
            ClientSecret = azureLoginDetail.ClientSecretKey;
            DatamarketAccessUri = azureLoginDetail.DataMarketAccessURI;
            APIUri = azureLoginDetail.FileUploadURI;
            ResourceId = azureLoginDetail.ClientID;

            string uri = string.Format(APIUri, agreementId);

            string token = String.Empty;
            string response = String.Empty;

            // Get Authorization Token
            token = GetAuthorizationToken(ClientId, ClientSecret, DatamarketAccessUri, ResourceId);


            // To Fetch API Response
            response = DetectMethod(token, lstDocumentContents, "POST", uri);

            return response;
        }
    }

    [DataContract]
    public class AdmAccessTokenForHelper
    {
        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public string token_type { get; set; }
        [DataMember]
        public string expires_in { get; set; }
        [DataMember]
        public string scope { get; set; }
    }
    public class AdmAuthenticationForHelper
    {
        public static readonly string DatamarketAccessUri;
        private string clientId;
        private string clientSecret;
        private string request;
        private AdmAccessTokenForHelper token;
        private Timer accessTokenRenewer;
        //Access token expires every 10 minutes. Renew it every 9 minutes only.
        private const int RefreshTokenDuration = 9;
        public AdmAuthenticationForHelper(string clientId, string clientSecret, string DatamarketAccessUri, string resourceId)
        {
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            //If clientid or client secret has special characters, encode before sending request
            this.request = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&resource=" + resourceId, System.Uri.EscapeDataString(clientId), System.Uri.EscapeDataString(clientSecret));
            this.token = HttpPost(DatamarketAccessUri, this.request);
            //renew the token every specfied minutes
            accessTokenRenewer = new Timer(new TimerCallback(OnTokenExpiredCallback), this, TimeSpan.FromMinutes(RefreshTokenDuration), TimeSpan.FromMilliseconds(-1));
        }
        public AdmAccessTokenForHelper GetAccessToken()
        {
            return this.token;
        }
        private void RenewAccessToken()
        {
            AdmAccessTokenForHelper newAccessToken = HttpPost(DatamarketAccessUri, this.request);
            //swap the new token with old one
            //Note: the swap is thread unsafe
            this.token = newAccessToken;
            Console.WriteLine(string.Format("Renewed token for user: {0} is: {1}", this.clientId, this.token.access_token));
        }
        private void OnTokenExpiredCallback(object stateInfo)
        {
            try
            {
                RenewAccessToken();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                try
                {
                    accessTokenRenewer.Change(TimeSpan.FromMinutes(RefreshTokenDuration), TimeSpan.FromMilliseconds(-1));
                }
                catch (Exception ex)
                {
                }
            }
        }
        private AdmAccessTokenForHelper HttpPost(string DatamarketAccessUri, string requestDetails)
        {
            //Prepare OAuth request 
            WebRequest webRequest = WebRequest.Create(DatamarketAccessUri);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = Encoding.ASCII.GetBytes(requestDetails);
            webRequest.ContentLength = bytes.Length;
            using (Stream outputStream = webRequest.GetRequestStream())
            {
                outputStream.Write(bytes, 0, bytes.Length);
            }
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AdmAccessTokenForHelper));
                //Get deserialized object from JSON stream
                AdmAccessTokenForHelper token = (AdmAccessTokenForHelper)serializer.ReadObject(webResponse.GetResponseStream());
                return token;
            }
        }
    }
}
