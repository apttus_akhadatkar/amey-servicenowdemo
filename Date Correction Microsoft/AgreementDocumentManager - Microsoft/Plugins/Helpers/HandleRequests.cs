﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPL = System.Threading.Tasks;

namespace Apttus.Plugins.Helpers
{
    public class HandleRequests
    {
        public const int BATCH_SIZE = 30;// Controls how many items get passed into an ExecuteMultipleRequest
        public const int LIST_BATCH_SIZE = 200; // Controls how many items get passed into a thread

        /// <summary>
        /// Runs requests using parameters for batching and threading
        /// </summary>
        /// <param name="orgService">Organization Service instance</param>
        /// <param name="requests">Requests to run</param>
        /// <param name="useExecuteMultiple">Specify whether to use ExecuteMultipleRequest to batch requests</param>
        /// <param name="useMultiThread">Specify whether to run requests in multi-thread mode</param>
        public static void Execute(IOrganizationService orgService, List<OrganizationRequest> requests, bool useExecuteMultiple, bool useMultiThread)
        {
            // If we are using multi-threading we need to call MultiThreadRequests – that method will take care of the batching

            if (useMultiThread && useExecuteMultiple)
            {
                Console.WriteLine("Executing the requests in ExecuteMultiple and MultiThreaded mode");
                MultiThreadRequests(orgService, requests, true);
            }
            else if (useMultiThread)
            {
                Console.WriteLine("Executing the requests only in MultiThreaded mode");
                MultiThreadRequests(orgService, requests, false);
            }
            // Do batching with no threading
            else if (useExecuteMultiple)
            {
                Console.WriteLine("Executing the requests only in ExcuteMultiple mode");
                ExecuteMultipleRequests(orgService, requests);
            }
            // Just loop through all requests
            else
            {
                Console.WriteLine("Executing the requests only in Single Request mode");
                ExecuteRequests(orgService, requests);
            }
        }

        /// <summary>
        /// Sets up and runs requests in multi-threaded mode
        /// </summary>
        /// <param name="orgService">Organization Service instance</param>
        /// <param name="requests">Requests to run</param>
        /// <param name="useExecuteMultiple">Specify whether to use ExecuteMultipleRequest to batch requests</param>
        public static void MultiThreadRequests(IOrganizationService orgService, List<OrganizationRequest> requests, bool useExecuteMultiple)
        {
            // Split all requests into a multi-dimensional list – this is a list of lists containing OrganizationRequest items
            // We want to hand off a list of requests to each thread so they can take care of larger chunks
            List<List<OrganizationRequest>> splitRequestLists = SplitRequestList(requests);

            // Create a list of function calls – these functions will be used by Tasks in RunTasks()
            // We use bool so we specify if the list was processed
            List<Func<bool>> requestActions = new List<Func<bool>>();

            foreach (List<OrganizationRequest> requestList in splitRequestLists)
            {
                // Change method call for the list depending on batching parameter
                if (useExecuteMultiple)
                {
                    requestActions.Add(() => ExecuteMultipleRequests(orgService, requestList));
                }
                else
                {
                    requestActions.Add(() => ExecuteRequests(orgService, requestList));
                }
            }
            RunTasks(requestActions);
        }

        /// <summary>
        /// Executes requests with no batching
        /// </summary>
        /// <param name="orgService">Organization Service instance</param>
        /// <param name="requests">Requests to run</param>
        /// <returns>Always true – operation was completed</returns>
        public static bool ExecuteRequests(IOrganizationService orgService, List<OrganizationRequest> requests)
        {
            foreach (OrganizationRequest request in requests)
            {
                orgService.Execute(request);
            }

            return true;
        }

        /// <summary>
        /// Executes requests with batching
        /// </summary>
        /// <param name="orgService">Organization Service instance</param>
        /// <param name="requests">Requests to run</param>
        /// <returns>Always true – operation was completed</returns>
        public static bool ExecuteMultipleRequests(IOrganizationService orgService, List<OrganizationRequest> requests)
        {
            int requestCount = 0; // Current request count – if we fill up an ExecuteMultiple we want to run it
            int batchCount = 1; // Current batch count – can be used for logging

            // Get a refresh ExecuteMultiplRequest
            ExecuteMultipleRequest executeMultipleRequest = CreateExecuteMultipleRequest();
            if (requests.Count > 0)
            {
                // Go through all the requests from the list
                // The requests will be executed once enough requests fill up the ExecuteMultipleRequest or we’ve run out of requests
                foreach (OrganizationRequest request in requests)
                {
                    executeMultipleRequest.Requests.Add(request);

                    requestCount++;

                    // Have we reached our batch size or did we already hit the total count?
                    // Only fire if there are actually requests
                    if ((executeMultipleRequest.Requests.Count == BATCH_SIZE || requestCount == requests.Count)
                    && executeMultipleRequest.Requests.Count > 0)
                    {
                        Console.WriteLine($"Executing Batch {batchCount}");
                        // Run all requests
                        ExecuteMultipleResponse response = orgService.Execute(executeMultipleRequest) as ExecuteMultipleResponse;
                        // This response will be faulted if there is at least one error
                        if (response.IsFaulted)
                        {
                            // TODO: Handle errors
                        }
                        // Reset ExecuteMultiple for next batch
                        executeMultipleRequest = CreateExecuteMultipleRequest();
                        batchCount++;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Generic function that runs a list of methods in multi-threaded mode. Waits for all methods to finish before returning.
        /// </summary>
        /// <param name="actions">Functions to call</param>
        public static void RunTasks(List<Func<bool>> actions)
        {
            TPL.Task[] tasks = new TPL.Task[actions.Count];

            // Loop through all functions and create a new Task for them
            for (int taskIndex = 0; taskIndex < tasks.Length; taskIndex++)
            {
                int localIndex = taskIndex;
                tasks[localIndex] = TPL.Task.Factory.StartNew(() => actions[localIndex]());
            }
            List<AggregateException> exceptions = new List<AggregateException>();
            try
            {
                // Function will only exit once all Tasks are complete
                TPL.Task.WaitAll(tasks);
            }
            catch (AggregateException ex)
            {
                exceptions.Add(ex);
            }

            // TODO: Handle errors in exceptions list
        }

        /// <summary>
        /// Creates a fresh ExecuteMultiplRequest with standard defaults – ContinueOnError and ReturnResponses
        /// </summary>
        /// <returns>ExecuteMultipleRequest ready for use</returns>
        public static ExecuteMultipleRequest CreateExecuteMultipleRequest()
        {
            ExecuteMultipleRequest executeMultipleRequest = new ExecuteMultipleRequest();
            executeMultipleRequest.Requests = new OrganizationRequestCollection();

            executeMultipleRequest.Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = true,
                ReturnResponses = true
            };

            return executeMultipleRequest;
        }

        /// <summary>
        /// Splits up a large list of OrganizationRequest items into lists that will be used for batching
        /// </summary>
        /// <param name="requests">Requests to run</param>
        /// <returns>List of Lists of OrganizationRequest items</returns>
        public static List<List<OrganizationRequest>> SplitRequestList(List<OrganizationRequest> requests)
        {
            List<List<OrganizationRequest>> splitRequestList = new List<List<OrganizationRequest>>();

            // We always round up to the next whole number to ensure all requests are accounted for
            int listCount = (int)Math.Ceiling((double)requests.Count / LIST_BATCH_SIZE);

            int currentListIndex = 0;

            // Add empty lists
            for (int i = 0; i < listCount; i++)
            {
                splitRequestList.Add(new List<OrganizationRequest>());
            }

            // Fill empty lists with requests up to the threshold
            foreach (OrganizationRequest request in requests)
            {
                // Increment the listIndex if the list is full
                if (splitRequestList[currentListIndex].Count == LIST_BATCH_SIZE)
                {
                    currentListIndex++;
                }

                splitRequestList[currentListIndex].Add(request);
            }

            return splitRequestList;
        }
    }
}
