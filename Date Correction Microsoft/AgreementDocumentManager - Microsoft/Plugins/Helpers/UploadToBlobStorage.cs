﻿using Apttus.Model;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Net;

namespace Apttus.Plugins
{
    class BlobStorageManager
    {
        private CloudBlockBlob blockBlob;
        private CloudBlobContainer container;

        public BlobStorageManager(string containerName)
        {
            // Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve a reference to a container.
            container = blobClient.GetContainerReference(containerName);

            // Create the container if it doesn't already exist.
            container.CreateIfNotExists();
        }

        /*Get blob name, attachment file path and upload to blob storage*/
        public Boolean UploadToBlob(String blobName, string filePath, string userName)
        {
            if (!File.Exists(filePath))
            {
                return false;// "Attachment does not exist";
            }
            blockBlob = container.GetBlockBlobReference(blobName);

            string FileName = blobName.Split('/')[1];
            FileName = WebUtility.UrlEncode(FileName);

            string ParentId = blobName.Split('/')[0];
            string FileId = ParentId + '$' + FileName;

            // Create or overwrite the existing blob with contents from a local file.
            using (var fileStream = System.IO.File.OpenRead(filePath))
            {
                String contentType = System.Web.MimeMapping.GetMimeMapping(FileName);
                blockBlob.Metadata.Add("FileName", FileName);
                string datePatt = @"M/d/yyyy hh:mm:ss tt";
                blockBlob.Metadata.Add("ParentId", ParentId);
                blockBlob.Metadata.Add("FileId", FileId);
                blockBlob.Metadata.Add("CreatedOn", DateTime.Now.ToString(datePatt));
                blockBlob.Metadata.Add("CreatedBy", userName);
                blockBlob.Metadata.Add("ContentType", contentType);
                blockBlob.Properties.ContentType = contentType;
                blockBlob.UploadFromStream(fileStream);
            }
            return true;
        }

        public bool CopyContentBlob(String blobName, string newBlobName, string userName)
        {
            blockBlob = container.GetBlockBlobReference(blobName);
            CloudBlockBlob blobCopy = container.GetBlockBlobReference(newBlobName);

            string FileName = newBlobName.Split('/')[1];
            FileName = WebUtility.UrlEncode(FileName);

            string ParentId = newBlobName.Split('/')[0];
            string FileId = ParentId + '$' + FileName;

            String contentType = System.Web.MimeMapping.GetMimeMapping(FileName);
            if (!blobCopy.Exists())
            {
                if (blockBlob.Exists())
                {
                    blobCopy.Metadata.Add("FileId", FileId);
                    blobCopy.Metadata.Add("FileName", FileName);
                    string datePatt = @"M/d/yyyy hh:mm:ss tt";
                    blobCopy.Metadata.Add("CreatedOn", DateTime.Now.ToString(datePatt));
                    blobCopy.Metadata.Add("ParentId", ParentId);
                    blobCopy.Metadata.Add("CreatedBy", userName);
                    blobCopy.Metadata.Add("ContentType", contentType);
                    blobCopy.Properties.ContentType = contentType;
                    blobCopy.StartCopy(blockBlob);
                    if (blobCopy.Exists())
                        blockBlob.Delete();
                }
            }

            return blobCopy.Exists();
        }
        public Boolean DownloadFromBlob(FileLogEntry FileData)
        {
            try
            {
                string filepath = FileData.FilePathToUpload;
                string filename = FileData.FileName;
                string blobName = FileData.DocumentVersionDetail + "/" + filename;
                blockBlob = container.GetBlockBlobReference(blobName);
                if (blockBlob.Exists())
                {

                    var memoryStream = new MemoryStream();
                    blockBlob.DownloadToStream(memoryStream);

                    System.IO.FileInfo file = new System.IO.FileInfo(filepath + FileData.AgreementNumber + "\\" + blobName);
                    file.Directory.Create();
                    if (!File.Exists(filepath + FileData.AgreementNumber + "\\" + blobName))
                    {
                        var sw = new StreamWriter(filepath + FileData.AgreementNumber + "\\" + blobName, true);
                        sw.Write(memoryStream);
                    }                    
                    memoryStream.Close();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
