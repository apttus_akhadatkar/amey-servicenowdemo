﻿using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.Plugins
{
    class CRM_RecoveryData : PluginExecution
    {
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.FileDataCapturingInProgress;
                #region ProcessInputFileDataLines and Prepare the CRM data
                this.ProcessInputFileDataLines(exceutionContext);
                #endregion
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw new Exception("Error while preparing data for process, please revisit your data or contact help desk.");
            }
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.FileDataCapturingDone;
        }
        private void ProcessInputFileDataLines(Model.IExecutionContext exceutionContext)
        {
            if (exceutionContext.InputFileDataLines.Count > 0)
            {

                //Get all Agreement Numbers from file
                exceutionContext.AgreementsToProcess = new List<string>();
                exceutionContext.AgreementAndDocumentType = new List<string>();
                foreach (var line in exceutionContext.InputFileDataLines)
                {
                    exceutionContext.FileDataLog.Add(new FileLogEntry()
                    {
                        UniqueIdentifier = line.Line.Split(exceutionContext.SplitDelimeter)[5] + "_" + line.Line.Split(exceutionContext.SplitDelimeter)[6] + "_" + line.Line.Split(exceutionContext.SplitDelimeter)[1],
                        AgreementNumber = line.Line.Split(exceutionContext.SplitDelimeter)[0],
                        Status = (UploadStatus)Enum.Parse(typeof(UploadStatus), line.Line.Split(exceutionContext.SplitDelimeter)[9]),
                        BlobSourcePath = line.Line.Split(exceutionContext.SplitDelimeter)[1],
                        CurrentRunIdentifier = line.Line.Split(exceutionContext.SplitDelimeter)[5],
                        DocumentVersion = line.Line.Split(exceutionContext.SplitDelimeter)[2],
                        DocumentVersionDetail = line.Line.Split(exceutionContext.SplitDelimeter)[3],
                        IsProcessed = Convert.ToBoolean(line.Line.Split(exceutionContext.SplitDelimeter)[4].Trim()),
                        BlobDetail = "Data taken for Recovery.",
                        DocumentType = line.Line.Split(exceutionContext.SplitDelimeter)[6],
                        FilePathToUpload = line.Line.Split(exceutionContext.SplitDelimeter)[1],
                        FileName = line.Line.Split(exceutionContext.SplitDelimeter)[7],
                        AgreementGUID = line.Line.Split(exceutionContext.SplitDelimeter)[8]
                    });
                }
                if (exceutionContext.FileDataLog.All(x => x.IsProcessed == true))
                {
                    exceutionContext.FileDataLog.ToList().ForEach(x => { x.BlobDetail = "No recovery required."; });
                    throw new Exception("No Recovery Required.");
                }
            }
        }
    }
}
