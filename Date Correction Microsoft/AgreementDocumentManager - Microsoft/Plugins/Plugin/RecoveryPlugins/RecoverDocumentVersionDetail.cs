﻿using Apttus.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;

namespace Apttus.Plugins
{
    public class RecoverDocumentVersionDetail : PluginExecution
    {
        Helper helper;
        public RecoverDocumentVersionDetail()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                Entity documentVersionDetailRecord;
                List<Entity> documentversionDetailtoBeDeletedList = new List<Entity>();
                foreach (FileLogEntry obj in exceutionContext.FileDataLog)
                {
                    if (obj.IsProcessed != true)
                    {
                        documentVersionDetailRecord = new Entity("apttus_documentversiondetail");
                        documentVersionDetailRecord["apttus_documentversiondetailid"] = Guid.Parse(obj.DocumentVersionDetail);
                        documentversionDetailtoBeDeletedList.Add(documentVersionDetailRecord);
                        obj.BlobDetail = "Document Version Detail deleted as BlobUpload failed.";
                    }
                }
                ExecuteMultipleResponse deletedDocumentVersionDetailResponse = helper.ChunkAndExecute(documentversionDetailtoBeDeletedList, EnumOperationType.Delete, exceutionContext.CRM_MaxInsertChunk);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
