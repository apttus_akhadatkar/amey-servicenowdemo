﻿using Apttus.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.Plugins
{
    public class RecoverDocumentVersion : PluginExecution
    {
        Helper helper;
        public RecoverDocumentVersion()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.FileDataLog.Where(x => x.IsProcessed == true).ToList().ForEach(x => { x.BlobDetail = "No recovery required."; });
                Entity documentVersionRecord;
                List<Entity> documentversiontoBeDeletedList = new List<Entity>();
                foreach (FileLogEntry obj in exceutionContext.FileDataLog.Where(x => x.IsProcessed != true))
                {
                    if (!string.IsNullOrEmpty(obj.DocumentVersion.Trim()))
                    {
                        documentVersionRecord = new Entity("apttus_documentversion");
                        documentVersionRecord.Id = Guid.Parse(obj.DocumentVersion);
                        documentVersionRecord["apttus_documentversionid"] = Guid.Parse(obj.DocumentVersion);
                        documentversiontoBeDeletedList.Add(documentVersionRecord);
                        obj.BlobDetail = "Document Version deleted as BlobUpload failed.";
                        obj.Status = UploadStatus.DocumentVersionRecovered;
                    }
                }
                ExecuteMultipleResponse deletedDocumentVersionDetailResponse = helper.ChunkAndExecute(documentversiontoBeDeletedList, EnumOperationType.Delete, exceutionContext.CRM_MaxInsertChunk);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
