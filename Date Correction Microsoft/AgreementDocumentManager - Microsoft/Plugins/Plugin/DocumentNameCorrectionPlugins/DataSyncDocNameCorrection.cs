﻿using System;
using System.Collections.Generic;
using Apttus.Model;

namespace Apttus.Plugins
{
    public class DataSyncDocNameCorrection : PluginExecution
    {
        public DataSyncDocNameCorrection()
        {
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.FileDataCapturingInProgress;
                #region ProcessInputFileDataLines and Prepare the CRM data
                this.ProcessInputFileDataLines(exceutionContext);
                #endregion
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw new Exception("Error while preparing data for process, please revisit your data or contact help desk.");
            }
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.FileDataCapturingDone;

        }
        private void ProcessInputFileDataLines(Model.IExecutionContext exceutionContext)
        {
            if (exceutionContext.InputFileDataLines.Count > 0)
            {
                foreach (var line in exceutionContext.InputFileDataLines)
                {
                    exceutionContext.FileDataLog.Add(new FileLogEntry()
                    {
                        //UniqueIdentifier = exceutionContext.CurrentUniqueRunIdentifier + "_" + agreeNumDocumentType + "_" + line.Line.Split(exceutionContext.SplitDelimeter)[1],
                        AgreementNumber = line.Line.Split(exceutionContext.SplitDelimeter)[0],
                        AgreementGUID = line.Line.Split(exceutionContext.SplitDelimeter)[1],
                        Status = UploadStatus.FileValidated,
                        CurrentRunIdentifier = exceutionContext.CurrentUniqueRunIdentifier,
                        DocumentVersion = line.Line.Split(exceutionContext.SplitDelimeter)[3],
                        DocumentVersionDetail = line.Line.Split(exceutionContext.SplitDelimeter)[4],
                        DocumentVersionDetailTitle = line.Line.Split(exceutionContext.SplitDelimeter)[5],
                        IsProcessed = false,
                        BlobDetail = "",
                        HasError = false,
                        FileName = line.Line.Split(exceutionContext.SplitDelimeter)[5]
                    });
                }
            }
        }
    }
}
