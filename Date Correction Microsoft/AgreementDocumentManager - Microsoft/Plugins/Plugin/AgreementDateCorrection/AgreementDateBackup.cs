﻿using Apttus.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.Plugins
{
    public class AgreementDateBackup : PluginExecution
    {
        Helper helper;
        public AgreementDateBackup()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            EntityCollection DocumentVersionEntityList = helper.ChunkAndFetchOndateEntity("apttus_apts_agreement", exceutionContext.CRM_MaxQueryConditionChunk);
            foreach (Entity entity in DocumentVersionEntityList.Entities)
            {
                exceutionContext.FileDataLog.Add(new FileLogEntry()
                {
                    AgreementNumber = entity.Contains("apttus_agreement_number") ? entity["apttus_agreement_number"].ToString() : "",
                    AgreementGUID = entity.Contains("apttus_apts_agreementid") ? entity["apttus_apts_agreementid"].ToString() : "",
                    AgreementName = entity.Contains("apttus_name") ? entity["apttus_name"].ToString() : "",
                    StartDateModified = entity.Contains("apttus_contract_start_date") ? Convert.ToDateTime(entity["apttus_contract_start_date"]) : (DateTime?)null,
                    EndDateModified = entity.Contains("apttus_contract_end_date") ? Convert.ToDateTime(entity["apttus_contract_end_date"]) : (DateTime?)null,
                    AgreementStartDate = entity.Contains("createdon") ? Convert.ToDateTime(entity["createdon"]) : (DateTime?)null,
                    AgreementEnddate = entity.Contains("modifiedon") ? Convert.ToDateTime(entity["modifiedon"]) : (DateTime?)null,
                    DocumentVersionDetail = entity.Contains("cpro_testdate") ? entity["cpro_testdate"].ToString() : "",
                    DocumentVersion = entity.Contains("cpro_testdate2") ? entity["cpro_testdate2"].ToString() : "",

                });
            }
            int i = 0;
            Entity newBackupRecord;
            List<Entity> newBackList = new List<Entity>();
            foreach (FileLogEntry data in exceutionContext.FileDataLog)
            {
                #region commented
                //DateTime date = (DateTime?)data.StartDateModified != null ? Convert.ToDateTime(data.StartDateModified) : DateTime.Now;

                //DateTime enddate = (DateTime?)data.EndDateModified != null ? Convert.ToDateTime(data.EndDateModified) : DateTime.Now;

                //if ((DateTime?)data.StartDateModified != null)
                //{
                //    i++;
                //    if (Convert.ToDateTime(data.StartDateModified).TimeOfDay == new TimeSpan(18, 30, 0))
                //    {
                //        date = date.AddHours(5.5);
                //        date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.StartDateModified = date;
                //    }
                //    if (Convert.ToDateTime(data.StartDateModified).TimeOfDay == new TimeSpan(23, 0, 0))
                //    {
                //        date = date.AddHours(1);
                //        date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.StartDateModified = date;
                //    }
                //    if (Convert.ToDateTime(data.StartDateModified).TimeOfDay == new TimeSpan(8, 0, 0))
                //    {
                //        date = date.AddHours(-8);
                //        date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.StartDateModified = date;
                //    }
                //    if (Convert.ToDateTime(data.StartDateModified).TimeOfDay == new TimeSpan(7, 0, 0))
                //    {
                //        date = date.AddHours(-7);
                //        date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.StartDateModified = date;
                //    }
                //    if (Convert.ToDateTime(data.StartDateModified).TimeOfDay == new TimeSpan(6, 0, 0))
                //    {
                //        date = date.AddHours(-6);
                //        date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.StartDateModified = date;
                //    }
                //    if (Convert.ToDateTime(data.StartDateModified).TimeOfDay == new TimeSpan(5, 0, 0))
                //    {
                //        date = date.AddHours(-5);
                //        date = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.StartDateModified = date;
                //    }
                //}

                //if ((DateTime?)data.EndDateModified != null)
                //{
                //    if (Convert.ToDateTime(data.EndDateModified).TimeOfDay == new TimeSpan(18, 30, 0))
                //    {
                //        enddate = enddate.AddHours(5.5);
                //        enddate = new DateTime(enddate.Year, enddate.Month, enddate.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.EndDateModified = enddate;
                //    }
                //    if (Convert.ToDateTime(data.EndDateModified).TimeOfDay == new TimeSpan(23, 0, 0))
                //    {
                //        enddate = enddate.AddHours(1);
                //        enddate = new DateTime(enddate.Year, enddate.Month, enddate.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.EndDateModified = enddate;
                //    }
                //    if (Convert.ToDateTime(data.EndDateModified).TimeOfDay == new TimeSpan(8, 0, 0))
                //    {
                //        enddate = enddate.AddHours(-8);
                //        enddate = new DateTime(enddate.Year, enddate.Month, enddate.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.EndDateModified = enddate;
                //    }
                //    if (Convert.ToDateTime(data.EndDateModified).TimeOfDay == new TimeSpan(7, 0, 0))
                //    {
                //        enddate = enddate.AddHours(-7);
                //        enddate = new DateTime(enddate.Year, enddate.Month, enddate.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.EndDateModified = enddate;
                //    }
                //    if (Convert.ToDateTime(data.EndDateModified).TimeOfDay == new TimeSpan(6, 0, 0))
                //    {
                //        enddate = enddate.AddHours(-6);
                //        enddate = new DateTime(enddate.Year, enddate.Month, enddate.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.EndDateModified = enddate;
                //    }
                //    if (Convert.ToDateTime(data.EndDateModified).TimeOfDay == new TimeSpan(5, 0, 0))
                //    {
                //        enddate = enddate.AddHours(-5);
                //        enddate = new DateTime(enddate.Year, enddate.Month, enddate.Day, 0, 0, 0, DateTimeKind.Utc);
                //        data.EndDateModified = enddate;
                //    }
                //}
                #endregion
                newBackupRecord = new Entity("cpro_backupdatesagreement");
                newBackupRecord["cpro_agreementguid"] = data.AgreementGUID.ToString();
                newBackupRecord["cpro_agreementnumber"] = data.AgreementNumber;
                newBackupRecord["cpro_name"] = data.AgreementNumber;
                if (data.StartDateModified != null)
                    newBackupRecord["cpro_contract_start_date"] = data.StartDateModified;
                if (data.EndDateModified != null)
                    newBackupRecord["cpro_contract_end_date"] = data.EndDateModified;
                if (data.AgreementStartDate != null)
                    newBackupRecord["cpro_agreementcreatedate"] = data.AgreementStartDate;
                if (data.AgreementEnddate != null)
                    newBackupRecord["cpro_agreementmodifieddate"] = data.AgreementEnddate;
                newBackList.Add(newBackupRecord);
           
            }
            ExecuteMultipleResponse newAgreementResponse = helper.ChunkAndExecute(newBackList, EnumOperationType.Insert, exceutionContext.CRM_MaxInsertChunk);
        }
    }
}
