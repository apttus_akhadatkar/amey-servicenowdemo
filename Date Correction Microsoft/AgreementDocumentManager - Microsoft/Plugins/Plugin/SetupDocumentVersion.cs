﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Apttus.Plugins
{
    class SetupDocumentVersion : PluginExecution
    {
        Helper helper;
        public SetupDocumentVersion()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionSyncInProgress;
                #region Fetch DocumentVersion and populate Map of Agreement Number To DocumentVersion ID

                //Query to existing Document version from CRM using Agreement numbers provided in the CSV file
                EntityCollection DocumentVersionEntityList = helper.ChunkAndFetchEntity(exceutionContext.AgreementsToProcess, "apttus_documentversion", exceutionContext.CRM_MaxQueryConditionChunk);

                //Populate dictionary to map Agreement number and existing Document version ID
                var agreeNumToDocVersionIDMap = new Dictionary<string, Guid>();
                foreach (Entity item in DocumentVersionEntityList.Entities.ToList())
                {
                    string documentType = BlobStorageAppResources.EXECUTED;
                    if (item.Attributes.Contains("apttus_documenttype"))
                    {
                        documentType = ((OptionSetValue)item["apttus_documenttype"]).Value.ToString();
                    }
                    string key = ((AliasedValue)item["agreement.apttus_agreement_number"]).Value.ToString() + "-" + documentType;
                    if (!agreeNumToDocVersionIDMap.ContainsKey(key))
                    {
                        agreeNumToDocVersionIDMap.Add(key, Guid.Parse(item["apttus_documentversionid"].ToString()));
                    }
                }

                #endregion

                #region Fetch Agreements and populate Map of Agreement Number To Agreement GUID

                //filter agreement Number whose document version ID does not exist in the CRM and we have to create new for the same.
                List<string> filteredAgreeNumList = exceutionContext.AgreementAndDocumentType.Except(agreeNumToDocVersionIDMap.Keys).ToList();//.Where(a => !agreeNumToDocVersionIDMap.Keys.Contains(a)).ToList();
                                                                                                                                              // This list is to contain agreement Number only
                List<string> filterOnlyAgreementNum = new List<string>();
                foreach (string agreementdocumenttype in filteredAgreeNumList)
                {
                    string agreementNumber = agreementdocumenttype.Split('-')[0];
                    if (!filterOnlyAgreementNum.Contains(agreementNumber))
                        filterOnlyAgreementNum.Add(agreementNumber);
                }

                EntityCollection AgreementEntityList = helper.ChunkAndFetchEntity(filterOnlyAgreementNum, "apttus_apts_agreement", exceutionContext.CRM_MaxQueryConditionChunk);
                //Populate dictionary to map Agreement number and Agreement ID
                var agreeNumToAgreeIDMap = AgreementEntityList.Entities.ToDictionary(ag => ag["apttus_agreement_number"].ToString(), ag => Guid.Parse(ag["apttus_apts_agreementid"].ToString()));
                #endregion

                #region Create and Insert Document Version records

                //Filter and Create new Document versions records for Agreement Number
                Entity newDocumentVersionRecord;
                List<Entity> newDocumentversionList = new List<Entity>();
                List<string> agreeNumForNewDocversion = new List<string>();
                foreach (string agreeNum in filteredAgreeNumList)
                {
                    Guid agreeId = Guid.Empty;
                    if (agreeNumToAgreeIDMap.TryGetValue(agreeNum.Split('-')[0], out agreeId))
                    {

                        newDocumentVersionRecord = new Entity("apttus_documentversion");
                        newDocumentVersionRecord["apttus_agreementidid"] = new EntityReference("apttus_apts_agreement", agreeId);
                        newDocumentVersionRecord["apttus_name"] = agreeId.ToString();
                        if (BlobStorageAppResources.EXECUTED == agreeNum.Split('-')[1])
                        {
                            newDocumentVersionRecord["apttus_documenttype"] = new OptionSetValue(Convert.ToInt32(BlobStorageAppResources.EXECUTED));
                        }
                        else
                        {
                            newDocumentVersionRecord["apttus_documenttype"] = new OptionSetValue(Convert.ToInt32(BlobStorageAppResources.OTHER));
                        }
                        newDocumentversionList.Add(newDocumentVersionRecord);
                        agreeNumForNewDocversion.Add(agreeNum);
                    }
                }

                //Perform Database Insert on DocumentVersion records
                ExecuteMultipleResponse newDocumentVersionResponse = helper.ChunkAndExecute(newDocumentversionList, exceutionContext.CRM_MaxInsertChunk);

                #endregion

                #region Process DocumentVersion Response

                exceutionContext.SkippedFileDataLog = new List<FileLogEntry>();

                //Process DocumentVersion Response and create Map for Agreement Number To Document versions ID and add it to existing list
                for (var index = 0; index < agreeNumForNewDocversion.Count; index++)
                {
                    string agreeNum = agreeNumForNewDocversion[index];
                    Guid documentVersionID = Guid.Empty;

                    IEnumerable<FileLogEntry> fileLogData = exceutionContext.FileDataLog.Where(x => x.UniqueIdentifier.StartsWith(exceutionContext.CurrentUniqueRunIdentifier + "_" + agreeNum));

                    ExecuteMultipleResponseItem responseItem = newDocumentVersionResponse.Responses[index];
                    if (responseItem.Response != null && responseItem.Fault == null)
                    {
                        documentVersionID = new Guid(responseItem.Response.Results["id"].ToString());
                        agreeNumToDocVersionIDMap.Add(agreeNum, documentVersionID);

                        foreach (var logData in fileLogData)
                        {
                            logData.DocumentVersion = responseItem.Response.Results["id"].ToString();
                            logData.Status = UploadStatus.DocumentVersionCreated;
                        }
                    }
                    else
                    {
                        foreach (var logData in fileLogData)
                        {
                            logData.BlobDetail = "Document version was not created or Agreement do not exist";
                            exceutionContext.FileDataLog.Remove(logData);
                            logData.Status = UploadStatus.DocumentVersionDetailCreationFailed;
                            exceutionContext.SkippedFileDataLog.Add(logData);
                        }
                    }
                }
                exceutionContext.AgreeNumToDocVersionIDMap = agreeNumToDocVersionIDMap;
                #endregion
            }
            catch (Exception ex)
            { }
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionSyncDone;
        }
    }
}
