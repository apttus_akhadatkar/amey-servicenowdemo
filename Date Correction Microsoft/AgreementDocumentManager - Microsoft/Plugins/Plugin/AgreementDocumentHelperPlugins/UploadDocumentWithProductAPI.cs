﻿using Microsoft.Xrm.Sdk;
using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Apttus.Plugins
{
    public class UploadDocumentWithProductAPI : PluginExecution
    {
        Helper helper;
        AuthenticationHelper authHelper;
        public UploadDocumentWithProductAPI()
        {
            helper = new Helper();
            authHelper = new AuthenticationHelper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.BlobFileUploadInProgress;
                var agreementNumbers = exceutionContext.FileDataLog.ToList().Select(x => x.AgreementNumber).Distinct().ToList();
                EntityCollection AgreementEntityList = helper.ChunkAndFetchEntity(agreementNumbers, "apttus_apts_agreement", exceutionContext.CRM_MaxQueryConditionChunk);
                var fileLog = (from log in exceutionContext.FileDataLog
                               join entity in AgreementEntityList.Entities
                               on log.AgreementNumber equals entity["apttus_agreement_number"].ToString()
                               select new
                               {
                                   AgreementNumber = log.AgreementNumber,
                                   AgreementGUID = entity["apttus_apts_agreementid"].ToString(),
                               }).ToList();
                foreach (FileLogEntry obj in exceutionContext.FileDataLog)
                {
                    obj.AgreementGUID = fileLog.Find(x => x.AgreementNumber == obj.AgreementNumber) != null ? fileLog.Find(x => x.AgreementNumber == obj.AgreementNumber).AgreementGUID : "";
                }
                exceutionContext.FileDataLog.ToList().Where(x => x.AgreementGUID == "").ToList().ForEach(x => { x.HasError = true; x.Status = UploadStatus.InvalidAgreementNumber; });

                List<IGrouping<string, FileLogEntry>> lst = exceutionContext.FileDataLog.Where(x => x.HasError == false).GroupBy(x => x.AgreementGUID).ToList();
                foreach (var documentContent in lst)
                {
                    string AgreementKey = documentContent.Key;
                    try
                    {
                        List<FileLogEntry> documentInfo = documentContent.ToList();
                        List<DocumentUploadContent> content = new List<DocumentUploadContent>();
                        documentInfo.ForEach(x => content.Add(new DocumentUploadContent() { DocumentFilePath = x.FilePathToUpload, DocumentName = x.FileName, DocumentType = x.AgreementDocumentType }));
                        string result = authHelper.UploadAgreementDocumentApi(Guid.Parse(AgreementKey), content, exceutionContext.AzureLogin);
                        exceutionContext.FileDataLog.ToList().FindAll(x => x.AgreementGUID == AgreementKey).ForEach(x => { x.Status = UploadStatus.FileUploaded; x.IsProcessed = true; x.BlobDetail = result; });
                    }
                    catch (Exception ex)
                    {
                        exceutionContext.FileDataLog.ToList().FindAll(x => x.AgreementGUID == AgreementKey).ForEach(x => { x.Status = UploadStatus.BlobUploadFailed; x.HasError = true; x.BlobDetail = ex.Message + "\r\n" + ex.StackTrace; });
                    }
                }
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.BlobFileUploadDone;
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = true;
                throw new Exception("Error While Uploading documents to blob through API."  + Environment.NewLine + ex.Message);
            }
        }
    }
}
