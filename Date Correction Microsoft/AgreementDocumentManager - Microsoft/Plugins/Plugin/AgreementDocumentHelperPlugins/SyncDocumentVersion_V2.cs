﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Apttus.Plugins
{
    public class SyncDocumentVersion_V2 : PluginExecution
    {
        Helper helper;
        public SyncDocumentVersion_V2()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionSyncInProgress;
            try
            {
                #region Fetch DocumentVersion and populate Map of Agreement Number To DocumentVersion ID

                EntityCollection AgreementEntityList = helper.ChunkAndFetchEntity(exceutionContext.FileDataLog.Select(x => x.AgreementNumber).ToList(), "apttus_apts_agreement", exceutionContext.CRM_MaxQueryConditionChunk);

                foreach (FileLogEntry obj in exceutionContext.FileDataLog.Where(x => x.DocumentVersion == ""))
                {
					//var agreementQuery = AgreementEntityList.Entities.Where(x => (x["apttus_agreement_number"]).ToString() == obj.AgreementNumber);
					//obj.AgreementGUID = agreementQuery.Count() > 0 ? agreementQuery.First()["apttus_apts_agreementid"].ToString() : "";
					var agreementQuery = AgreementEntityList.Entities.Where(x => (x["apttus_agreement_number"]).ToString() == obj.AgreementNumber);
					if (agreementQuery.ToList().Count > 0)
					{
						obj.AgreementGUID = agreementQuery.First().Contains("apttus_apts_agreementid") && agreementQuery.Count() > 0 ? agreementQuery.First()["apttus_apts_agreementid"].ToString() : "";
						obj.AgreementName = agreementQuery.First().Contains("apttus_name") ? agreementQuery.First()["apttus_name"].ToString() : "";
						obj.AgreementStartDate = agreementQuery.First().Contains("apttus_contract_start_date") ? Convert.ToDateTime(agreementQuery.First()["apttus_contract_start_date"]) : (DateTime?)null;
						obj.AgreementEnddate = agreementQuery.First().Contains("apttus_contract_end_date") ? Convert.ToDateTime(agreementQuery.First()["apttus_contract_end_date"]) : (DateTime?)null;
						obj.AgreementStatus = agreementQuery.First().Contains("apttus_status") ? this.GetAgreementStatus(((Microsoft.Xrm.Sdk.OptionSetValue)(agreementQuery.First()["apttus_status"])).Value.ToString()) : "";
						obj.AgreementStatusCategory = agreementQuery.First().Contains("apttus_status_category") ? this.GetAgreementStatusCategory(((Microsoft.Xrm.Sdk.OptionSetValue)(agreementQuery.First()["apttus_status_category"])).Value.ToString()) : "";
						obj.AgreementRecordType = agreementQuery.First().Contains("apttus_recordtypeid") ? ((Microsoft.Xrm.Sdk.EntityReference)(agreementQuery.First()["apttus_recordtypeid"])).Id.ToString() : "";
						obj.AgreementOwnerID = agreementQuery.First().Contains("ownerid") ? ((Microsoft.Xrm.Sdk.EntityReference)(agreementQuery.First()["ownerid"])).Id.ToString() : "";
						obj.AgreementAccount = agreementQuery.First().Contains("apttus_accountid") ? ((Microsoft.Xrm.Sdk.EntityReference)(agreementQuery.First()["apttus_accountid"])).Id.ToString() : "";
						obj.AgreementOwner = agreementQuery.First().Contains("agreementUser.fullname") ? ((Microsoft.Xrm.Sdk.AliasedValue)((agreementQuery.First()).Attributes["agreementUser.fullname"])).Value.ToString() : "";
					}
				}
                exceutionContext.FileDataLog.ToList().Where(x => x.AgreementGUID == "").ToList().ForEach(x => { x.HasError = true; x.Status = UploadStatus.InvalidAgreementNumber; });

                var docVersionPending = exceutionContext.FileDataLog.Where(x => !string.IsNullOrEmpty(x.AgreementGUID ) && x.HasError == false)
                                                                    .Select(x => new
                                                                    {
                                                                        AgreementNumber = x.AgreementNumber,
                                                                        AgreementDocumentType = x.AgreementDocumentType,
                                                                        AgreementGUID = x.AgreementGUID.ToString(),
                                                                        FilePath = x.FilePathToUpload
                                                                    }).Distinct().ToList();

                Entity newDocumentVersionRecord;
                List<Entity> newDocumentversionList = new List<Entity>();
                Decimal numberofVersion = (Decimal)1.0000;
                foreach (var log in docVersionPending)
                {
                    Guid agreeId = Guid.Empty;
                    newDocumentVersionRecord = new Entity("apttus_documentversion");
                    newDocumentVersionRecord["apttus_agreementidid"] = new EntityReference("apttus_apts_agreement", Guid.Parse(log.AgreementGUID));
                    newDocumentVersionRecord["apttus_name"] = log.AgreementGUID.ToString();
                    newDocumentVersionRecord["apttus_documenttype"] = new OptionSetValue(Convert.ToInt32(log.AgreementDocumentType));
                    newDocumentVersionRecord["apttus_latestversion"] = "1.0";
                    newDocumentVersionRecord["apttus_numberofversions"] = numberofVersion;

                    newDocumentversionList.Add(newDocumentVersionRecord);
                }

                //Perform Database Insert on DocumentVersion records
                ExecuteMultipleResponse newDocumentVersionResponse = helper.ChunkAndExecute(newDocumentversionList, exceutionContext.CRM_MaxInsertChunk);

                int i = 0;
                foreach (var log in docVersionPending)
                {
                    ExecuteMultipleResponseItem responseItem = newDocumentVersionResponse.Responses[i];
                    var fileLogData = exceutionContext.FileDataLog.ToList().Where(x => x.AgreementNumber == log.AgreementNumber &&
                                                                                     x.AgreementDocumentType == log.AgreementDocumentType &&
                                                                                     x.FilePathToUpload == log.FilePath);
                    if (responseItem.Response != null && responseItem.Fault == null)
                    {
                        foreach (var logData in fileLogData)
                        {
                            logData.DocumentVersion = responseItem.Response.Results["id"].ToString();
                            logData.Status = UploadStatus.DocumentVersionCreated;
                            logData.BlobDetail = "Document Version Created.";
                        }
                    }
                    else
                    {
                        foreach (var logData in fileLogData)
                        {
                            logData.BlobDetail = "Document version was not created or Agreement do not exist";
                            logData.HasError = true;
                            logData.Status = UploadStatus.DocumentVersionDetailCreationFailed;
                            exceutionContext.SkippedFileDataLog.Add(logData);
                        }
                    }
                    i = i + 1;
                }

				#endregion
				exceutionContext.FileDataLog.ToList().Where(x => ((x.DocumentVersion == null || x.DocumentVersion == "" || x.DocumentVersion.Trim() == "") && x.HasError == false)).ToList().ForEach(x => { x.HasError = true; x.Status = UploadStatus.DocumentVersionCreationFailed; });
				exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionSyncDone;
			}
            catch (Exception ex)
            {
                exceutionContext.FileDataLog.ToList().ForEach(x => { x.HasError = x.DocumentVersion == "" ? true : false; });
                exceutionContext.ContinueOnError = true;
                throw new Exception("Error occured while Document Version Sync."  + Environment.NewLine + ex.Message);
            }
        }
		private string GetAgreementStatusCategory(string statusCategory)
		{
			switch (statusCategory)
			{
				case "100000000":
					{
						return "Request";
					}
				case "100000001":
					{
						return "In Authoring";
					}
				case "100000002":
					{
						return "In Signatures";
					}
				case "100000003":
					{
						return "In Filing";
					}
				case "100000004":
					{
						return "In Effect";
					}
				case "100000005":
					{
						return "Expired";
					}
				case "100000006":
					{
						return "Terminated";
					}
				case "100000007":
					{
						return "Amended";
					}
				case "100000008":
					{
						return "Cancelled";
					}
				case "100000009":
					{
						return "Renewed";
					}
				default:
					{
						return "Request";
					}
			}
		}
		private string GetAgreementStatus(string status)
		{
			switch (status)
			{
				case "100000027":
					{
						return "Pending Approval";
					}
				case "100000028":
					{
						return "Rejected Approval";
					}
				case "1000000000":
					{
						return "Request";
					}
				case "100000001":
					{
						return "Request Approval";
					}
				case "100000002":
					{
						return "Submitted Request";
					}
				case "100000003":
					{
						return "Approved Request";
					}
				case "100000004":
					{
						return "Cancelled Request";
					}
				case "100000005":
					{
						return "In Amendment";
					}
				case "100000006":
					{
						return "Author Contract";
					}
				case "100000007":
					{
						return "Language Approval";
					}
				case "100000008":
					{
						return "Language Approved";
					}
				case "100000009":
					{
						return "Internal Signatures";
					}
				case "100000010":
					{
						return "Other Party Signatures";
					}
				case "100000011":
					{
						return "Activated";
					}
				case "100000012":
					{
						return "Being Amended";
					}
				case "100000013":
					{
						return "Being Renewed";
					}
				case "100000014":
					{
						return "In Renewal";
					}
				case "100000015":
					{
						return "Superseded";
					}
				case "100000016":
					{
						return "Being Terminated";
					}
				case "100000017":
					{
						return "Terminated";
					}
				case "100000018":
					{
						return "Expired";
					}
				case "100000019":
					{
						return "In Reconciliation";
					}
				case "100000020":
					{
						return "Reconciled";
					}
				case "100000021":
					{
						return "Other Party Review";
					}
				case "100000022":
					{
						return "Signature Declined";
					}
				case "100000023":
					{
						return "Ready for Signatures";
					}
				case "100000024":
					{
						return "Fully Signed";
					}
				case "100000025":
					{
						return "Approval Required";
					}
				case "100000026":
					{
						return "Pending Other Party Signature";
					}
				default:
					{
						return "Request";
					}
			}
		}
	}
}
