﻿using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.Plugins
{
    class DownloadFileFromBlob : PluginExecution
    {
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                var fileToProcess = exceutionContext.FileDataLog.Where(x => x.HasError == false);
                foreach (FileLogEntry fileLog in fileToProcess)
                {
                    string filepath = fileLog.FilePathToUpload;
                    string filename = fileLog.FileName;

                    String containerName = exceutionContext.AzureBlobContainer;
                    //Inititalize Azure Blob Storage connection
                    BlobStorageManager uploadToBlobStorage = new BlobStorageManager(containerName);

                    try
                    {
                        string blobName = fileLog.DocumentVersionDetail + "/" + filename;
                        if (uploadToBlobStorage.DownloadFromBlob(fileLog))
                        {
                            fileLog.IsProcessed = true;
                            fileLog.Status = UploadStatus.FileDownloaded;
                            fileLog.BlobDetail = "File downloaded.";
                        }
                        else
                        {
                            fileLog.BlobDetail = "Cannot download file: " + blobName;
                            fileLog.Status = UploadStatus.FileDownloadFailed;
                            fileLog.HasError = true;
                        }

                        fileLog.IsProcessed = true;
                    }
                    catch (Exception ex)
                    {
                        fileLog.IsProcessed = false;
                        fileLog.HasError = true;
                        fileLog.BlobDetail = "Message: "  + Environment.NewLine + ex.Message + "Stack Trace : " + ex.StackTrace.ToString();
                    }
                }
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.BlobFileUploadDone;
            }
            catch (Exception ex)
            {
                exceutionContext.FileDataLog.ToList().ForEach(x => { x.HasError = x.IsProcessed; });
                exceutionContext.ContinueOnError = true;
                throw new Exception("Error While Uploading documents to blob."  + Environment.NewLine + ex.Message);
            }
        }
    }
}
