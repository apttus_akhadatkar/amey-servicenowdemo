﻿using Apttus.Model;
using System;
using System.Linq;

namespace Apttus.Plugins
{
    class UploadFileToBlob : PluginExecution
    {
        Helper helper;
        public UploadFileToBlob()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            //Process DocumentVersion Detail Response and Upload Attachments
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.BlobFileUploadInProgress;
            try
            {
                var fileToProcess = exceutionContext.FileDataLog.Where(x => x.HasError == false);
                foreach (FileLogEntry fileLog in fileToProcess)
                {
                    string filepath = fileLog.FilePathToUpload;
                    string filename = fileLog.FileName;

                    String containerName = exceutionContext.AzureBlobContainer;
                    //Inititalize Azure Blob Storage connection
                    BlobStorageManager uploadToBlobStorage = new BlobStorageManager(containerName);

                    try
                    {
                        string blobName = fileLog.DocumentVersionDetail + "/" + filename;
                        if (uploadToBlobStorage.UploadToBlob(blobName, filepath, exceutionContext.CRMUser.LoggedInUser))
                        {
                            fileLog.IsProcessed = true;
                            fileLog.Status = UploadStatus.FileUploaded;
                            fileLog.BlobDetail = "File uploaded to Blob.";
                        }
                        else
                        {
                            fileLog.BlobDetail = "Cannot Find Blob file on Path: " + filepath;
                            fileLog.Status = UploadStatus.BlobUploadFailed;
                            fileLog.HasError = true;
                        }

                        fileLog.IsProcessed = true;
                    }
                    catch (Exception ex)
                    {
                        fileLog.IsProcessed = false;
                        fileLog.HasError = true;
                        fileLog.BlobDetail = "Message: "  + Environment.NewLine + ex.Message + "Stack Trace : " + ex.StackTrace.ToString();
                    }
                }
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.BlobFileUploadDone;
            }
            catch (Exception ex)
            {
                exceutionContext.FileDataLog.ToList().ForEach(x => { x.HasError = x.IsProcessed; });
                exceutionContext.ContinueOnError = true;
                throw new Exception("Error While Uploading documents to blob."  + Environment.NewLine + ex.Message);
            }
        }
    }
}
