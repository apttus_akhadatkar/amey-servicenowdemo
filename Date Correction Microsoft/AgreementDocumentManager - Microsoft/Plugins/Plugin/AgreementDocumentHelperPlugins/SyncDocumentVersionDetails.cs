﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Apttus.Plugins
{
    class SyncDocumentVersionDetails : PluginExecution
    {
        Helper helper;
        public SyncDocumentVersionDetails()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionDetailSyncInProgress;
            try
            {
                Entity documentVersionDetailRecord;
                List<Entity> newDocumentversionDetailList = new List<Entity>();
				var FileData = exceutionContext.FileDataLog.Where(x => x.HasError == false || (x.DocumentVersion != null && x.DocumentVersion != ""));
				List<int> FileSizeList = new List<int>();
                int fileSize = 0;
                foreach (FileLogEntry obj in FileData)
                {
                    documentVersionDetailRecord = new Entity("apttus_documentversiondetail");
                    documentVersionDetailRecord["apttus_documentversionidid"] = new EntityReference("apttus_documentversion", Guid.Parse(obj.DocumentVersion));
                    documentVersionDetailRecord["apttus_name"] = "1.0.0";
                    documentVersionDetailRecord["apttus_title"] = obj.FileName; // Insert the title of the Document
                    documentVersionDetailRecord["apttus_comment"] = exceutionContext.CurrentUniqueRunIdentifier;
					obj.DocumentSecurity = "100000000";
					if (!string.IsNullOrEmpty(obj.DocumentSecurity))
                        documentVersionDetailRecord["apttus_documentsecurity"] = new OptionSetValue(Convert.ToInt32(obj.DocumentSecurity));
                    if (obj.AgreementDocumentType == BlobStorageAppResources.EXECUTED)
                    {
                        documentVersionDetailRecord["apttus_isexecuted"] = true;
                    }
                    newDocumentversionDetailList.Add(documentVersionDetailRecord);
                }

                //Perform Databse Insert on DocumentVersionDetail records
                ExecuteMultipleResponse newDocumentVersionDetailResponse = helper.ChunkAndExecute(newDocumentversionDetailList, exceutionContext.CRM_MaxInsertChunk, FileSizeList);
                //OrganizationResponse responseItem = helper.ExecuteEntitySave(documentVersionDetailRecord, true);

                int index = 0;
                foreach (FileLogEntry fileLog in FileData)
                {
                    ExecuteMultipleResponseItem responseItem = newDocumentVersionDetailResponse.Responses[index];
                    if (responseItem.Response != null && responseItem.Fault == null)
                    {
                        fileLog.DocumentVersionDetail = responseItem.Response.Results["id"].ToString();
                        fileLog.Status = UploadStatus.DocumentVersionDetailCreated;
                    }
                    else
                    {
                        fileLog.BlobDetail = "Document version Detail was not created or Agreement do not exist";
                        fileLog.HasError = true;
                        fileLog.Status = UploadStatus.DocumentVersionDetailCreationFailed;
                        exceutionContext.SkippedFileDataLog.Add(fileLog);
                    }
                    index = index + 1;
                }
                EntityCollection annotationsList;
                annotationsList = helper.ChunkAndFetchEntity(exceutionContext.FileDataLog.Where(x => x.DocumentVersionDetail != "").Select(x => x.DocumentVersionDetail).ToList(), "annotation", exceutionContext.CRM_MaxQueryConditionChunk);
                foreach (Entity entity in annotationsList.Entities)
                {
                    var DoumentVersionDetailQuery = exceutionContext.FileDataLog.Where(x => x.DocumentVersionDetail == ((Microsoft.Xrm.Sdk.EntityReference)(entity.Attributes["objectid"])).Id.ToString());
                    if (DoumentVersionDetailQuery.Count() > 0)
                    {
                        FileLogEntry objFile = DoumentVersionDetailQuery.First();
                        objFile.Annotations = entity.Id.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                exceutionContext.FileDataLog.ToList().ForEach(x => { x.HasError = x.DocumentVersionDetail == "" ? true : false; });
                exceutionContext.ContinueOnError = true;
                throw new Exception("Error occured while Document Version Detail Sync.");
            }
        }
        private Entity CreateNoteAttachment(string targetObject, String documentBody, string subject, string filename)
        {
            try
            {
                Guid attachmentId = Guid.Empty;
                Entity note = new Entity("annotation");
                note["subject"] = subject;
                note["filename"] = filename;
                note["documentbody"] = documentBody;
                note["mimetype"] = "application/octet-stream";
                return note;
            }
            catch (Exception e)
            {
                //String errorMessage = string.Format(ContractConstants.ERROR_CREATE_ATTACHMENT, e.Message);
                //throw new InvalidPluginExecutionException(errorMessage);
                throw e;
            }
        }
    }
}
