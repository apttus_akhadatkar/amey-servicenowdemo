﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Apttus.Plugins
{
    public class SyncDocumentVersion : PluginExecution
    {
        Helper helper;
        public SyncDocumentVersion()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionSyncInProgress;
            try
            {
                #region Fetch DocumentVersion and populate Map of Agreement Number To DocumentVersion ID

                //Query to existing Document version from CRM using Agreement numbers provided in the CSV file
                EntityCollection DocumentVersionEntityList = helper.ChunkAndFetchEntity(exceutionContext.AgreementsToProcess, "apttus_documentversion", exceutionContext.CRM_MaxQueryConditionChunk);

                foreach (FileLogEntry obj in exceutionContext.FileDataLog)
                {
                    var agreementGuidQuery = DocumentVersionEntityList.Entities.Where(x => ((AliasedValue)x["agreement.apttus_agreement_number"]).Value.ToString() == obj.AgreementNumber);
                    obj.AgreementGUID = agreementGuidQuery.Count() > 0 ? ((Microsoft.Xrm.Sdk.EntityReference)((agreementGuidQuery.First()).Attributes["apttus_agreementidid"])).Id.ToString() : "";
                    var docVersionQuery = DocumentVersionEntityList.Entities.
                        Where(x => ((AliasedValue)x["agreement.apttus_agreement_number"]).Value.ToString() == obj.AgreementNumber &&
                        ((OptionSetValue)x["apttus_documenttype"]).Value.ToString() == obj.AgreementDocumentType);
                    obj.DocumentVersion = docVersionQuery.Count() > 0 ? docVersionQuery.First()["apttus_documentversionid"].ToString() : "";
                }
                List<string> filteredAgreeNumList = exceutionContext.FileDataLog.Where(x => x.DocumentVersion == "").Select(x => x.AgreementNumber).ToList();

                if (filteredAgreeNumList.Count > 0)
                {
                    EntityCollection AgreementEntityList = helper.ChunkAndFetchEntity(filteredAgreeNumList, "apttus_apts_agreement", exceutionContext.CRM_MaxQueryConditionChunk);

                    foreach (FileLogEntry obj in exceutionContext.FileDataLog.Where(x => x.DocumentVersion == ""))
                    {
                        var agreementQuery = AgreementEntityList.Entities.Where(x => (x["apttus_agreement_number"]).ToString() == obj.AgreementNumber);
                        obj.AgreementGUID = agreementQuery.Count() > 0? agreementQuery.First()["apttus_apts_agreementid"].ToString() : "";
                    }
                    exceutionContext.FileDataLog.ToList().Where(x => x.AgreementGUID == "").ToList().ForEach(x => { x.HasError = true; x.Status = UploadStatus.InvalidAgreementNumber; });

                    var docVersionPending = exceutionContext.FileDataLog.Where(x => x.DocumentVersion == "" && x.AgreementGUID != "" && x.HasError == false)
                                                                        .Select(x => new
                                                                        {
                                                                            AgreementNumber = x.AgreementNumber,
                                                                            AgreementDocumentType = x.AgreementDocumentType,
                                                                            AgreementGUID = x.AgreementGUID.ToString()
                                                                        }).Distinct().ToList();

                    Entity newDocumentVersionRecord;
                    List<Entity> newDocumentversionList = new List<Entity>();
                    foreach (var log in docVersionPending)
                    {
                        Guid agreeId = Guid.Empty;
                        newDocumentVersionRecord = new Entity("apttus_documentversion");
                        newDocumentVersionRecord["apttus_agreementidid"] = new EntityReference("apttus_apts_agreement", Guid.Parse(log.AgreementGUID));
                        newDocumentVersionRecord["apttus_name"] = log.AgreementGUID.ToString();
                        if (BlobStorageAppResources.EXECUTED == log.AgreementDocumentType)
                        {
                            newDocumentVersionRecord["apttus_documenttype"] = new OptionSetValue(Convert.ToInt32(BlobStorageAppResources.EXECUTED));
                        }
                        else
                        {
                            newDocumentVersionRecord["apttus_documenttype"] = new OptionSetValue(Convert.ToInt32(BlobStorageAppResources.OTHER));
                        }
                        newDocumentversionList.Add(newDocumentVersionRecord);
                    }

                    //Perform Database Insert on DocumentVersion records
                    ExecuteMultipleResponse newDocumentVersionResponse = helper.ChunkAndExecute(newDocumentversionList, exceutionContext.CRM_MaxInsertChunk);

                    int i = 0;
                    foreach (var log in docVersionPending)
                    {
                        ExecuteMultipleResponseItem responseItem = newDocumentVersionResponse.Responses[i];
                        var fileLogData = exceutionContext.FileDataLog.ToList().Where(x => x.AgreementNumber == log.AgreementNumber &&
                                                                                         x.AgreementDocumentType == log.AgreementDocumentType);
                        if (responseItem.Response != null && responseItem.Fault == null)
                        {
                            foreach (var logData in fileLogData)
                            {
                                logData.DocumentVersion = responseItem.Response.Results["id"].ToString();
                                logData.Status = UploadStatus.DocumentVersionCreated;
                            }
                        }
                        else
                        {
                            foreach (var logData in fileLogData)
                            {
                                logData.BlobDetail = "Document version was not created or Agreement do not exist";
                                logData.HasError = true;
                                //exceutionContext.FileDataLog.Remove(logData);
                                logData.Status = UploadStatus.DocumentVersionDetailCreationFailed;
                                exceutionContext.SkippedFileDataLog.Add(logData);
                            }
                        }
                        i = i + 1;
                    }
                }
                #endregion

                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionSyncDone;
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = true;
                throw new Exception("Error occured while Document Version Sync."  + Environment.NewLine + ex.Message);
            }
        }
    }
}
