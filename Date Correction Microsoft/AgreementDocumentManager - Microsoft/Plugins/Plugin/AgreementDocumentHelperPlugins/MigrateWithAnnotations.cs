﻿using Apttus.Model;
using Apttus.Plugins.Helpers;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Apttus.Plugins
{
    public class MigrateWithAnnotations : PluginExecution
    {
        Helper helper;
        ConcurrentQueue<Entity> createdDocumentVersions = null;
        public MigrateWithAnnotations()
        {
            helper = new Helper();
            createdDocumentVersions = new ConcurrentQueue<Entity>();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionSyncInProgress;
            try
            {
                var dataToProcess = exceutionContext.FileDataLog.Where(x => x.HasError == false);
                Parallel.ForEach(dataToProcess, fileLogEntry =>
                {
                    ProcessFile(fileLogEntry);
                });
                ExecuteMultipleResponse newDocumentVersionResponse = helper.ChunkAndExecute(createdDocumentVersions.ToList(), exceutionContext.CRM_MaxInsertChunk);
                //var Data = helper.ExecuteMultiple(createdDocumentVersions.ToList(), EnumOperationType.Insert);
                int i = 0;
                foreach (var log in dataToProcess)
                {
                    ExecuteMultipleResponseItem responseItem = newDocumentVersionResponse.Responses[i];
                    var fileLogData = exceutionContext.FileDataLog.ToList().Where(x => x.AgreementNumber == log.AgreementNumber &&
                                                                                     x.AgreementDocumentType == log.AgreementDocumentType &&
                                                                                     x.FilePathToUpload == log.FilePathToUpload);
                    if (responseItem.Response != null && responseItem.Fault == null)
                    {
                        foreach (var logData in fileLogData)
                        {

                            if (responseItem.Response.Results.Contains("id") && responseItem.Response.Results["id"] != null)
                            {
                                logData.DocumentVersion = responseItem.Response.Results["id"].ToString();
                                logData.Status = UploadStatus.DocumentVersionCreated;
                                logData.IsProcessed = true;
                                logData.HasError = false;
                                logData.BlobDetail = "Migration with annotations completed.";
                            }
                        }
                    }
                    else
                    {
                        foreach (var logData in fileLogData)
                        {
                            logData.BlobDetail = "Migration could not be completed or Agreement do not exist";
                            logData.HasError = true;
                            logData.IsProcessed = false;
                            logData.Status = UploadStatus.DocumentVersionDetailCreationFailed;
                            exceutionContext.SkippedFileDataLog.Add(logData);
                        }
                    }
                    i = i + 1;
                }

                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.BlobFileUploadDone;
            }
            catch (Exception ex)
            {

            }
        }

        private void ProcessFile(FileLogEntry fileData)
        {
            var documentVersion = createDocumentVersion(new Guid(fileData.AgreementGUID), fileData.AgreementDocumentType);
            var documentVersionDetailRecord = createDocumentVersionDetail(Convert.ToDecimal(1.0), fileData.AgreementDocumentType);

            string targetObject = "apttus_documentversiondetail";

            var note = CreateNoteAttachment(targetObject, Convert.ToBase64String(File.ReadAllBytes(fileData.FilePathToUpload)), "Migration of Agreement Document", fileData.FileName);
            EntityCollection relatedNoteEntities = new EntityCollection();
            relatedNoteEntities.Entities.Add(note);

            Relationship noterelationship = new Relationship("apttus_documentversiondetail_Annotations");
            //noterelationship.PrimaryEntityRole = EntityRole.Referenced;
            documentVersionDetailRecord.RelatedEntities.Add(noterelationship, relatedNoteEntities);

            EntityCollection relatedEntitiestobecreated = new EntityCollection();
            relatedEntitiestobecreated.Entities.Add(documentVersionDetailRecord);

            Relationship documentVersionRelationship = new Relationship("Apttus__DocumentVersionId__r1500008823");
            documentVersion.RelatedEntities.Add(documentVersionRelationship, relatedEntitiestobecreated);
            //Console.WriteLine($"Document Version created for : {DateTime.Now}");
            createdDocumentVersions.Enqueue(documentVersion);

            fileData.BlobDetail = "Added to queue for Annotations.";
        }

        private string ConvertToBase64String(FileStream inputFileStream)
        {
            //int BASE64_BLOCK_SIZE = 3;
            //byte[] base64Block = new byte[BASE64_BLOCK_SIZE];
            //StringBuilder resultString = new StringBuilder();
            //int bytesRead = 0;
            //do
            //{
            //    // read one block from the input stream
            //    bytesRead = inputFileStream.Read(base64Block, 0, base64Block.Length);
            //    // encode the base64 string
            //    string base64String = Convert.ToBase64String(base64Block, 0, bytesRead);
            //    // write the string
            //    resultString.Append(base64String);
            //} while (bytesRead == base64Block.Length);
            //return resultString.ToString();
            string result = string.Empty;
            using (var cs = new CryptoStream(inputFileStream, new ToBase64Transform(), CryptoStreamMode.Read))
            {
                using (var sr = new StreamReader(cs))
                {
                    result = sr.ReadToEnd();
                }
            }

            return result;
        }

        private Entity createDocumentVersion(Guid agreementId, string agreementDocumentType)
        {
            try
            {
                Decimal numberofVersion = (Decimal)1.0000;
                Entity documentVersionRecord = new Entity("apttus_documentversion");
                documentVersionRecord["apttus_agreementidid"] = new
                            EntityReference("apttus_apts_agreement", agreementId);
                documentVersionRecord["apttus_title"] = "Generate Document";
                documentVersionRecord["apttus_name"] = "Generate Document";
                documentVersionRecord["apttus_latestversion"] = "1.0";
                documentVersionRecord["apttus_numberofversions"] = numberofVersion;
                documentVersionRecord["apttus_documenttype"] = new OptionSetValue(Convert.ToInt32(agreementDocumentType));
                return documentVersionRecord;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private Entity createDocumentVersionDetail(decimal majorVersion, string agreementDocumentType)
        {
            try
            {
                Guid documentVersionDetailRecordId = Guid.Empty;
                Entity documentVersionDetailRecord = new Entity("apttus_documentversiondetail");
                documentVersionDetailRecord["apttus_title"] = "Generate Document";
                documentVersionDetailRecord["apttus_name"] = "1.0.0";
                documentVersionDetailRecord["apttus_versionmajor"] = majorVersion;
                if (agreementDocumentType == BlobStorageAppResources.EXECUTED)
                {
                    documentVersionDetailRecord["apttus_isexecuted"] = true;
                }
                return documentVersionDetailRecord;
            }
            catch (Exception e)
            {
                //String errorMessage = string.Format(ContractConstants.ERRORCREATEDVD, e.Message);
                //throw new InvalidPluginExecutionException(errorMessage);
                throw e;
            }
        }

        private Entity CreateNoteAttachment(string targetObject, String documentBody, string subject, string filename)
        {
            try
            {
                Guid attachmentId = Guid.Empty;
                Entity note = new Entity("annotation");
                note["subject"] = subject;
                note["filename"] = filename;
                note["documentbody"] = documentBody;
                note["mimetype"] = "application/octet-stream";
                return note;
            }
            catch (Exception e)
            {
                //String errorMessage = string.Format(ContractConstants.ERROR_CREATE_ATTACHMENT, e.Message);
                //throw new InvalidPluginExecutionException(errorMessage);
                throw e;
            }
        }
    }
}
