﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apttus.Model;
using System.IO;
using Microsoft.Xrm.Sdk;

namespace Apttus.Plugins
{
    public class BlobFileUpload : PluginExecution
    {
        Helper helper;
        public BlobFileUpload()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.BlobFileUploadInProgress;
                #region Initialization and Connection to Azure Blob Storage

                String containerName = exceutionContext.AzureBlobContainer;
                //Inititalize Azure Blob Storage connection
                UploadToBlobStorage uploadToBlobStorage = new UploadToBlobStorage(containerName);

                #endregion

                #region Process Document Version Detail response and Upload Attachments

                //Process DocumentVersion Detail Response and Upload Attachments
                int uploadedCount = 0; //Sucessfully uploaded count
                int compltedtTillNow = 0;
                string documentVersionDetailID;

                var logResults = new StringBuilder();
                #region Create and Insert Document Version Detail records

                //Create new Document versions details records for Agreement Number
                Entity documentVersionDetailRecord;
                foreach (var line in exceutionContext.InputFileDataLines)
                {
                    string agreeNum = line.Line.Split(exceutionContext.SplitDelimeter)[0];
                    string executedDocumentvalue = line.Line.Split(exceutionContext.SplitDelimeter)[3];  //Change #1
                    if (BlobStorageAppResources.EXECUTEDSTRING == executedDocumentvalue)
                    {
                        executedDocumentvalue = BlobStorageAppResources.EXECUTED;
                    }
                    else
                    {
                        executedDocumentvalue = BlobStorageAppResources.OTHER;
                    }
                    // Start Change by Anshul
                    string fileName = line.Line.Split(exceutionContext.SplitDelimeter)[2];
                    Guid docverId = Guid.Empty;


                    String agrNumAndPath = agreeNum + "|" + line.Line.Split(exceutionContext.SplitDelimeter)[1];
                    if (exceutionContext.AgreeNumToDocVersionIDMap.TryGetValue(agreeNum + "-" + executedDocumentvalue, out docverId))
                    {

                        documentVersionDetailRecord = new Entity("apttus_documentversiondetail");
                        documentVersionDetailRecord["apttus_documentversionidid"] = new EntityReference("apttus_documentversion", docverId);
                        documentVersionDetailRecord["apttus_name"] = docverId.ToString();
                        documentVersionDetailRecord["apttus_title"] = fileName; // Insert the title of the Document
                        documentVersionDetailRecord["apttus_comment"] = exceutionContext.CurrentUniqueRunIdentifier;
                        if (executedDocumentvalue == BlobStorageAppResources.EXECUTED)
                        {
                            documentVersionDetailRecord["apttus_isexecuted"] = true;
                        }

                        OrganizationResponse responseItem = helper.ExecuteEntitySave(documentVersionDetailRecord, true);

                        string filepath = line.Line.Split(exceutionContext.SplitDelimeter)[1];//Attachment local file path
                        string filename = line.Line.Split(exceutionContext.SplitDelimeter)[2];//Attachment file name

                        FileLogEntry fileLog = exceutionContext.FileDataLog.Where(x => x.UniqueIdentifier == exceutionContext.CurrentUniqueRunIdentifier + "_" + agreeNum + "-" + executedDocumentvalue + "_" + filepath).FirstOrDefault();
                        //Entity entity = exceutionContext.BlobRunFileMap[exceutionContext.CurrentUniqueRunIdentifier + "_" + agreeNum + filepath];

                        //ExecuteMultipleResponseItem responseItem = exceutionContext.newDocumentVersionDetailResponse.Responses[lineNo];

                        if (responseItem != null)
                        {
                            documentVersionDetailID = responseItem.Results["id"].ToString();
                            fileLog.DocumentVersionDetail = documentVersionDetailID;


                            string blobName = documentVersionDetailID + "/" + filename;

                            try
                            {
                                if (uploadToBlobStorage.UploadToBlob(blobName, filepath, exceutionContext.CRM_Username))
                                {
                                    uploadedCount++;
                                    fileLog.IsProcessed = true;
                                }
                                else
                                {
                                    fileLog.BlobDetail = "Cannot Find Blob file on Path: " + filepath;
                                }
                                compltedtTillNow = compltedtTillNow + 1;
                                if (compltedtTillNow % 1500 == 0)
                                {
                                    Console.WriteLine(compltedtTillNow + " Records Processes till " + (DateTime.Now).ToString());
                                }
                            }
                            catch (Exception e)
                            {
                                String errorDetails = e.StackTrace.ToString();
                                fileLog.BlobDetail = (errorDetails.Count() > 3999) ? errorDetails.Substring(0, 3999) : errorDetails;
                            }
                            //exceutionContext.BlobRunFileMap[exceutionContext.CurrentUniqueRunIdentifier + "_" + agreeNum + filepath] = entity;
                        }
                        File.AppendAllText(exceutionContext.LogFilePath, String.Format("{0}{6}{1}{6}{2}{6}{3}{6}{4}{6}{5}", fileLog.AgreementNumber, fileLog.BlobSourcePath, fileLog.DocumentVersion, fileLog.DocumentVersionDetail, fileLog.IsProcessed, fileLog.IsProcessed, exceutionContext.SplitDelimeter) + Environment.NewLine);

                    }
                }

                #endregion

                if (exceutionContext.SkippedFileDataLog.Count > 0)
                {
                    foreach (FileLogEntry fileData in exceutionContext.SkippedFileDataLog)
                    {
                        File.AppendAllText(exceutionContext.LogFilePath, String.Format("{0}{6}{1}{6}{2}{6}{3}{6}{4}{6}{5}", fileData.AgreementNumber, fileData.BlobSourcePath, fileData.DocumentVersion, fileData.DocumentVersionDetail, fileData.IsProcessed, fileData.BlobDetail, exceutionContext.CurrentUniqueRunIdentifier) + Environment.NewLine);
                    }
                }

                String fileNameTochange = Path.GetFileName(exceutionContext.LogFilePath);
                int indexToRemove = fileNameTochange.IndexOf(BlobStorageAppResources.IN_PROGRESS);
                string finalFileName = (indexToRemove < 0)
                    ? fileNameTochange
                    : fileNameTochange.Remove(indexToRemove, BlobStorageAppResources.IN_PROGRESS.Length);
                File.Move(exceutionContext.LogFilePath, Path.GetDirectoryName(exceutionContext.LogFilePath) + Path.DirectorySeparatorChar + finalFileName);
                #endregion
                
            }
            catch (Exception ex)
            { }
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.BlobFileUploadDone;
        }
        

    }
}
