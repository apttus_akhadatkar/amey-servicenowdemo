﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Apttus.Plugins
{
    class CorrectDocumentVersionDetail : PluginExecution
    {
        Helper helper;
        public CorrectDocumentVersionDetail()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                Entity documentVersionDetailRecord;
                List<Entity> newDocumentversionDetailList = new List<Entity>();
                var documentsToProcess = exceutionContext.FileDataLog.Where(x => x.IsCorrected == false && x.HasError == false && x.DocumentVersion != "");
                foreach (FileLogEntry obj in documentsToProcess)
                {
                    documentVersionDetailRecord = new Entity("apttus_documentversiondetail");
                    documentVersionDetailRecord["apttus_documentversiondetailid"] = Guid.Parse(obj.DocumentVersionDetail);
                    documentVersionDetailRecord["apttus_documentversionidid"] = new EntityReference("apttus_documentversion", Guid.Parse(obj.DocumentVersion));
                    documentVersionDetailRecord["apttus_name"] = obj.DocumentVersion.ToString();
                    documentVersionDetailRecord["apttus_title"] = obj.DocumentVersionDetailTitle; // Insert the title of the Document
                    documentVersionDetailRecord["apttus_comment"] = exceutionContext.CurrentUniqueRunIdentifier;
                    if (obj.AgreementDocumentType == BlobStorageAppResources.EXECUTED)
                    {
                        documentVersionDetailRecord["apttus_isexecuted"] = true;
                    }
                    newDocumentversionDetailList.Add(documentVersionDetailRecord);
                    obj.BlobDetail = "Passed for Remapping of DocumentVersionDetail.";
                }

                //Perform Databse Insert on DocumentVersionDetail records
                ExecuteMultipleResponse newDocumentVersionDetailResponse = helper.ChunkAndExecute(newDocumentversionDetailList, EnumOperationType.Update, exceutionContext.CRM_MaxInsertChunk);
            }
            catch
            (Exception ex)
            {
                exceutionContext.ContinueOnError = true;
                throw new Exception("Error occured while Correcting Document Version Detail.");
            }
        }
    }
}
