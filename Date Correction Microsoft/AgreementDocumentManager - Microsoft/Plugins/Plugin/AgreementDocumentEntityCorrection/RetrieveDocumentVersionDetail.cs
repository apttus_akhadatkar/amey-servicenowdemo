﻿using Microsoft.Xrm.Sdk;
using Apttus.Model;
using System;
using System.Linq;

namespace Apttus.Plugins
{
    public class RetrieveDocumentVersionDetail : PluginExecution
    {
        Helper helper;
        public RetrieveDocumentVersionDetail()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.FileDataLog.Where(x => x.DocumentVersion == "").ToList().ForEach(x => x.HasError = true);
                EntityCollection DocumentVersionEntityList = helper.ChunkAndFetchEntity(exceutionContext.FileDataLog.Where(x => x.DocumentVersion != "").Select(x => x.DocumentVersion).ToList(), "apttus_documentversiondetail", exceutionContext.CRM_MaxQueryConditionChunk);

                foreach (Entity entity in DocumentVersionEntityList.Entities)
                {
                    var DoumentVersionQuery = exceutionContext.FileDataLog.Where(x => x.DocumentVersion == ((Microsoft.Xrm.Sdk.EntityReference)(entity.Attributes["apttus_documentversionidid"])).Id.ToString());
                    if (DoumentVersionQuery.Count() > 0)
                    {
                        FileLogEntry objFile = DoumentVersionQuery.First();
                        if (string.IsNullOrEmpty(objFile.DocumentVersionDetail))
                        {
                            objFile.DocumentVersionDetail = entity.Id.ToString();
                            if (entity.Contains("apttus_title"))
                                objFile.DocumentVersionDetailTitle = entity["apttus_title"].ToString();
                            objFile.IsCorrected = true;
                            objFile.BlobDetail = "Document Version and Document Version detail in sync.";
                        }
                        else
                        {
                            exceutionContext.FileDataLog.Add(new FileLogEntry()
                            {
                                AgreementNumber = objFile.AgreementNumber,
                                AgreementGUID = objFile.AgreementGUID,
                                DocumentVersionDetail = entity.Id.ToString(),
                                AgreementDocumentType = objFile.AgreementDocumentType,
                                DocumentVersionDetailTitle = entity.Contains("apttus_title") ? entity["apttus_title"].ToString() : "",
                                IsCorrected = false,
                                BlobDetail = "New Document Version to be created."
                            });
                        }
                    }
                }

                exceutionContext.FileDataLog.Where(x => string.IsNullOrEmpty(x.DocumentVersionDetail)).ToList().ForEach(
                        x =>
                        {
                            x.HasError = true;
                            x.BlobDetail = "No Document Version Detail Exist";
                        }
                    );
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw new Exception("Error occured while Retrieving Document Version Detail.");
            }
        }
    }
}
