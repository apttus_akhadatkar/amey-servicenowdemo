﻿using Microsoft.Xrm.Sdk;
using Apttus.Model;
using System;
using System.Linq;

namespace Apttus.Plugins
{
    public class RetrieveDocumentVersion : PluginExecution
    {
        Helper helper;
        public RetrieveDocumentVersion()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DocumentVersionSyncInProgress;

            try
            {
                EntityCollection DocumentVersionEntityList = helper.ChunkAndFetchEntity(exceutionContext.AgreementsToProcess.Distinct().ToList(), "apttus_documentversion", exceutionContext.CRM_MaxQueryConditionChunk);
                foreach (Entity entity in DocumentVersionEntityList.Entities)
                {
                    exceutionContext.FileDataLog.Add(new FileLogEntry()
                    {
                        AgreementNumber = entity.Contains("agreement.apttus_agreement_number") ? ((AliasedValue)entity["agreement.apttus_agreement_number"]).Value.ToString() : "",
                        AgreementGUID = ((Microsoft.Xrm.Sdk.EntityReference)entity.Attributes["apttus_agreementidid"]) != null ? ((Microsoft.Xrm.Sdk.EntityReference)entity.Attributes["apttus_agreementidid"]).Id.ToString() : "",
                        DocumentVersion = entity["apttus_documentversionid"] != null ? entity["apttus_documentversionid"].ToString() : "",
                        AgreementDocumentType = entity.Contains("apttus_documenttype") ? ((Microsoft.Xrm.Sdk.OptionSetValue)(entity["apttus_documenttype"])).Value.ToString() : ""
                    });
                }
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw new Exception("Error occured while Retrieving Document Version.");
            }
        }
    }
}
