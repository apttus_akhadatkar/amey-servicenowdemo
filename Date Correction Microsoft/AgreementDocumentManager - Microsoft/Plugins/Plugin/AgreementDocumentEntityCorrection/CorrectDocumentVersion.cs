﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Apttus.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Apttus.Plugins
{
    public class CorrectDocumentVersion : PluginExecution
    {
        Helper helper;
        public CorrectDocumentVersion()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            Entity newDocumentVersionRecord;
            try
            {
                List<Entity> newDocumentversionList = new List<Entity>();
                var documentsToProcess = exceutionContext.FileDataLog.Where(x => x.IsCorrected == false && x.HasError == false);
                documentsToProcess.ToList().ForEach(x =>
                {

                    newDocumentVersionRecord = new Entity("apttus_documentversion");
                    newDocumentVersionRecord["apttus_agreementidid"] = new EntityReference("apttus_apts_agreement", Guid.Parse(x.AgreementGUID));
                    newDocumentVersionRecord["apttus_name"] = x.AgreementGUID.ToString();
                    newDocumentVersionRecord["apttus_documenttype"] = string.IsNullOrEmpty(x.AgreementDocumentType) ? new OptionSetValue(Convert.ToInt32(BlobStorageAppResources.OTHER)) : new OptionSetValue(Convert.ToInt32(x.AgreementDocumentType));
                    newDocumentversionList.Add(newDocumentVersionRecord);
                    x.BlobDetail = "Added to queue to create new Document Version.";
                });

                ExecuteMultipleResponse newDocumentVersionResponse = helper.ChunkAndExecute(newDocumentversionList, exceutionContext.CRM_MaxInsertChunk);


                int i = 0;
                foreach (var log in documentsToProcess)
                {
                    ExecuteMultipleResponseItem responseItem = newDocumentVersionResponse.Responses[i];
                    if (responseItem.Response != null && responseItem.Fault == null)
                    {
                        log.DocumentVersion = responseItem.Response.Results["id"].ToString();
                        log.Status = UploadStatus.DocumentVersionCreated;
                        log.BlobDetail = "New Document version created succesfully.";
                    }
                    else
                    {
                        log.BlobDetail = "Document version was not created or Agreement do not exist";
                        log.HasError = true;
                        //exceutionContext.FileDataLog.Remove(log);
                        log.Status = UploadStatus.DocumentVersionDetailCreationFailed;
                        exceutionContext.SkippedFileDataLog.Add(log);

                    }
                    i = i + 1;
                }
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = true;
                throw new Exception("Error occured while Correcting Document Version.");
            }
        }

    }
}
