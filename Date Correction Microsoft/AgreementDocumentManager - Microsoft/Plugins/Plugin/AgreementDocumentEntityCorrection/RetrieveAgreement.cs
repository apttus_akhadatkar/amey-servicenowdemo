﻿using Apttus.Model;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.Plugins
{
    public class RetrieveAgreement : PluginExecution
    {
        Helper helper;
        public RetrieveAgreement()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            List<string> agreeNumList = exceutionContext.FileDataLog.Select(x => x.AgreementNumber).ToList();
            try
            {
                EntityCollection AgreementEntityList = helper.ChunkAndFetchEntity(agreeNumList, "apttus_apts_agreement", exceutionContext.CRM_MaxQueryConditionChunk);

                foreach (FileLogEntry obj in exceutionContext.FileDataLog.Where(x => x.DocumentVersion == ""))
                {
                    var agreementQuery = AgreementEntityList.Entities.Where(x => (x["apttus_agreement_number"]).ToString() == obj.AgreementNumber);
                    obj.AgreementGUID = agreementQuery.Count() > 0 ? agreementQuery.First()["apttus_apts_agreementid"].ToString() : "";
                }
                exceutionContext.FileDataLog.ToList().Where(x => x.AgreementGUID == "").ToList().ForEach(x => { x.HasError = true; x.Status = UploadStatus.InvalidAgreementNumber; });
            }
            catch (Exception ex)
            {

            }
        }
    }
}
