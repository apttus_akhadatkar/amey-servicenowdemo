﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Apttus.Plugins
{
    public class GetAgreementNumbers : PluginExecution
    {
        public override void Process(Model.IExecutionContext exceutionContext)
        {

            try
            {
                exceutionContext.AgreementsToProcess = readFile(exceutionContext.FilePath);
                this.CreateNewLogFile(exceutionContext);
            }

            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw ex;
            }
        }
        private static List<string> readFile(string filePath)
        {
            List<string> dataLines = new List<string>();
            try
            {
                using (StreamReader reader = new StreamReader(File.OpenRead(filePath)))
                {
                    while (!reader.EndOfStream)
                    {
                        String line = reader.ReadLine();
                        if (line != null && !String.IsNullOrWhiteSpace(line))
                            dataLines.Add(line.Replace("|",""));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in Reading file:" + Environment.NewLine + e.Message);
            }
            return dataLines;
        }
        public void CreateNewLogFile(Model.IExecutionContext exceutionContext)
        {
            //Get file to Read
            string strfile = "";
            String fullPath = exceutionContext.FilePath;//Path.GetFullPath(strfile);

            if (!exceutionContext.AllowLogs)
            {
                throw new Exception("No Write permission to save log file..");
            }

            //Create New LogFile
            String extension = Path.GetExtension(strfile);
            String sourceFileName = Path.GetFileNameWithoutExtension(strfile);

            if (fullPath.EndsWith(extension))
            {
                fullPath = fullPath.Substring(0, fullPath.LastIndexOf(extension));
            }
            exceutionContext.LogFilePath = Path.GetDirectoryName(fullPath) + Path.DirectorySeparatorChar + BlobStorageAppResources.IN_PROGRESS + sourceFileName + "_" + exceutionContext.CurrentUniqueRunIdentifier + Path.GetExtension(strfile);

        }
    }
}
