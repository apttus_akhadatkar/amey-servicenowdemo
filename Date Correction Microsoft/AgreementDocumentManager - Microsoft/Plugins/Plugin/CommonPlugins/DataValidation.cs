﻿using Apttus.Model;
using System;
using System.IO;
using System.Text;

namespace Apttus.Plugins
{
    public class DataValidation : PluginExecution
    {
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DataValidationInProgress;

                var invalidDataLength = exceutionContext.InputFileDataLines.FindAll(x => x.Line.Split(exceutionContext.SplitDelimeter).Length != 4);
                exceutionContext.InputFileDataLines.RemoveAll(x => invalidDataLength.Contains(x));

                var invalidAccess = exceutionContext.InputFileDataLines.FindAll(x => CheckFileAceess(x.Line.Split(exceutionContext.SplitDelimeter)[1]) == false);
                exceutionContext.InputFileDataLines.RemoveAll(x => invalidAccess.Contains(x));

                invalidAccess.ForEach(x => exceutionContext.InvalidDataLines.Add(new DataLine()
                {
                    Line = string.Concat(x.Line, "|", "Could  not access file or file doesn't exist."),
                    AgreementNumber = x.Line.Split('|')[0],
                    FileLocation = x.Line.Split('|')[1],
                    BlobFileName = x.Line.Split('|')[2],
                    DocumentType = x.Line.Split('|')[3],
                    Error = "Could  not access file or file doesn't exist."
                }));
                invalidDataLength.ForEach(x => exceutionContext.InvalidDataLines.Add(new DataLine() { Line = string.Concat(x.Line, "|", "Data line is not proper, please check again."), Error = "Data line is not proper, please check again." }));

                StringBuilder strLog = new StringBuilder();
                if (exceutionContext.InvalidDataLines.Count > 0)
                {
                    foreach (DataLine obj in exceutionContext.InvalidDataLines)
                    {
                        strLog.Append(obj.Line + Environment.NewLine);
                    }
                    if (File.Exists(exceutionContext.InvalidDataFilePath))
                    {
                        File.Delete(exceutionContext.InvalidDataFilePath);
                    }
                }
                if (exceutionContext.ValidationOnly)
                {
                    exceutionContext.InputFileDataLines.ForEach(x => exceutionContext.InvalidDataLines.Add(
                        new DataLine()
                        {
                            Line = string.Concat(x.Line, "|", "Could  not access file or file doesn't exist."),
                            AgreementNumber = x.Line.Split('|')[0],
                            FileLocation = x.Line.Split('|')[1],
                            BlobFileName = x.Line.Split('|')[2],
                            DocumentType = x.Line.Split('|')[3]
                        }
                        ));
                    exceutionContext.InputFileDataLines.ForEach(x => strLog.Append(x.Line + Environment.NewLine));
                }
                if (File.Exists(exceutionContext.InvalidDataFilePath))
                {
                    File.Delete(exceutionContext.InvalidDataFilePath);
                }
                File.AppendAllText(exceutionContext.InvalidDataFilePath, strLog.ToString());
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw new Exception("Data Validation Failed, please revisit your data or contact help desk." + Environment.NewLine + ex.Message);
            }
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.DataValidationDone;
        }
        private bool CheckFileAceess(string FileData)
        {
            if (File.Exists(FileData))
            {
                if (Helper.HasReadPermissionOnDirectory(FileData))
                {
                    return true;
                }
            }
            return false;

        }
    }
}
