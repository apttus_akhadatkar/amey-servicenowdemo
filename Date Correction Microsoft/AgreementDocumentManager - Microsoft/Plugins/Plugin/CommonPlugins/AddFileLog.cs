﻿using Apttus.Model;
using System;
using System.IO;
using System.Text;

namespace Apttus.Plugins
{
    public class AddFileLog : PluginExecution
    {
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                StringBuilder strLogBuilder = new StringBuilder();
                foreach (FileLogEntry fileLog in exceutionContext.FileDataLog)
                {
                    strLogBuilder.Append(String.Format("{0}{10}{1}{10}{2}{10}{3}{10}{4}{10}{5}{10}{6}{10}{7}{10}{8}{10}{9}",
                        fileLog.AgreementNumber,
                        fileLog.BlobSourcePath,
                        fileLog.DocumentVersion,
                        fileLog.DocumentVersionDetail,
                        fileLog.IsProcessed,
                        fileLog.CurrentRunIdentifier,
                        fileLog.DocumentType,
                        fileLog.FileName,
                        fileLog.AgreementGUID,
                        fileLog.Status,
                        exceutionContext.SplitDelimeter) + Environment.NewLine);
                }
                if (exceutionContext.SkippedFileDataLog != null)
                {
                    foreach (FileLogEntry fileLog in exceutionContext.SkippedFileDataLog)
                    {
                        strLogBuilder.Append(String.Format("{0}{10}{1}{10}{2}{10}{3}{10}{4}{10}{5}{10}{6}{10}{7}{10}{8}{10}{9}",
                            fileLog.AgreementNumber,
                            fileLog.BlobSourcePath,
                            fileLog.DocumentVersion,
                            fileLog.DocumentVersionDetail,
                            fileLog.IsProcessed,
                            fileLog.CurrentRunIdentifier,
                            fileLog.DocumentType,
                            fileLog.FileName,
                            fileLog.AgreementGUID,
                            fileLog.Status,
                            exceutionContext.SplitDelimeter) + Environment.NewLine);
                    }
                }
                if (File.Exists(exceutionContext.LogFilePath))
                {
                    File.Delete(exceutionContext.LogFilePath);
                }
                File.AppendAllText(exceutionContext.LogFilePath, strLogBuilder.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("Error while adding file log."  + Environment.NewLine + ex.Message);
            }
        }
    }
}
