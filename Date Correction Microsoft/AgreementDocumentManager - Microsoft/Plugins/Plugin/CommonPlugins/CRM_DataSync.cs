﻿using System;
using System.Collections.Generic;
using Apttus.Model;

namespace Apttus.Plugins
{
    public class CRM_DataSync : PluginExecution
    {
        Helper helper;
        public CRM_DataSync()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.FileDataCapturingInProgress;
                #region ProcessInputFileDataLines and Prepare the CRM data
                this.ProcessInputFileDataLines(exceutionContext);
                #endregion
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw new Exception("Error while preparing data for process, please revisit your data or contact help desk."  + Environment.NewLine + ex.Message);
            }
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.FileDataCapturingDone;

        }
        private void ProcessInputFileDataLines(Model.IExecutionContext exceutionContext)
        {
            if (exceutionContext.InputFileDataLines.Count > 0)
            {

                //Get all Agreement Numbers from file
                exceutionContext.AgreementsToProcess = new List<string>();
                exceutionContext.AgreementAndDocumentType = new List<string>();
                foreach (var line in exceutionContext.InputFileDataLines)
                {
                    //Get Agreement Number in a List "allAgreeNumList"
                    string agreeNum = line.Line.Split(exceutionContext.SplitDelimeter)[0];
                    string executedDocument = line.Line.Split(exceutionContext.SplitDelimeter)[3];  //Change #1
                    string agreeNumDocumentType = "";
                    string documentTypeValue = "";
                    if (!exceutionContext.AgreementsToProcess.Contains(agreeNum) && !string.IsNullOrEmpty(agreeNum))
                    {
                        exceutionContext.AgreementsToProcess.Add(agreeNum);

                    }
                    if (BlobStorageAppResources.EXECUTEDSTRING == executedDocument)
                    {
                        documentTypeValue = BlobStorageAppResources.EXECUTED;
                        agreeNumDocumentType = agreeNum + "-" + BlobStorageAppResources.EXECUTED;
                    }
                    else if (BlobStorageAppResources.AGREEMENTSTRING == executedDocument)
                    {
                        documentTypeValue = BlobStorageAppResources.AGREEMENT;
                        agreeNumDocumentType = agreeNum + "-" + BlobStorageAppResources.AGREEMENT;
                    }
                    else if (BlobStorageAppResources.SUPPORTINGSTRING == executedDocument)
                    {
                        documentTypeValue = BlobStorageAppResources.SUPPORTING;
                        agreeNumDocumentType = agreeNum + "-" + BlobStorageAppResources.SUPPORTING;
                    }
                    else
                    {
                        documentTypeValue = BlobStorageAppResources.OTHER;
                        agreeNumDocumentType = agreeNum + "-" + BlobStorageAppResources.OTHER;
                    }
                    if (!exceutionContext.AgreementAndDocumentType.Contains(agreeNumDocumentType) && !string.IsNullOrEmpty(agreeNumDocumentType))
                    {
                        //Prepare List with Agreement Number and Document type
                        exceutionContext.AgreementAndDocumentType.Add(agreeNumDocumentType);

                    }
                    exceutionContext.FileDataLog.Add(new FileLogEntry()
                    {
                        UniqueIdentifier = exceutionContext.CurrentUniqueRunIdentifier + "_" + agreeNumDocumentType + "_" + line.Line.Split(exceutionContext.SplitDelimeter)[1],
                        AgreementNumber = agreeNum,
                        Status = UploadStatus.FileValidated,
                        BlobSourcePath = line.Line.Split(exceutionContext.SplitDelimeter)[1],
                        CurrentRunIdentifier = exceutionContext.CurrentUniqueRunIdentifier,
                        DocumentVersion = "",
                        DocumentVersionDetail = "",
                        IsProcessed = false,
                        BlobDetail = "",
                        DocumentType = line.Line.Split(exceutionContext.SplitDelimeter)[3],
                        FilePathToUpload = line.Line.Split(exceutionContext.SplitDelimeter)[1],
                        AgreementDocumentType = documentTypeValue,
                        FileName = line.Line.Split(exceutionContext.SplitDelimeter)[2]
                    });
                }
            }
        }
    }
}
