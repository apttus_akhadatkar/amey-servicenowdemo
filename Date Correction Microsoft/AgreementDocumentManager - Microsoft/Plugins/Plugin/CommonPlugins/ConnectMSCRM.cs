﻿using Apttus.Model;
using System;

namespace Apttus.Plugins
{
    class ConnectMSCRM : PluginExecution
    {
        Helper helper;

        public ConnectMSCRM()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                //if (exceutionContext.FileDataLog.Count > 0 || exceutionContext.AgreementsToProcess.Count > 0)
                //{
                    exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.CRMConnectionInProgress;
                    #region Connection to CRM dynamics

                    //Connection to the CRM dynamics
                    helper.ConnectToMSCRM(exceutionContext.CRMUser);
                    exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.CRMConnectionDone;
                //}
                //else
                //{
                //    throw new Exception("No Data to process.");
                //}

                #endregion
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw new Exception("Unable to connect to CRM. Please check your connection or contact help desk."  + Environment.NewLine + ex.Message);
            }
        }
    }
}
