﻿using Apttus.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace Apttus.Plugins
{
    public class FileExecution : PluginExecution
    {
        #region Read File and Get Agreement Numbers
        public override void Process(Model.IExecutionContext exceutionContext)
        {

            try
            {
                exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.FileDataCapturingInProgress;
                this.CreateNewLogFile(exceutionContext);

                exceutionContext.InputFileDataLines = readFile(exceutionContext.FilePath);
            }
            catch (Exception ex)
            {
                exceutionContext.ContinueOnError = false;
                throw new Exception("Error while reading file." + Environment.NewLine + ex.Message);                
            }
            exceutionContext.ApplicationRunningState.ApplicationRunStatus = EnumApplicationState.FileDataCapturingDone;
        }
        private static List<DataLine> readFile(string filePath)
        {
            List<DataLine> dataLines = new List<DataLine>();
            try
            {
                using (StreamReader reader = new StreamReader(File.OpenRead(filePath)))
                {
                    while (!reader.EndOfStream)
                    {
                        String line = reader.ReadLine();
                        if (line != null && !String.IsNullOrWhiteSpace(line))
                            dataLines.Add(new DataLine() { Line = line });
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error in Reading file:" + Environment.NewLine + e.Message);
            }
            return dataLines;
        }
        public void CreateNewLogFile(Model.IExecutionContext exceutionContext)
        {
            //Get file to Read
            string strfile = "";
            String fullPath = exceutionContext.FilePath;//Path.GetFullPath(strfile);

            if (!exceutionContext.AllowLogs)
            {
                throw new Exception("No Write permission to save log file..");
            }

            //Create New LogFile
            String extension = Path.GetExtension(strfile);
            String sourceFileName = Path.GetFileNameWithoutExtension(strfile);

            if (fullPath.EndsWith(extension))
            {
                fullPath = fullPath.Substring(0, fullPath.LastIndexOf(extension));
            }
        }
        #endregion
    }
}