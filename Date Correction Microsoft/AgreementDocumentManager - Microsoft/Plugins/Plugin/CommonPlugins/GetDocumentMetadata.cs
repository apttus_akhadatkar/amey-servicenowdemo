﻿using Apttus.Model;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.Plugins
{
    class GetDocumentMetadata : PluginExecution
    {
        Helper helper;
        public GetDocumentMetadata()
        {
            helper = new Helper();
        }
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                EntityCollection AgreementEntityList = helper.ChunkAndFetchEntity(exceutionContext.AgreementsToProcess, "DocumentOnAgreements", exceutionContext.CRM_MaxQueryConditionChunk);

                foreach (Entity entity in AgreementEntityList.Entities)
                {
                    exceutionContext.FileDataLog.Add(new FileLogEntry()
                    {
                        AgreementNumber = entity.Contains("apttus_agreement_number") ? entity["apttus_agreement_number"].ToString() : "",
                        AgreementGUID = entity.Contains("apttus_apts_agreementid") ? entity["apttus_apts_agreementid"].ToString() : "",
                        DocumentVersion = entity.Contains("documentversion.apttus_documentversionidid") ? ((Microsoft.Xrm.Sdk.EntityReference)((Microsoft.Xrm.Sdk.AliasedValue)(entity["documentversion.apttus_documentversionidid"])).Value).Id.ToString() : "",
                        AgreementDocumentType = entity.Contains("agreement.apttus_documenttype") ? ((Microsoft.Xrm.Sdk.OptionSetValue)((Microsoft.Xrm.Sdk.AliasedValue)(entity["agreement.apttus_documenttype"])).Value).Value.ToString() : "",
                        DocumentVersionDetail = entity.Contains("documentversion.apttus_documentversiondetailid") ? ((Microsoft.Xrm.Sdk.AliasedValue)(entity["documentversion.apttus_documentversiondetailid"])).Value.ToString() : "",
                        DocumentVersionDetailTitle = entity.Contains("documentversion.apttus_title") ? ((Microsoft.Xrm.Sdk.AliasedValue)(entity["documentversion.apttus_title"])).Value.ToString() : "",
                        FileName = entity.Contains("documentversion.apttus_title") ? ((Microsoft.Xrm.Sdk.AliasedValue)(entity["documentversion.apttus_title"])).Value.ToString() : "",
                        FilePathToUpload = exceutionContext.fileDownLoadPath
                    });
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while Getting metadata." + Environment.NewLine + ex.Message);
            }
        }
    }
}
