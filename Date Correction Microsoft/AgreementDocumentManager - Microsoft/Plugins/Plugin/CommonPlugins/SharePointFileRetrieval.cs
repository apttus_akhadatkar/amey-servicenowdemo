﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using System.Text.RegularExpressions;
using System.IO;
using System.Security;

namespace Apttus.Plugins
{
    class SharePointFileRetrieval: PluginExecution
    {
        public override void Process(Model.IExecutionContext exceutionContext)
        {
            try
            {
                using (ClientContext clientContext = GetContextObject())
                {

                    Web web = clientContext.Web;
                    clientContext.Load(web, website => website.ServerRelativeUrl);
                    clientContext.ExecuteQuery();
                    Regex regex = new Regex("SiteURL", RegexOptions.IgnoreCase);
                    string strSiteRelavtiveURL = regex.Replace("FileUrl", string.Empty);
                    string strServerRelativeURL = CombineUrl(web.ServerRelativeUrl, strSiteRelavtiveURL);

                    Microsoft.SharePoint.Client.File oFile = web.GetFileByServerRelativeUrl(strServerRelativeURL);
                    clientContext.Load(oFile);
                    //ClientResult<Stream> stream = oFile.OpenBinaryStream();
                    //clientContext.ExecuteQuery();
                    //var output =  this.ReadFully(stream.Value);
                }
            }
            catch (Exception ex)
            {

            }
        }
        public string CombineUrl(string path1, string path2)
        {

            return path1.TrimEnd('/') + '/' + path2.TrimStart('/');
        }
        private ClientContext GetContextObject()
        {
            ClientContext context = new ClientContext("SiteURL");
            context.Credentials = new SharePointOnlineCredentials("UserName", Password);
            return context;
        }
        private Stream ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return new MemoryStream(ms.ToArray()); ;
            }
        }
        private SecureString _password;
        SecureString Password
        {
            get
            {

                if (_password == null)
                {
                    _password = GetPasswordFromConsoleInput("Password");
                }

                return _password;
            }
        }
        private static SecureString GetPasswordFromConsoleInput(string password)
        {
            //Get the user's password as a SecureString
            SecureString securePassword = new SecureString();
            char[] arrPassword = password.ToCharArray();
            foreach (char c in arrPassword)
            {
                securePassword.AppendChar(c);
            }

            return securePassword;
        }
    }
}
