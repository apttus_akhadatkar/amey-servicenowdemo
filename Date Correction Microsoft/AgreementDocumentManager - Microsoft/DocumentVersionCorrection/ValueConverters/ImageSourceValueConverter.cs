﻿using System;
using System.Configuration;
using System.Globalization;
using System.Windows.Data;

namespace Apttus.DocumentVersionCorrection.ValueConverters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class ImageSourceValueConverter : IValueConverter
    {
        private static string ImageFileLocation = ConfigurationManager.AppSettings.Get("ImageFileLocation");
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ImageFileLocation + value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }
}
