﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Apttus.CRMQueryManager.Controls
{
    /// <summary>
    /// Interaction logic for CRMQueryControl.xaml
    /// </summary>
    public partial class CRMQueryControl : Window
    {
        public CRMQueryControl()
        {
            InitializeComponent();
        }
    }
}
